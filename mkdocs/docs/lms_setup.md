
## Tool Registration Methods

> In the following LMS setup guides, `your.ltiaas.com` should be replaced with the subdomain that is registered in the LTIAAS portal for each specific API.
### Manual Setup

The endpoints needed to setup LTIaaS manually are:

- **Tool URL** - `https://your.ltiaas.com`
- **Redirect URL** - `https://your.ltiaas.com`
- **Deep Linking Launch URL** - `https://your.ltiaas.com`
- **Login URL** - `https://your.ltiaas.com/login`
- **Public Keyset URL** - `https://your.ltiaas.com/keys`

### Dynamic Registration

If Dynamic Registration is enabled for your instance, it can be used to quickly register your LTI tool in any compliant Platform.

- **Dynamic Registration URL** - `https://your.ltiaas.com/register`



### Canvas JSON Registration

If Canvas JSON Registration is enabled for your instance, it can be used to quickly register the Tool in any instance of the Canvas LMS.

- **Canvas JSON Registration URL** - `https://your.ltiaas.com/register/canvas`

More information on the **Canvas JSON Registration** can be found on the [Canvas LTI Registration Documentation](https://canvas.instructure.com/doc/api/file.lti_dev_key_config.html).

## LMS Specific Guides

* [Moodle Dynamic Registration](lms_setup/moodle_dynamic_registration.md)
* [Moodle Manual Registration](lms_setup/moodle_setup.md)
* [Canvas Manual Registration](lms_setup/canvas_setup.md)
* [Blackboard Manual Registration](lms_setup/blackboard_setup.md)
