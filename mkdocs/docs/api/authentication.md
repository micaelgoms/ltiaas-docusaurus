After a successful LTI® Launch is completed, **LTIaaS** will redirect the user to the URL registered during your onboarding configurations. **LTIaaS** will also append a `ltik` query parameter, a signed JWT identifying the current launch context.

*LTIaaS launch flow:*

![Launch Flow](../assets/launch.png)


This token **MUST be returned whenever the Tool wants to access any of the** [LTIaaS API Service endpoints](./endpoints.md#ltiaas-api-service-endpoints).

*LTIaaS Service API access flow:*

![API Service Access Flow](../assets/access.png)



The `ltik` token must be passed to the API through the **LTIK-AUTH-V1** Authorization header.


### LTIK-AUTH-V1 Authorization header

**LTIaaS** implements it's own authorization schema `LTIK-AUTH-V1`:

> Authorization: LTIK-AUTH-V1 Token=\<ltik\>, Additional=Bearer \<API_KEY\>

- **Token** - Ltik authentication token.
- **API_KEY** - API Key generated for your LTIaaS instance.

Example:

```
Authorization: LTIK-AUTH-V1 Token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c, Additional=Bearer KxwRJSMeKKF2QT4fwpM
```

### The Platforms endpoint

The only LTIaaS API endpoint that does not require the `ltik` to be passed is **/api/platforms**, since this endpoint should be accessible outside of a LTI® Launch.

In order to access the **Platforms** endoint you must use a **Bearer Authorization** header passing the API Key generated for your LTIaaS instance.

> Authorization: Bearer \<API_KEY\>


Example:

```
Authorization: Bearer KxwRJSMeKKF2QT4fwpM
```