The `/api/lineitems` endpoints implement the [Assignment and Grade Service](../the_lti_1.3_protocol.md#assignment-and-grade) and are used to access and manipulate line items and scores.

## Retrieve line items

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems` | `GET` | `LTIK_AUTH_V1` |

### Request

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `id` | `String` | `query` |   |
| `resourceId` | `String` | `query`  |   |
| `tag` | `String` | `query`  |   |
| `label` | `String` | `query`  |   |
| `resourceLinkId` | `Boolean` | `query`  |   |
| `limit` | `Number` | `query`  |   |
| `url` | `String` | `query`  |   |


- **id:** Retrieves a specific line item by ID. Line item IDs are URLs so the parameter should be URL encoded.
- **resourceId:** Filters line items by resourceId.
- **tag:** Filters line items by tag.
- **label:** Filters line items by label.
- **resourceLinkId:** Filters line items by the resource link ID attached to the current launch context.
- **limit:** Limits the number of line items returned.
- **url:** Retrieves line items from a specific URL.
    - *When present, the `url` parameter causes every other parameter to be ignored.* 
    - The `url` parameter should be URL encoded.
    - In cases where not all line items are retrieved when the line item limit is reached, the returned object will contain a `next` field holding an URL that can be used to retrieve the remaining line items through the `url` parameter. 


#### Example usage

```javascript
app.get('/get-lineitems', async (req, res, next) => {
  const ltik = req.query.ltik
  const query = {
    tag: 'grade_line',
    label: 'Grade line'
  }
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.get('https://your.ltiaas.com/api/lineitems', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
  return res.send(response.lineItems)
})
```


### Response

Returns an object with a **lineItems** field containing an array of line items.

More information about line items can be found in the [IMS Line Item Service specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#line-item-service).

- **lineItems** = Array of line items.
- **next** - URL of the next line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**
- **prev** - URL of the previous line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**
- **first** - URL of the first line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**
- **last** - URL of the last line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**

#### Example response

```javascript
{
  next: 'https://lms.example.com/sections/2923/lineitems/pages/2',
  first: 'https://lms.example.com/sections/2923/lineitems/pages/1',
  last: 'https://lms.example.com/sections/2923/lineitems/pages/3',
  lineItems: [  
    {
      id: 'https://lms.example.com/context/2923/lineitems/1',
      scoreMaximum: 60,
      label: 'Chapter 5 Test',
      resourceId: 'a-9334df-33',
      tag: 'grade',
      resourceLinkId: '1g3k4dlk49fk',
      endDateTime: '2018-04-06T22:05:03Z'
    },
    {
      id: 'https://lms.example.com/context/2923/lineitems/47',
      scoreMaximum: 100,
      label: 'Chapter 5 Progress',
      resourceId: 'a-9334df-33',
      tag: 'originality',
      resourceLinkId: '1g3k4dlk49fk'
    },
    {
      id: 'https://lms.example.com/context/2923/lineitems/69',
      scoreMaximum: 60,
      label: 'Chapter 2 Essay',
      tag: 'grade'
    }
  ]
}
```


## Create line item

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems` | `POST` | `LTIK_AUTH_V1` |

### Request

#### Parameters

Requests **MUST** have a body representing a Line Item Object as specified in the [IMS Line Item Object Specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#creating-a-new-line-item).

The only required parameters are **label** and **scoreMaximum**.

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `label` | `String` | `body`  | ✔️ |
| `scoreMaximum` | `Number` | `body`  | ✔️ |

- **label:** Line item label.
- **scoreMaximum:** Maximum score allowed for the line item.


#### Example usage

```javascript
app.post('/create-lineitem', async (req, res, next) => {
  const ltik = req.query.ltik
  const lineitem = {
    scoreMaximum: 60,
    label: 'Grade line',
    resourceId: 'quiz-231',
    tag: 'grade',
    resourceLinkId: '42', // Can be retrieved from ID Token's resource link ID (idtoken.launch.resource.id). Used to bind line item to LMS resource (activity).
    startDateTime: '2022-03-06T20:05:02Z',
    endDateTime: '2022-04-06T22:05:03Z'
  }
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.post('https://your.ltiaas.com/api/lineitems', { json: lineitem, headers: { Authorization: authorizationHeader } }).json()
  return res.send(response)
})
```

*Setting a resourceLinkId will bind the created line item to a specific resource (activity) in the LMS.*


### Response

Returns the newly created line item.


#### Example response

```javascript
{
  id: 'https://lms.example.com/context/2923/lineitems/1',
  scoreMaximum: 60,
  label: 'Chapter 5 Test',
  resourceId: 'quiz-231',
  tag: 'grade',
  resourceLinkId: '1234',
  startDateTime: '2018-03-06T20:05:02Z',
  endDateTime: '2018-04-06T22:05:03Z'
}
```


## Retrieve line item by ID

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid` | `GET` | `LTIK_AUTH_V1` |

*The line item ID is an URL so it **MUST** be URL encoded.*


### Request

#### Parameters

The `/lineitems/:lineitemid` GET endpoint receives no parameters.


#### Example usage

```javascript
app.get('/get-lineitem', async (req, res, next) => {
  const ltik = req.query.ltik
  const lineitemId = encodeURIComponent('https://lms.example.com/context/2923/lineitems/1')
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.get(`https://your.ltiaas.com/api/lineitems/${lineitemId}`, { headers: { Authorization: authorizationHeader } }).json()
  return res.send(response)
})
```


### Response

Returns the requested line item.


#### Example response

```javascript
{
  id: 'https://lms.example.com/context/2923/lineitems/1',
  scoreMaximum: 60,
  label: 'Chapter 5 Test',
  resourceId: 'quiz-231',
  tag: 'grade',
  startDateTime: '2018-03-06T20:05:02Z',
  endDateTime: '2018-04-06T22:05:03Z'
}
```


## Update line item by ID

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid` | `PUT` | `LTIK_AUTH_V1` |

*The line item ID is an URL so it **MUST** be URL encoded.*


### Request

#### Parameters

Requests **MUST** have a body representing a Line Item Object as specified in the [IMS Line Item Object Specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#creating-a-new-line-item).

The only required parameters are **label** and **scoreMaximum**.

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `label` | `String` | `body`  | ✔️ |
| `scoreMaximum` | `Number` | `body`  | ✔️ |


- **label:** Line item label.
- **scoreMaximum:** Maximum score allowed for the line item.

#### Example usage

```javascript
app.put('/update-lineitem', async (req, res, next) => {
  const ltik = req.query.ltik
  const lineitemId = encodeURIComponent('https://lms.example.com/context/2923/lineitems/1')
  const lineitem = {
    scoreMaximum: 60,
    label: 'Chapter 5 Updated'
  }
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.put(`https://your.ltiaas.com/api/lineitems/${lineitemId}`, { json: lineitem, headers: { Authorization: authorizationHeader } }).json()
  return res.send(response)
})
```


### Response

Returns the updated line item.


#### Example response

```javascript
{
  id: 'https://lms.example.com/context/2923/lineitems/1',
  scoreMaximum: 60,
  label: 'Chapter 5 Updated',
  resourceId: 'quiz-231',
  tag: 'grade',
  startDateTime: '2018-03-06T20:05:02Z',
  endDateTime: '2018-04-06T22:05:03Z'
}
```


## Delete line item by ID

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid` | `DELETE` | `LTIK_AUTH_V1` |

*The line item ID is an URL so it **MUST** be URL encoded.*

### Request

#### Parameters

The `/lineitems/:lineitemid` DELETE endpoint receives no parameters.

#### Example usage

```javascript
app.delete('/launch', async (req, res, next) => {
  const ltik = req.query.ltik
  const lineitemId = encodeURIComponent('https://lms.example.com/context/2923/lineitems/1')
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  await request.delete(`https://your.ltiaas.com/api/lineitems/${lineitemId}`, { headers: { Authorization: authorizationHeader } })
  return res.sendStatus(204)
})
```

### Response

Returns `204` status if the line item is successfully deleted.



## Submit scores

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid/scores` | `POST` | `LTIK_AUTH_V1` |

*The line item ID is an URL so it **MUST** be URL encoded.*

### Request

#### Parameters

Requests MUST have a body representing a Score Object as specified in the [IMS Score Specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#example-application-vnd-ims-lis-v1-score-json-representation).

The required parameters are **userId**, **activityProgress** and **gradingProgress**. The parameters **timestamp** and **scoreMaximum** are also required but are set automatically by **LTIaaS**.

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `userId` | `String` | `body`  | ✔️ |
| `activityProgress` | `String` | `body`  | ✔️ |
| `gradingProgress` | `String` | `body`  | ✔️ |


- **userId:** Target user ID.
- **activityProgress:** Status of the user towards the activity's completion.
- **gradingProgress:** Status of the grading process.


#### Example usage

```javascript
app.get('/submit-score', async (req, res, next) => {
  const ltik = req.query.ltik
  const lineitemId = encodeURIComponent('https://lms.example.com/context/2923/lineitems/1')
  const score = {
    userId: '5323497',
    activityProgress: 'Completed',
    gradingProgress: 'FullyGraded',
    scoreGiven: 83,
    comment: 'This is exceptional work.',
  }
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.post(`https://your.ltiaas.com/api/lineitems/${lineitemId}/scores`, { json: score, headers: { Authorization: authorizationHeader } }).json()
  return res.send(response)
})
```


### Response

Returns the submitted score.


#### Example response

```javascript
{
  userId: '5323497',
  activityProgress: 'Completed',
  gradingProgress: 'FullyGraded',
  scoreGiven: 83,
  scoreMaximum: 100,
  comment: 'This is exceptional work.',
  timestamp: '2021-04-16T18:54:36.736+00:00',
}
```


## Retrieve scores

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid/scores` | `GET` | `LTIK_AUTH_V1` |

*The line item ID is an URL so it **MUST** be URL encoded.*

### Request

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `userId` | `String` | `query` |   |
| `limit` | `Number` | `query`  |   |
| `url` | `String` | `query`  |   |


- **userId:** Filters scores based on the user ID.
- **limit:** Limits the number of scores returned.
- **url:** Retrieves scores from a specific URL.
    - *When present, the `url` parameter causes every other parameter to be ignored.* 
    - The `url` parameter should be URL encoded.
    - In cases where not all scores are retrieved when the score limit is reached, the returned object will contain a `next` field holding an URL that can be used to retrieve the remaining scores through the url parameter. 


#### Example usage

```javascript
app.get('/launch', async (req, res, next) => {
  const ltik = req.query.ltik
  const lineitemId = encodeURIComponent('https://lms.example.com/context/2923/lineitems/1')
  const query = {
    userId: 2,
    limit: 10
  }
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.get(`https://your.ltiaas.com/api/lineitems/${lineitemId}/scores`, { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
  return res.send(response.scores)
})
```


### Response

Returns an object with a **scores** field containing an array of scores.

More information about scores can be found in the [IMS Result Service specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#result-service).

- **scores** - Array of scores.
- **next** - URL of the next score page. **This field is only present if the score limit was reached before the full list was retrieved.**
- **prev** - URL of the previous score page. **This field is only present if the score limit was reached before the full list was retrieved.**
- **first** - URL of the first score page. **This field is only present if the score limit was reached before the full list was retrieved.**
- **last** - URL of the last score page. **This field is only present if the score limit was reached before the full list was retrieved.**


#### Example response

```javascript
{
  next: 'https://lms.example.com/sections/2923/scores/pages/2',
  first: 'https://lms.example.com/sections/2923/scores/pages/1',
  last: 'https://lms.example.com/sections/2923/scores/pages/3',
  scores: [
    {
      id: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem/results?type_id=1&user_id=2',
      userId: '2',
      resultScore: 100,
      resultMaximum: 100,
      scoreOf: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem?type_id=1',
      timestamp: '2020-06-02T10:51:08-03:00'
    },
    {
      id: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem/results?type_id=1&user_id=2',
      userId: '3',
      resultScore: 90,
      resultMaximum: 100,
      scoreOf: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem?type_id=1',
      timestamp: '2020-06-02T10:51:08-03:00'
    }
  ]
}
```