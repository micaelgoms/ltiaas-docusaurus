The `/api/idtoken` endpoint is used to retrieve the **ID Token** object containing the current launch context information.

## Retrieve ID Token

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/idtoken` | `GET` | `LTIK_AUTH_V1` |


### Request

#### Parameters

The `/api/idtoken` endpoint receives no parameters.

#### Example usage

```javascript
app.get('/launch', async (req, res, next) => {
  const ltik = req.query.ltik
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const idtoken = await request.get('https://your.ltiaas.com/api/idtoken', { headers: { Authorization: authorizationHeader } }).json()
  return res.send(idtoken)
})
```

### Response

Returns an **ID Token** object containing the entire launch context information. 

The contents of the object may vary, since Platforms might return different information, but the information is divided into easy to understand categories:

- **user** - User Information.
    - **id** - User ID.
    - **email** - User email.
    - **given_name** - User given name.
    - **family_name** - User family name.
    - **name** - User name.
    - **roles** - User roles in the current context.
- **platform** - Platform information.
    - **id** - Platform ID.
    - **url** - Platform URL.
    - **clientId** - Platform generated Client ID.
    - **deploymentId** - Platform generated Deployment ID.
    - **product_family_code** - Platform family code.
    - **version** - Platform version.
    - **name** - Platform name.
    - **description** - Platform description.
- **launch** - Launch context information.
    - **type** - Launch type.
    - **target** - Launch target URL.
    - **context** - Course or section information
        - id
        - label
        - title
        - type
    - **resource** - Activity or placement information.
        - id
        - title
    - **presentation** - Information regarding how the Tool is being displayed in the Platform.
        - locale
        - document_target
    - **custom** - Custom parameters object.
    - **lineItemId** - Line item assigned to this resource. This field is only present if there is exactly one line item assigned to this resource. Can be used to quickly access the [LineItems endpoint](lineitems.md) to [submit](lineitems.md#submit-scores) or [retrieve scores](lineitems.md#retrieve-scores).
  

#### Example response

```javascript
{
  user: {
    id: '2',
    given_name: 'Admin',
    family_name: 'User',
    name: 'Admin User',
    email: 'admin@lms.example.com',
    roles: [
      'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator',
      'http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor',
      'http://purl.imsglobal.org/vocab/lis/v2/system/person#Administrator'
    ]
  },
  platform: {
    id: '0c41aa0849215449d4298e58f7626c68',
    url: 'https://lms.example.com',
    clientId: 'CLIENTID',
    deploymentId: '1',
    product_family_code: 'canvas',
    version: '2020073000',
    guid: 'lms.example.com',
    name: 'LMS',
    description: 'LMS',
    lis: {
      person_sourcedid: '',
      course_section_sourcedid: ''
    },
    deepLinkingSettings: null
  },
  launch: {
    type: 'LtiResourceLinkRequest',
    target: 'http://tool.example.com?resource=value1',
    context: {
      id: '2',
      label: 'course',
      title: 'Course',
      type: [
          'CourseSection'
        ]
    },
    resource: {
      title: 'Activity',
      id: '1'
    },
    presentation: {
      locale: 'en',
      document_target: 'iframe',
      return_url: 'https://lms.example.com/mod/lti/return.php?course=2&launch_container=3&instanceid=1&sesskey=huxFIyJ0BP'
    },
    custom: {
      group: '2',
      system_setting_url: 'https://lms.example.com/mod/lti/services.php/tool/1/custom',
      context_setting_url: 'https://lms.example.com/mod/lti/services.php/CourseSection/2/bindings/tool/1/custom',
      link_setting_url: 'https://lms.example.com/mod/lti/services.php/links/{link_id}/custom'
    },
    lineItemId: 'https://lms.example.com/mod/lti/services.php/2/lineitems/2/lineitem?type_id=1'
  }
}
```

Since the idtoken receives varied information depending on the LMS, fields don't follow a strict naming convention. Fields not using camel case are usually the ones coming directly from the LMS without any modification.
