In order for Tool and Platform to interact with each other, they must be registered inside one another. The **/platforms** endpoints are used to programatically access and manipulate Platform registrations.

For more information regarding Tool and Platform registration check [LMS Setup](../lms_setup.md).

## Retrieve Platforms

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms` | `GET` |   |


### Request

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `id` | `String` | `query` |   |
| `url` | `String` | `query` |   |
| `clientId` | `String` | `query`  |   |


- **id:** Retrieves a specific Platform by ID.
- **url:** Filters line items by the URL.
- **clientId:** Filters line items by the client ID.


#### Example usage

```javascript
const query = {
  url: 'https://platform.org'
}
const authorizationHeader = `Bearer ${API_KEY}`
const response = await request.get('https://your.ltiaas.com/api/platforms', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
return response.platforms
```


### Response

Returns an object with a **platforms** field containing an array of Platforms registrations.

- **platforms** - Array of `Platform` objects.

#### Example response

```javascript
{
  platforms: [
    {
      url: 'https://platform.org',
      clientId: '10000000000001',
      name: 'Platform 1',
      authenticationEndpoint: 'https://platform.org/lti/authorize',
      authConfig: {
        key: 'https://platform.org/lti/security/jwks',
        method: 'JWK_SET'
      },
      accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
      id: 'f33d6c450b7632c1ff87586c4f150eee',
      publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
      active: true
    },
    {
      url: 'https://platform.org',
      clientId: '20naAWuw8m4SGVH',
      name: 'Platform 2',
      authenticationEndpoint: 'https://platform.org/mod/lti/auth.php',
      authConfig: {
        method: 'JWK_SET',
        key: 'https://platform.org/mod/lti/certs.php'
      },
      accesstokenEndpoint: 'https://platform.org/mod/lti/token.php',
      id: '3e7853607a37ed57c980f05c03b116ed',
      publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
      active: true
    }
  ]
}
```



## Register Platform

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms` | `POST` |   |

### Request

#### Parameters


| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `url` | `String` | `body`  | ✔️ |
| `clientId` | `String` | `body`  | ✔️ |
| `name` | `String` | `body`  | ✔️ |
| `authenticationEndpoint` | `String` | `body`  | ✔️ |
| `accesstokenEndpoint` | `String` | `body`  | ✔️ |
| `authConfig` | `Object` | `body`  | ✔️ |
| `authConfig.method` | `String` | `body`  | ✔️ |
| `authConfig.key` | `String` | `body`  | ✔️ |

- **url:** Platform URL.
- **clientId:** Platform client ID.
- **name:** Platform name.
- **authenticationEndpoint:** Platform authentication endpoint.
- **accesstokenEndpoint:** Platform access token endpoint.
- **authConfig:** Platform token authentication configuration object.
    - **method:** Platform token authentication method. **MUST be one of JWK_SET, JWK_KEY or RSA_KEY**.
    - **key:** Platform token authentication key.


#### Example usage

```javascript
const platform = {
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  }
}
const authorizationHeader = `Bearer ${API_KEY}`
const newPlatform = await request.post('https://your.ltiaas.com/api/platforms', { json: platform, headers: { Authorization: authorizationHeader } }).json()
return newPlatform
```

### Response

Returns the newly created Platform.


#### Example response

```javascript
{
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  },
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  id: 'f33d6c450b7632c1ff87586c4f150eee',
  publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
  active: true
}
```


## Retrieve Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid` | `GET` |   |

### Request

#### Parameters

The **/platforms/:platformid** GET endpoint receives no parameters.

#### Example usage

```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
const platform = await request.get(`https://your.ltiaas.com/api/platforms/${platformId}`, { headers: { Authorization: authorizationHeader } }).json()
return platform
```


### Response

Returns the requested Platform.

#### Example response

```javascript
{
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  },
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  id: 'f33d6c450b7632c1ff87586c4f150eee',
  publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
  active: true
}
```

## Update Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid` | `PUT` |   |

### Request

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `url` | `String` | `body`  |   |
| `clientId` | `String` | `body`  |   |
| `name` | `String` | `body`  |   |
| `authenticationEndpoint` | `String` | `body`  |   |
| `accesstokenEndpoint` | `String` | `body`  |   |
| `authConfig` | `Object` | `body`  |   |
| `authConfig.method` | `String` | `body`  |   |
| `authConfig.key` | `String` | `body`  |   |

- **url:** Platform URL.
- **clientId:** Platform client ID.
- **name:** Platform name.
- **authenticationEndpoint:** Platform authentication endpoint.
- **accesstokenEndpoint:** Platform access token endpoint.
- **authConfig:** Platform token authentication configuration object.
    - **method:** Platform token authentication method. **MUST be one of JWK_SET, JWK_KEY or RSA_KEY**.
    - **key:** Platform token authentication key.


#### Example usage

```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const platform = {
  name: 'Platform 3',
  authenticationEndpoint: 'https://platform.org/lti/authorize2',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks2'
  }
}
const authorizationHeader = `Bearer ${API_KEY}`
const updatedPlatform = await request.post(`https://your.ltiaas.com/api/platforms/${platformId}`, { json: platform, headers: { Authorization: authorizationHeader } }).json()
return updatedPlatform
```

### Response

Returns the updated Platform.


#### Example response

```javascript
{
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 3',
  authenticationEndpoint: 'https://platform.org/lti/authorize2',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks2',
    method: 'JWK_SET'
  },
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  id: 'f33d6c450b7632c1ff87586c4f150eee',
  publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
  active: true
}
```


## Delete Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid` | `DELETE` |   |

### Request

#### Parameters

The **/platforms/:platformid** DELETE endpoint receives no parameters.

#### Example usage


```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
await request.delete(`https://your.ltiaas.com/api/platforms/${platformId}`, { headers: { Authorization: authorizationHeader } })
return true
```

### Response

Returns `204` if the Platform is successfully deleted.



## Activate Platform by ID

Dynamically registered Platforms have to be activated if the the `Auto Activate Dynamically Registered Platforms` option was turned off during the LTIaaS onboarding process.


| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid/activate` | `POST` |   |

### Request

#### Parameters

The **/platforms/:platformid/activate** POST endpoint receives no parameters.

#### Example usage


```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
await request.post(`https://your.ltiaas.com/api/platforms/${platformId}/activate`, { headers: { Authorization: authorizationHeader } })
return true
```

### Response

Returns `200` if the Platform is successfully activated.



## Deactivate Platform by ID

Dynamically registered Platforms have to be activated if the the `Auto Activate Dynamically Registered Platforms` option was turned off during the LTIaaS onboarding process. Platforms can also be deactivated in order to block its access to the Tool.


| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid/deactivate` | `POST` |   |

### Request

#### Parameters

The **/platforms/:platformid/deactivate** POST endpoint receives no parameters.

#### Example usage

```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
await request.post(`https://your.ltiaas.com/api/platforms/${platformId}/deactivate`, { headers: { Authorization: authorizationHeader } })
return true
```

### Response

Returns `200` if the Platform is successfully deactivated.



