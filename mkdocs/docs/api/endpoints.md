Through the **LTIaaS** API it's possible to access all of the LTI® 1.3 services and functionalities.


### Basic endpoints

The Base LTI endpoints are the ones that don't implement a LTI® 1.3 service and are usually not directly accessed by the Tool:

- **/** - The base route is used to receive LTI® launches and redirect to the endpoints defined during the LTIaaS onboarding process.
- **/login** - Used to receive login initiation requests at the start of the LTI® launch process.
- **/keys** - Public keyset route, used by the Platforms to validate signed messages. Returns the public JWK to each registered Platform.
- **/register** - Endpoint used to start Dynamic Registration Flow.
- **/register/canvas** - Endpoint used to retrieve Canvas Registration JSON.


### LTIaaS API Service endpoints

The LTIaaS API endpoints are the ones that implement the LTI® 1.3 services and are accessed directly by the Tool:

- **[/idtoken](./id-token.md) [GET]** - Retrieve IdToken for the current launch context.
- **[/deeplinking](./deeplinking.md)** - Create Deep Linking message or form.
    - **/message [POST]** - Create Deep Linking signed JWT containing Content Items chosen.
    - **/form [POST]** - Create Deep Linking self submitting form containing Content Items chosen.
- **[/memberships](./memberships.md) [GET]** - Retrieve a list of user memberships inside the current Platform context.
- **[/lineitems](./lineitems.md) [GET, POST]**  - Retrieve and create line items.
    - **/:lineitemId [GET, PUT, DELETE]** - Retrieve, update and delete a line item.
        - **/scores [GET, POST]** - Retrieve or Publish scores to a line item.


These routes are accessed with the prefix `/api`.


### LTIaaS API Platforms endpoint

- **[/platforms](./platforms.md) [GET, POST]** - Retrieve or create Platform registrations.
    - **/:platformID [GET, PUT, DELETE]** - Retrieve, update or delete a Platform registration. 
        - **/:activate [POST]** - Activates dynamically registered Platform.
        - **/:deactivate [POST]** - Dectivates dynamically registered Platform. 

These routes are also accessed with the prefix `/api`.
