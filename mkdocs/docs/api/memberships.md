The `/api/memberships` endpoint implements the [Names and Roles Provisioning Service](../the_lti_1.3_protocol.md#names-and-roles-provisioning) and is used to retrieve a list of User **memberships**.

## Retrieve memberships

| ROUTE | METHOD  | AUTHENTICATION METHOD |
| ---- | --- | -- |
| `/api/memberships` | `GET` | `LTIK_AUTH_V1` |

### Request

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `role` | `String` | `query` |   |
| `limit` | `Number` | `query`  |   |
| `resourceLinkId` | `Boolean` | `query`  |   |
| `url` | `String` | `query`  |   |


- **role:** Filters memberships by role. Possible role values are defined in the [IMS Role Vocabularies](https://www.imsglobal.org/spec/lti/v1p3/#role-vocabularies).
- **limit:** Limits the number of memberships returned.
- **resourceLinkId:** Retrieves the Resource Link level memberships as specified in the [IMS Resource Link Membership Service specification](https://www.imsglobal.org/spec/lti-nrps/v2p0#resource-link-membership-service).
- **url:** Retrieves memberships from a specific URL.
    - *When present, the `url` parameter causes every other parameter to be ignored.*
    - The `url` parameter should be URL encoded.
    - In cases where not all members are retrieved when the membership limit is reached, the returned object will contain a `next` field holding an URL that can be used to retrieve the remaining members through the `url` parameter.



#### Example usage

```javascript
app.get('/get-members', async (req, res, next) => {
  const ltik = req.query.ltik
  const query = {
    role: 'Learner',
    limit: 10
  }
  const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
  const response = await request.get('https://your.ltiaas.com/api/memberships', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
  return res.send(response.members)
})
```

### Response

Returns an object with a **members** field containing an array of User memberships. **Only the `user_id` and `roles` fields of each membership are required to be returned by the Platform**.

More information about User memberships can be found in the [IMS Membership specification](https://www.imsglobal.org/spec/lti-nrps/v2p0#membership-container-media-type).

- **id** - URL the memberships were retrieved from.
- **members** - Array of User memberships.
- **context** - Context the memberships were retrieved from.
- **next** - URL of the next membership page. **This field is only present if the membership limit was reached before the full members list was retrieved.**

#### Example response

```javascript
{
  id : 'https://lms.example.com/sections/2923/memberships',
  context: {
    id: '2923-abc',
    label: 'CPS 435',
    title: 'CPS 435 Learning Analytics',
  },
  next: 'https://lms.example.com/sections/2923/memberships/pages/2',
  members : [
    {
      status : 'Active',
      name: 'Jane Q. Public',
      picture : 'https://platform.example.edu/jane.jpg',
      given_name : 'Jane',
      family_name : 'Doe',
      middle_name : 'Marie',
      email: 'jane@platform.example.edu',
      user_id : '0ae836b9-7fc9-4060-006f-27b2066ac545',
      lis_person_sourcedid: '59254-6782-12ab',
      roles: [
        'http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor'
      ]
    }
  ]
}
```