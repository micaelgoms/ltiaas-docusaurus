## Setting Up Your Account
With LTIAAS, setting up an LTI API for your company is easy. LTIAAS hosts a private API service that you can use to connect LTI tools with LMSs. Getting started is free and easy.

This guide will walk you through creating an account and activating your API in the [LTIAAS Portal](https://portal.ltiaas.com).

### Step 1. Log In To The Portal
All LTIAAS services are managed via the customer portal. To access the portal, please log in using either your email address or Google account. If an account doesn't exist, it will be created for you.

[![](../assets/portal/portal-login.png)](https://portal.ltiaas.com)

### Step 2. Add An Account
Once logged in, you'll be presented with the option to add an account by clicking on the `+ Add Account` button.

![](../assets/portal/add-account.png)

This will take you to the account creation screen.

### Step 3. Create New Account
Before LTIAAS can create and activate the account, we need to know the account type. We have two types that we support today: An LTI Tool (a web application that is accessed from an LMS) or an LTI Platform (the LMS itself).

On this 'Create New Account' screen, you are asked to provide a name. This name is only use by you. Some customers have several accounts, so it is helpful to have easily identifiable names for each.

![](../assets/portal/create-account.png)

### Step 4. Create Your Plan
On the next screen, you get to choose the options for your plan. Options can be selected by clicking the `Select` button below each plan option.

- **Core** (required): This is the core component to the API that is required for a basic LTI launch, the most basic LTI function.
- **Deep Linking**: Select this component when you want your tool to provide multiple activities within the LMS. Deep linking allows an LTI tool to present an HTML selection screen where a teacher can select a specific activity to add to a course.
- **Dynamic Registration**: This is a new LTI 1.3 feature that, as of February 2022, is implemented by the latest versions of Moodle and BrightSpace (D2L). It makes registration of LTI tools into LMSs push button. With this option, LTIAAS provides a registration URL that LMSs can use to register tools automatically.
- **Names and Roles**: This component enables the API features that allow you to query the LMS for a list of students in a given class.
- **Assignments and Grades**: When selected, this component will enable the API endpoint to get and send grades between the LMS and tool.
- **Custom Domain Name**: When selected, LTIAAS will host your private API on a custom domain of you choosing that you own. For example _lti.mycompany.com_, instead of a subdomain of _ltiaas.com_. The API setup page will allow you to enter in a [custom domain name](custom_domain.md) and LTIAAS servers will serve your API on that domain with SSL termination.
- **Moodle Plugin**: This is a custom extension to the LTI 1.3 protocol that only LTIAAS has. It enables any Moodle instance to act like an LTI tool. If you choose this option, we will give you a download link to a Moodle plugin that talks with LTIAAS. When users launch to your Moodle instance through LTIAAS, they will be automatically logged in. The companion Moodle plugin will even enable your Moodle instance to provide a list of available assignments in a pre-built Deep-Linking view.
- **Self Hosted**: Some of our customers require LTIAAS to be hosted on-site in their data center. We have an offering for this, but it is not available in our self-service portal yet. Please [Contact Us](https://ltiaas.com/contact-us) for more information.

Once you have selected your plan options, we ask that you enter in your credit card to activate the subscription. If a credit card is not an available option for payment, please [reach out](https://ltiaas.com/contact-us) to us. We can accommodate most payment methods with manual intervention.

When the form is complete. Click the `Subscribe` button.

![](../assets/portal/subscribe-button.png)

> After subscribing, you can change your subscription and billing information at any time without an interruption in service. Just click on the `Billing` link in the portal for this account.

### Step 5. Set Your API Settings
Once you have created your account, the next step is to set up your API. You should be guided to the `API Settigns` page. If not, please click on the `API Settings` link on the sidebar.

![](../assets/portal/api-settings-link.png)

You will be asked to fill in the API settings. 

> All the API settings can be changed at any time. However, the subdomain, once set, cannot be changed without contacting support.

In order for basic LTI launch to work, you need to set the `Launch URL` setting. This is the URL, hosted by you, that users will be directed to when they click the link to your activity in the LMS.

In order for Deep Linking to work (if you choose it as an option in step 4), you need to provide the URL. This URL, hosted by you, is the location that that users (probably teachers) will be directed to when they click the option to "*choose an activity*" when adding your tool into a course.

After filling on the *API settings* form (as shown below), click the submit button at the bottom of the form.

![](../assets/portal/api-settings-1.png)
![](../assets/portal/api-settings-2.png)
![](../assets/portal/api-settings-3.png)

> Note: The last three sections displayed in this guide: `Deep Linking Settings`, `Moodle Plugin Settings`, and `Dynamic Registration Settings` will only appear if you selected those features in step 4.


### Step 6. Deploy!
Any time you make a change to the *API settings*, you need to deploy those changes to the LTIAAS cloud. The LTIAAS Portal will detect when these settings change and will display a message (shown below) asking you to deploy the settings change.

![](../assets/portal/deploy-now.png)

Clicking on the `Deploy Now` button will trigger the settings to propagate to the LTIAAS cloud service. Typically, changes will be available in about 10 seconds. But in rare cases, it may take up to 5 minutes for the new settings to be deployed. In the process of deploying the changes, your private API may become unavailable for a few seconds.

### Step 7. Get Your API Key
After your first deploy, your private API key will become visible.

![](../assets/portal/api-key.png)

Click on the button to copy this key to your clipboard. This key should be used when [making API requests](https://ltiaas.com/docs/api_documentation/#authentication) to LTIAAS. 

### Step 8. Test The API
There are a couple quick tests you can do to make sure your API is live and your API key is working.

#### Access The Public `/keys` Endpoint
LTIAAS serves a list of public keys for all the platforms registered to your service. You can use the `/keys` endpoint to get this list. It is also a good first test to ensure the LTIAAS service is accessible (from behind a fire walled server, for example). You can access this endpoint with any web browser by navigating to [https://your.ltiaas.com/keys](https://your.ltiaas.com/keys) to use the cURL tool:

```bash
curl https://your.ltiaas.com/keys
```
> Where `your` is the subdomain you chose in step 5.

A new API with no registrations should return an empty array called 'keys':

```bash
{"keys":[]}
```

#### Access The Private `/api/platforms` Endpoint
Once the `/keys` endpoint has been validated, the next step is to attempt to access a private endpoint that requires an API Key to access. The simplest API request to make is a GET request to the `/api/platforms` endpoint. It is the endpoint used to get a lost of registered LMSs.

```bash
curl -H "Authorization: Bearer <API_KEY>" https://your.ltiaas.com/api/platforms
```
> Where `your` is the subdomain you chose in step 5, and `<API_KEY>` is the key found from step 7.

A new API with no registrations should return an empty array called 'platforms'.

```bash
{"platforms":[]}
```

 If your API Key was incorrect, you will get a message stating you have an "Invalid API Key".