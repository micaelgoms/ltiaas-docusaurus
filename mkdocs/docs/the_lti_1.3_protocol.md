

The learning tools interoperability (LTI®) protocol describes how any learning resource can communicate with a Learning management system (LMS) and exchange relevant information, seamlessly integrating them together to improve the educational process.

Possible interactions between Platforms and the Tools are described as Services. The LTI® 1.3 protocol officially describes 4 Services:

- **Core (Launch)**
- **Deep Linking**
- **Names and Roles Provisioning (Memberships)**
- **Assignment and Grade**

## Core

The [Core Service](https://www.imsglobal.org/spec/lti/v1p3/) (Launch) is the most basic interaction between a Platform and Tool. It describes the process through which a Tool can be launched from within a Platform, usually in the form of an embedded iframe, while receiving information about the launch context.

After a successful launch, a Tool should have access to an IdToken, a signed JWT containing the launch context and a variety of additional information.

Some of the relevant information that can be found in an IdToken:

- **User Information**
  - User ID
  - Email
  - Name
  - Role inside current Platform context (e.g. Instructor, Learner).
- **Platform information**
  - URL
  - Name
  - Version
- **Tool Information**
  - ClientID
- **Context Information**
  - Course ID
  - Activity ID
- **Custom Parameters**

A **Custom Parameter** is a key-value pair set at the moment of the creation of a Tool Link (e.g. Activity) inside a Platform. Tools offering multiple resources often use either **Custom Parameters** or **Query Parameters** to decide which resource to display after a successful launch.

More information about the LTI Launch and the IdToken can be found in the [Official IMS LTI® Specification](https://www.imsglobal.org/spec/lti/v1p3/).


## Deep Linking

The [Deep Linking Service](https://www.imsglobal.org/spec/lti-dl/v2p0) can be used to create Tool Links inside a Platform. 

The Deep Linking process consists of a Launch to the Tool’s deep linking endpoint where the user can select which resource(s) they want to link. The tool will then generate a signed JWT containing the selected resources and submit this message through a self-submitting form to the Platform.

  
Deep Linking resources are called “Content Items”, Tool Links are only one of the available types of Content Item:

- LTI® Resource Link (Tool Link).
- Link
- File
- HTML Fragment
- Image

The LTI® protocol allows custom Content Item types to be created. Each Platform can choose any number of available types to support.

Tools will often assign **Query Parameters** to identify the selected resource in each Tool Link URL.


## Names and Roles Provisioning

The [Names and Roles Provisioning Service](https://www.imsglobal.org/spec/lti-nrps/v2p0) allows a Tool to retrieve a list of User **memberships** inside the current Platform context (usually the current Course). It is possible to filter the results with the following parameters:

- role - The User’s role within the context, e.g. Instructor, Learner.
- limit - The maximum number of results to be returned.
- rlid - Access [Resource Link level memberships](https://www.imsglobal.org/spec/lti-nrps/v2p0#resource-link-membership-service) by passing the id for the current context's resource link.

Platforms can choose what User information they want to share with Tools, only two parameters are required to be present:

- user_id - The User’s ID inside the Platform.
- roles - The User’s roles within the current Platform context.

## Assignment and Grade

The [Assignment and Grade Service](https://www.imsglobal.org/spec/lti-ags/v2p0) allows a Tool to retrieve and create scores and line items inside a Platform. These functionalities are separated into three subservices:

- **Line Item Service** - Allows Tools to retrieve, create or delete line items in the current context’s grade book.

- **Score Publish Service** - Allows Tools to publish scores to a specific line item in the current context’s grade book.

- **Result Service** - Allows Tools to retrieve scores from lines items in the current context’s grade book.

