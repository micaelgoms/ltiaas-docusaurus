# Blackboard - Manual Registration

## Summary

To set up LTIAAS with Blackboard you need to:

1.  [Install the LTIAAS tool into Blackboard for all tenants](#1-install-the-ltiaas-tool-into-blackboard-for-all-tenants)
2.  [Register the Blackboard provided details into LTIAAS](#2-register-the-blackboard-provided-details-into-ltiaas)
3.  [Install the LTIAAS tool into Blackboard](#3-install-ltiaas-tool-into-blackboard)
4.  [Set up a placement for the LTIAAS tool](#4-setup-a-placement-for-the-tool)
5.  [Do a test launch](#4-do-a-test-launch)

## 1) Install the LTIAAS tool into Blackboard for all tenants
Blackboard uses a concept called 'multi-tenancy'. This allows an LTI tool to be registered once within Blackboard.com and is then made available to all Blackboard "tenants". So, a tool registration is not done for each LMS instance, but only once at [https://developer.blackboard.com](https://developer.blackboard.com), then made available by Blackboard to all instances.

1. Navigate to to [https://developer.blackboard.com](https://developer.blackboard.com) and sign up and/or sign in.

2. Click on “My Applications”, then click “Register a REST or LTI application”.
![](../assets/lti_how_to/blackboard-register-application.png)
3. Fill in the form with the details below:
    *   **Key name:** 
    *   **Application Name:** `Your Tool Name`
    *   **Description:** `Your Tool Description` 
    *   **Domain(s):** `your.ltiaas.com`
    *   **Group:** (your choice) <- _This is only for organizing apps within your Blackboard Developer account._
    *   **My Integration supports LTI 1.3:** `CHECKED`
    *   **Login Initiation URL:** `https://your.ltiaas.com/login`
    *   **Tool Redirect URL(s):** `https://your.ltiaas.com`
    *   **Tool JWKS URL:** `https://your.ltiaas.com/keys`
    *   **Signing Algorithm:** `RS256`
4. Click the '**Register Application**' button.
![](../assets/lti_how_to/blackboard-register-tool.png)
5. After the Blackboard registration process is complete, the blackboard endpoints for this tool are made available. Copy the following values that will be used in the LTIAAS registration:

    * Application ID
    * Issuer
    * Public keyset URL
    * Auth token endpoint
    * OIDC auth request endpoint

![](../assets/lti_how_to/blackboard-tool-key.png)

## 2) Register the Blackboard provided details into LTIAAS

Follow the steps on the [Registering Your First Platform](portal_registration.md) page. Use the table below to help translate the information provided by the LMS to values needed in the LTIAAS registration process.

LTIAAS Registration API Name | Blackboard Provided Name
----------- | ---------------
**name**  | _Any name you would like that helps you identify your LSM_
**Client ID**   | _Application ID_
**url** | _Issuer_
**authConfig.method** | `JWK_SET`
**authConfig.key** | _Public keyset URL_
**accesstokenEndpoint** | _Auth token endpoint_
**authenticationEndpoint** | _OIDC auth request endpoint_

## 3) Install LTIAAS Tool into Blackboard

1.  Login to blackboard as aa administrator and click on the 'System Admin' menu button  
    ![](../assets/lti_how_to/blackboard_system_admin.png)
2.  Navigate to 'Integrations' -> 'LTI Tool Providers'  
    ![](../assets/lti_how_to/blackboard_lti_tool_providers.png)
3.  In the resulting 'LTI Tool Providers' page, click 'Register LTI 1.3/Advantage Tool'  
    ![](../assets/lti_how_to/blackboard_register_tool.png)
4.  In the resulting page you will be prompted to enter a client ID.  
    enter the `Client ID` that was created in step 1, then click 'Submit'. ![](../assets/lti_how_to/blackboard_client_id.png)
5.  This will bring you to a pre-filled 'Accept LTI 1.3 Tool' form. Please make sure the following settings are changed:
    *   **Tool Status:** Approved
    *   **User Fields To Send:** Role in Course, Name, Email Address
    *   **Allow grade service access:** Yes
    *   **Allow Membership Service Access:** Yes  
6.  Finally click 'Submit' at the bottom of the form.
![](../assets/lti_how_to/blackboard_accept_lti_tool.png)

## 4) Setup A Placement For The Tool

1.  Now that the tool is added to Blackboard, find it in the table on the 'LTI Tool Providers' table. Hover over it's name and click on the small 'down arrow' to the right of the name. Then select 'Manage Placements'  
    ![](../assets/lti_how_to/blackboard_edit_placements_combined.png)
2.  Click the 'Create Placement' button on the resulting Manage Placements page.  
    ![](../assets/lti_how_to/blackboard_create_placement.png)
3.  Fill in the Placement form with the following information:
    *   **Label:** `Your Tool Name`
    *   **Description:** `Your Tool Description`
    *   **Handle:** `YourToolName`
    *   **Availability:** `Yes`
    *   **Type:**`Deep Linking content tool (+ Allow student access)` <- _If you are using the LTIAAS deep linking service._
    *   **Type:**`Course content tool (+ Allows grading)` <- _If you are using the LTIAAS assignments and grades service._
    *   **Type:**`Course tool (+ Allow student access)` <- _If you are using a basic LTIAAS launch._
    *   **Target Link URI:** `https://your.ltiaas.com` Then click 'Submit'  
    ![](../assets/lti_how_to/blackboard_create_placement_form.png)

## 5) Do A Test Launch

1.  In Blackboard, go to a course and then select 'Content' in the left hand menu.  
2.  In the Content Page, select 'Build Content' -> 'Your Tool Name'  
    ![](../assets/lti_how_to/blackboard_create_content.png)
3.  You should then see the app running inside blackboard.

4.   If you get an error similar to 'The Tool Provider has been disabled by the System Administrator', there are a couple things you should check:

    1.  Navigate to 'System Admin' -> 'Integrations' -> 'LTI Tool Providers' and validate that the tool's State is 'Approved'. If it is not, click on the small down arrow next to the tool name and click 'Approve' ![](../assets/lti_how_to/blackboard_approve_placement.png)
    2.  Navigate to 'System Admin' -> 'Tools and Utilities' -> 'Tools' and search for a section labeled 'LTI'. Make sure LTI is enabled.  
    ![](../assets/lti_how_to/blackboard_lti_enable.png)
