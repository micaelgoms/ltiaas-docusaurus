
This page outlines some of the LMS prerequisites needed before LTIAAS can be tested and used with an LMS.

## 1. A Publicly Accessible Domain Name

LTIAAS uses the internet to send and receive messages to the LMS. This requires LMSes to have publicly accessible domain names. If you are hosting an LMS on a personal machine (i.e. your laptop), that is not accessible from the internet, LTIAAS will not work.

The best option to access an LMS, even for testing purposes, is to host one properly on the internet. If this is not an option, you maybe be able to use the [ngrok](https://ngrok.com/) service to expose a private machine to the internet for testing purposes.

## 2. SSL

The LTI protocol relies in HTTPS for security. LTIAAS will refuse all insecure connections to LMSes. If an LMS doens't support HTTPS, if an SSL certificate has expired, or if an SSL certificate is self-signed, the connection will be denied by LTIAAS.

[Let's Encrypt](https://letsencrypt.org/) is a good resource to generate trusted SSL certificates for free.

## 3. Bitnami Canvas Caution

At LTIAAS, we have had many customers come to us telling us that they aren't able to get LTIAAS to work with Canvas. This, so far, has always been the case with using the [Canvas LMS Packaged By Bitnami](https://aws.amazon.com/marketplace/pp/prodview-fvjvjp65tfujm) on AWS.

The image provided by Bitnami doesn't have the provisions required to use LTI. There are some modifications that need to be made to get the image working. [This guide](https://community.canvaslms.com/t5/Canvas-Developers-Group/Canvas-LTI-1-3-Error-Unknown-Key-Type/m-p/390285/highlight/true#M6345) explains more about what changes are needed to get Canvas working.

If Canvas testing is required and getting the Bitnami image working is not bearing fruit, we recommend reaching out to the Canvas partner team to [register as a partner](https://www.instructure.com/become-integration-services-partner). Using this program, you will get access to your own Canvas test instance hosted by Instructure. It is actually about the same annual price as running a Bitnami instance on AWS.

## Accessing A Test LMS

Many of our customers want to test LTIAAS before releasing it to their customers, but find Canvas and Blackboard LMS pricing prohibitive.

- One option is to test on the public [Moodle sandbox](https://sandbox.moodledemo.net/). It allows LTI tools to be installed, but be warned that it gets reset every 24 hours.

- Brightspace now has a [developer sandbox](https://devcop.brightspace.com/d2l/login) that appears to have recently enabled support LTI 1.3.

- LTIAAS can host private Moodle servers that can be made available on a monthly bases to our customers for testing. Feel free to [contact us](https://ltiaas.com/contact-us) for more information.