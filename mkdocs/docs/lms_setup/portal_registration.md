## Registering Your First Platform
The LTIAAS API isn't very useful unless you have at least one registration. A registration creates the collateral needed to securely connect one LTI tool with one LTI LMS. Each LMS can register with multiple tools, and each tool can register with multiple LMSs.

The registration process happens in this order:

1. The LTI tool is registered into the LMS using LMS-specific instructions.
2. The LMS provides a unique `client ID` for that registration.
3. The `client ID` is used to registration the LMS with the tool.

This article breaks down this registration process and explains how to do it using LTIAAS.

### Dynamic Registration - The Easy Way Out
The easiest way to register a tool with an LMS is with dynamic registration.
Currently, the [Dynamic Registration Specification v1.0](https://www.imsglobal.org/spec/lti-dr/v1p0) is in Candidate Final status. Some LMSs have already implemented it: Moodle (_3.10 or higher_), and BrightSpace.

Dynamic registration automates the registration process by exposing a single [Dynamic Registration Endpoint](https://ltiaas.com/docs/lms_setup/?h=dynamic+registration#dynamic-registration) that is provided to the LMS. To use this feature, the `Dynamic Registration` option must be chosen when creating the account.

Each LMS has a different place where dynamic registration is initiated. But the same Dynamic Registration URL can be used for all LMSs that have the feature implemented.

### Manual Registration
For LSMs that do not support dynamic registration (i.e. canvas and older Moodle instances), or LTIAAS accounts without the dynamic registration feature, the registration process must be done manually.

#### Step 1. Register The LTIAAS Tool Into The LMS
The [LTIAAS documentation](https://ltiaas.com/docs/lms_setup/) has a great guide that explains how to initiate the registration from within the most popular LMSs.

#### Step 2. Register The LMS Into the LTIAAS Tool
When step 1 was completed inside the LMS, a `client ID` should have been provided as a result. The LMS will also provide the necessary endpoints required to register the LMS into the tool.

A registration can be done through the LTIAAS RESTful API using the [`/api/platforms` endpoint](https://ltiaas.com/docs/api_documentation/#register-platform). Alternatively, the registration can be done from within the [LTIAAS Portal](https://portal.ltiaas.com).

Start by logging in to your account and navigating to the `Registrations` page. Then click on the `+ Add Registration` button.

![](../assets/portal/add-registration.png)

Enter the information that the LMS provides for its LTI endpoints. The example below shows what a production Canvas setup might look like.
Note that the `clientID` field in this form should be filled with the client ID provided by the LMS in step 1.

Click submit what you have made the necessary changes. Also, you can edit these values at any time.

![](../assets/portal/portal-registration-form.png)

#### Step 3. (Optional) Edit The Registration
If at any time you want to edit or delete a registration, simply click on the blue pencil icon on the leftmost column of the table for the registration item you want to change.

By default, all new registrations are activated upon creation. There may be an instance where a platform was registered, but you don't want the registration to be active. You can activate/deactivate a registration by clicking on the `true/false` button on the *Active* column of the table.

![](../assets/portal/registration-table.png)

#### Step 4. Do a Test Launch
Once the platform is registered (and activated) you can now go to the LMS and to a trial launch of the tool. The LMS will have a way to create a new assignment of place the tool in the LMS. Once this is done, you should be able to click the link that the LMS created that links to your tool. If everything went well, you should see the `launch URL` that you configured in the portal displayed within the LSM.
