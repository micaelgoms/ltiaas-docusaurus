# Moodle - Dynamic Registration

## Summary

_Dynamic Registration is available in version 3.10 or higher of Moodle._

> Make sure that Dynamic Registration for your LTIAAS API is enabled during the LTIaaS onboarding process.

To set up LTIAAS with Moodle using dynamic registration you need to:

1.  [Add a new 'LTI Advantage' Tool in your Moodle installation](#1-add-a-new-lti-advantage-tool-in-your-moodle-installation)
2.  [Do a test launch](#2-do-a-test-launch)

## 1) Add a new 'LTI Advantage' Tool in your Moodle installation

1.  Login to your Moodle installation as an administrator.
2.  Navigate to 'Site administration' -> 'Plugins' -> 'Activity modules' -> 'External tool' -> 'Manage tools'
3. In the `Tool Url` field enter: `https://your.ltiaas.com/register`, where `your` is the custom subdomain set up in the LTIAAS portal.
4. Click the `Add LTI Advantage` button 

![Enter the Tool's Dynamic Registration URL](../assets/lti_how_to/moodle-dynamic-register.png)

Once the tool is registered, you can view and activate/deactivate the registration in the LTIAAS portal.


## 2) Do A Test Launch

1.  In the Moodle installation, go to a course and turn editing on.
2.  Press the "Add and activity or resource" link in one of the section of the course.
3.  Select the newly created tool.
4.  Give the Activity a name, set any other options to your liking, and click 'Save and display'
5.  If the setup was successful, you should see the new tool launch successfully.