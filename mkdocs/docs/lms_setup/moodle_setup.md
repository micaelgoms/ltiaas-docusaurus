# Moodle - Manual Registration

## Summary

_Manual registration is available in version 3.9 or higher of Moodle._

To set up LTIAAS with Moodle you need to:

1.  [Create a new tool in the Moodle installation](#1-create-a-new-external-tool-in-your-moodle-installation)
2.  [Register the Moodle installation inside LTIAAS](#2-register-the-moodle-installation-inside-ltiaas)
3.  [Do a test launch](#3-do-a-test-launch)

## 1) Create a new 'External Tool' in your Moodle installation

1.  Login to your Moodle installation as an administrator.
2.  Navigate to 'Site administration' -> 'Plugins' -> 'Activity modules' -> 'External tool' -> 'Manage tools'
3.  Click the 'configure a tool manually' link  
    ![](../assets/lti_how_to/moodle_manual_tool.png)
4.  In the resulting form. configure the tool with the following information:
    *   **Tool name:** `Your Tool Name`
    *   **Tool URL:** `https://your.ltiaas.com/`
    *   **Tool description:** `Your Tool Description`
    *   **LTI version:** `LTI 1.3`
    *   **Public key type:** `Keyset URL`
    *   **Public keyset:** `https://your.ltiaas.com/keys`
    *   **Initiate login URL:** `https://your.ltiaas.com/login`
    *   **Redirection URI(s):** `https://your.ltiaas.com/`
    *   **Tool configuration usage:** `Show in activity chooser as a preconfigured tool`
    *   **Default launch container:** _(your choice)_
    *   **Supports Deep Linking (Content-Item Message):** `CHECKED` <- _This is needed if the LTIAAS deep linking service is going to be used.Otherwise leave it unchecked._
    *   **Content Selection URL:** _(left blank)_
    > NOTE: When using LTIAAS, the Content Selection URL should be left blank. This URL is configured within the LTIAAS setup.
    *   **Icon URL:** `https://your-webiste.com/<icon.png>`
    *   **Secure icon URL:** `https://your-website.com/<icon.png>`
    *   **IMS LTI Assignment and Grade Services:**`Use this service for grade sync and column management` <- _This is needed if the LTIAAS assignments and grades service is going to be used._
    *   **IMS LTI Names and Role Provisioning:** `Use this service to retrieve members' information as per privacy settings`  <- _This is needed if the LTIAAS names and roles service is going to be used._
    *   **Tool Settings:** Use this service
    *   **Share launcher's name with tool:** `Delegate to teacher (recommended)`
    *   **Share launcher's email with tool:** `Delegate to teacher (recommended)`
    *   **Accept grades from the tool:** `Delegate to teacher (recommended)`
    *   **Force SSL:** `CHECKED`
![](../assets/lti_how_to/moodle_external_tool_1.png)
![](../assets/lti_how_to/moodle_external_tool_2.png)
![](../assets/lti_how_to/moodle_external_tool_3.png)
5.  Click the 'Save changes' button at the bottom when the form is filled. You can always go back and change this form later if you need to.
6.  After saving, find the tool on the list of tools and click the button that has 4 horizontal lines.  
    ![](../assets/lti_how_to/moodle_external_tool_box.png)
7.  A dialog should pop up that shows the information hat you need to provide to LTIAAS. Save this for the next step.  
    ![](../assets/lti_how_to/moodle_external_tool_details.png)

## 2) Register The Moodle Installation Inside LTIAAS

Follow the steps on the [Registering Your First Platform](portal_registration.md) page. Use the table below to help translate the information provided by the LMS to values needed in the LTIAAS registration process.

LTIAAS Registration API Name | Moodle Provided Name
----------- | ---------------
**name**  | _Any name you would like that helps you identify your LSM_
**clientId**   | _Client ID_
**url** | _Platform ID_
**authConfig.method** | `JWK_SET`
**authConfig.key** | _Public Keyset URL_
**accesstokenEndpoint** | _Access Token URL_
**authenticationEndpoint** | _Authentication Request URL_

## 3) Do A Test Launch

1.  In the Moodle installation, go to a course and turn editing on.
2.  Press the "Add and activity or resource" link in one of the section of the course.
3.  Select the newly created tool.
4.  Give the Activity a name, set any other options to your liking, and click 'Save and display'
5.  If the setup was successful, you should see the new tool launch successfully.

