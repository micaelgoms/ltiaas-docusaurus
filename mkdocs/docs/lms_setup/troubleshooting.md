
This guide covers some of the more common issues you may run into when connecting an LMS with LTIAAS.

Have an issue that you think should be covered here? [Let us know!](https://ltiaas.com/contact-us)

## Error 500

`Error 500` messages are notoriously difficult to debug because they often don't contain much useful information. There can be many many causes for this, but the most common is an invalid or missing SSL certificate.

A quick test that can give more information about this error is a `curl` command getting the `url` of the registered LMS.

```log
curl https://lms.mycompany.com
```

Below is a sample output where it can be seen that there is an SSL issue with this LMS.
```log
curl: (60) SSL certificate problem: self signed certificate
More details here: https://curl.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```

## Timeout During Launch

On rare occasions, the LTI launch process will timeout int he web browser, or hang indefinitely. This is often an issue with firewalls. Some schools have strict firewalls that block domains that aren't on their whitelist. This should be debugged with the following steps:

1. Ask the LMS owner to prove that they can access ltiaas.com:
    1. Access [https://your.ltiaas.com/keys](https://your.ltiaas.com/keys) in a web browser from within the LMS owner's network.
    2. From the LMS server itself, run a curl command to ensure no firewall is in place on the server:

        ```
        curl https://your.ltiaas.com/keys
        ```

2. [Ask LTIAAS](https://ltiaas.com/contact-us) to check access to the LMS form LTIAAS servers. Sometimes schools block inbound traffic from the internet (including LTIAAS). LTIAAS needs to send LTI messages to the LMS directly. Provide the LMS urls that you would like us to check. If LTIAAS believes that there is a firewall in place, we'll let you know so you can work with the LMS provider to resolve the issue.

>  _your.ltiaas.com_ is the subdomain your LTIAAS API is hosted on.
