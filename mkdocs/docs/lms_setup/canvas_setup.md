
# Canvas - Manual Registration

## Summary

To set up LTIAAS with Canvas you need to:

1.  [Add a developer LTI Key in Canvas](#1-add-a-developer-key-in-canvas)
2.  [Install the LTIAAS tool into Canvas](#2-install-the-ltiaas-tool-into-canvas)
3.  [Register the Canvas provided details into LTIAAS](#3-register-the-canvas-provided-details-into-ltiaas)
4.  [Do a test launch](#4-do-a-test-launch)

## 1) Add a Developer Key in Canvas

1.  Login to your Canvas LMS as an administrator.
2.  Navigate to 'Admin' -> [your account name] -> 'Developer Keys'
3.  Click the '+ Developer Key' button followed by the '+ LTI Key' dropdown  
    ![](../assets/lti_how_to/canvas_developer_keys.png)
4.  In the resulting form, enter the following information:
    *   **Key name:** `Your Tool Name`
    *   **Method:** `Enter URL`
    *   **Redirection URI(s):** `https://your.ltiaas.com/`
    *   **JSON URL:** `https://your.ltiaas.com/register/canvas`
5.  Click the 'Save' button at the bottom when the form is filled.  
    ![](../assets/lti_how_to/canvas_key_settings.png)
6.  After saving, find the newly registered tool's key on the list of keys and make sure the State is switched to 'ON'
7.  Note the number in the details column. It will be used as the `Client ID` in the next steps  
    ![](../assets/lti_how_to/canvas_developer_keys_on.png)

## 2) Install The LTIAAS Tool into Canvas

1.  In the Canvas Admin menu, navigate to 'Settings' -> 'Apps'
2.  Click the '+ App' button  
    ![](../assets/lti_how_to/canvas_add_app.png)
3.  In the resulting form choose:
    *   **Configuration Type:** `By Client ID`
    *   **Client ID:** `[The Client ID number you found in the previous series of steps]`
4.  Click the 'Submit' button  
    ![](../assets/lti_how_to/canvas_add_app_form.png)
5.  In the resulting dialog, click 'Install'  
    ![](../assets/lti_how_to/canvas_install.png)
6.  The tool will now show up in the 'External Apps' table.

## 3) Register The Canvas Provided Details Into LTIAAS 

Follow the steps on the [Registering Your First Platform](portal_registration.md) page. Use the table below to help translate the information provided by the LMS to values needed in the LTIAAS registration process.

<table>
  <tr>
    <th>LTIAAS Registration API Name</th>
    <th>Canvas Provided Name (Instructure Cloud)</th>
    <th>Canvas Provided Name (Self Hosted)</th>
  </tr>
  <tr>
    <td><b>name</b></td>
    <td colspan="2" style="text-align:center"><i>Any name you would like that helps you identify your LSM</i></td>
  </tr>
  <tr>
    <td><b>clientId</b></td>
    <td colspan="2" style="text-align:center"><i>The client ID found in the first sequence of steps</i></td>
  </tr>
  <tr>
    <td><b>url</b></td>
    <td style="text-align:center"><i>https://canvas.instructure.com</i></td>
    <td style="text-align:center"><i>https://[YOUR_CANVAS_URL]</i></td>
  </tr>
  <tr>
    <td><b>authConfig.method</b></td>
    <td colspan="2" style="text-align:center"><pre>JWK_SET</pre></td>
  </tr>
  <tr>
    <td><b>authConfig.key</b></td>
    <td colspan="2" style="text-align:center"><i>https://[YOUR_CANVAS_URL]/api/lti/security/jwks</i></td>
  </tr>
  <tr>
    <td><b>accesstokenEndpoint</b></td>
    <td colspan="2" style="text-align:center"><i>https://[YOUR_CANVAS_URL]/login/oauth2/token</i></td>
  </tr>
  <tr>
    <td><b>authenticationEndpoint</b></td>
    <td colspan="2" style="text-align:center"><i>https://[YOUR_CANVAS_URL]/api/lti/authorize_redirect</i></td>
  </tr>
</table>

> Replace **[YOUR_CANVAS_URL]** with the base URL of your canvas instance. For example: _canvas.my-university.edu_ or _canvas.instructure.com_. More information about the Canvas LTI 1.3 endpoints can be found on **[Instructure's website](https://canvas.instructure.com/doc/api/file.lti_dev_key_config.html)**.

## 4) Do A Test Launch

> Note that you may need to **[add placements](https://community.canvaslms.com/t5/Instructor-Guide/How-do-I-configure-a-manual-entry-external-app-for-a-course/ta-p/1137#:~:text=To%20edit%20the%20app%2C%20click%20the%20Edit%20link%20%5B2%5D.%20To%20manage%20where%20the%20app%20displays%20in%20Canvas%2C%20click%20the%20Placements%20link%20%5B3%5D.%20To%20delete%20the%20app%2C%20click%20the%20Delete%20link%20%5B4%5D)** for the newly installed tool before adding it to certain places within the LMS.

1.  Follow the instructions on the Canvas website to add an external tool assignment to a course: [How do I add an assignment using an external app?](https://community.canvaslms.com/t5/Instructor-Guide/How-do-I-add-an-assignment-using-an-external-app/ta-p/656)
2.  When choosing an external tool, choose the newly registered tool from step 1.
