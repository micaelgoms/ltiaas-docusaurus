
- **Tool** - LTI® 1.3 compliant Learning Resources.
- **Platform** - Learning Management System (LMS) capable of consuming LTI® 1.3 compliant Learning Resources.
- **Tool Link** - Instance of a **Tool** inside of a **Platform**. A Tool Link can take many forms, but the most common one is the Activity.
- **Launch context** - Information about the entire context surrounding the launch, usually data identifying the User, Platform, Course and Activity.
- **Platform context** - Platform context references the wider scope in which a launch took place, usually a Course.
- **Membership** - User information. Always contains the user's id and roles inside the current Platform context, but can be extended to contain information like the user's name and email. More information about User memberships can be found in the [IMS Membership specification](https://www.imsglobal.org/spec/lti-nrps/v2p0#membership-container-media-type).
- **Line Item** - Grade line contained in the current Platform context (usually the current Course).