## Summary

With LTIAAS it is possible to use your own custom domain/subdomain. Instead of having an LTIAAS subdomain for your API like `mycompany.ltiaas.com` you can enable the _Custom Subdomain_ option to make LTIAAS accessible through a domain you own like `lti.mycompany.com`.

This page outlines the steps needed to enable this feature.

1. [Choose The Custom Domain Option During Account Setup](#1-choose-the-custom-domain-option-during-account-setup)
2. [Point Your Subdomain To LTIAAS Servers](#2-point-your-subdomain-to-ltiaas-servers)
2. [Enter Your Chosen Domain/Subdomain into the LTIAAS API Settings](#3-enter-your-chosen-domainsubdomain-into-the-ltiaas-api-settings)
2. [Verify The New DNS Setting Within LTIAAS](#4-verify-the-new-dns-setting-within-ltiaas)
2. [Deploy](#5-deploy)

## 1) Choose The Custom Domain Option During Account Setup

In order to enable this feature, it must be selected when [configuring your plan](account_setup.md#step-4-create-your-plan).

There will be an option called **Custom Subdomain** that needs to be selected.
![custom subdomain portal option](assets/portal/custom-subdomain.png)

> If you want to add a custom subdomain to an already existing account, you can do so as long as the account doesn't have an already provisioned LTIAAS subdomain. This can be done by going to _Billing -> Change Subscription Plan_. If a subdomain has already been provisioned, **[contact us](mailto:support@ltiaas.com)** and it can be resolved by LTIAAS staff.

## 2) Point Your Subdomain To LTIAAS Servers

1. Login to your DNS or hosting provider and navigate to the DNS section. This may be called 'DNS', ‘Networking’, or ‘Zone Editor’ depending on your provider. 
2. Add a new `A` record that points a subdomain to the LTIAAS IP address: `143.198.245.249`. In the screenshot below, a record is created for lti.mycompany.com, but there is no restriction on the subdomain name.
![add DNS record](assets/portal/dns-record.png)
3. Wait up to 24 hours for the new DNS changes to propagate through the internet. We typically see the changes take effect in less than an hour, but in some cases it can take longer.

## 3) Enter Your Chosen Domain/Subdomain into the LTIAAS API Settings

When going through the [account setup](account_setup.md#step-5-set-your-api-settings) process, enter your chosen domain/subdomain name in the `Subdomain` field and save the settings.

## 4) Verify The New DNS Setting Within LTIAAS

In the [LTIAAS portal](https://portal.ltiaas.com), on the `API settings` page, LTIAAS will attempt to do a DNS lookup to make sure the `A` record you created is pointing to LTIAAS correctly. If it is, you will be able to [deploy your API](account_setup.md#step-6-deploy). If it is not, you will get a message as shown below.

![DNS Incorrect Message](assets/portal/dns-incorrect-message.png)

Clicking the `Retry` button or refreshing the page, will re-check the DNS settings.

## 5) Deploy

Once the DNS is correct, you will be able to [deploy your API](account_setup.md#step-6-deploy) by clicking the Deploy button on the API settings page.
