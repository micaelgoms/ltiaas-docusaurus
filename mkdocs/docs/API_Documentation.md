
Through the **LTIaaS** API it's possible to access all of the LTI® 1.3 services and functionalities.

## Endpoints

**LTIaaS** has two types of endpoints:

### Basic endpoints

The Base LTI endpoints are the ones that don't implement a LTI® 1.3 service and are usually not directly accessed by the Tool:

- **/** - The base route is used to receive LTI® launches and redirect to the endpoints defined during the LTIaaS onboarding process.
- **/login** - Used to receive login initiation requests at the start of the LTI® launch process.
- **/keys** - Public keyset route, used by the Platforms to validate signed messages. Returns the public JWK to each registered Platform.
- **/register** - Endpoint used to start Dynamic Registration Flow.
- **/register/canvas** - Endpoint used to retrieve Canvas Registration JSON.


### LTIaaS API endpoints

The LTIaaS API endpoints are the ones that implement the LTI® 1.3 services and are accessed directly by the Tool:

- **/idtoken [GET]** - Retrieve IdToken for the current launch context.
- **/memberships [GET]** - Retrieve a list of user memberships inside the current Platform context.
- **/lineitems [GET, POST]**  - Retrieve and create line items.
  - **/:lineitemId [GET, PUT, DELETE]** - Retrieve, update and delete a line item.
    - **/scores [GET, POST]** - Retrieve or Publish scores to a line item.
- **/deeplinking** - Create Deep Linking message or form.
  - **/message [POST]** - Create Deep Linking signed JWT containing Content Items chosen.
  - **/form [POST]** - Create Deep Linking self submitting form containing Content Items chosen.
- **/platforms [GET, POST]** - Retrieve or create Platform registrations.
  - **/:platformID [GET, PUT, DELETE]** - Retrieve, update or delete a Platform registration. 
    - **/:activate [POST]** - Activates dynamically registered Platform.
    - **/:deactivate [POST]** - Dectivates dynamically registered Platform. 

These routes are accessed with the prefix `/api`.

## Authentication

After a successful LTI® Launch is completed, **LTIaaS** will redirect the user to the URL registered during your onboarding configurations. **LTIaaS** will also append a `ltik` query parameter, a signed JWT identifying the current launch context.

*LTIaaS launch flow:*

<div align='center'><img alt='LTIaaS Launch Flow' width="800" src='../assets/launch.png'/></div>



This token **MUST be returned whenever the Tool wants to access any of the** [LTIaaS API endpoints](#LTIaaS-api-endpoints).

*LTIaaS API access flow:*

<div align='center'><img alt='LTIaaS Service Access Flow' width="800" src='../assets/access.png'/></div>


The `ltik` token can be passed to the API through the **LTIK-AUTH-V1** Authorization header.


### LTIK-AUTH-V1 Authorization header

**LTIaaS** implements it's own authorization schema `LTIK-AUTH-V1`:

> Authorization: LTIK-AUTH-V1 Token=\<ltik\>, Additional=Bearer \<API_KEY\>

- **Token** - Ltik authentication token.
- **API_KEY** - API Key generated for your LTIaaS instance.

Example:

> Authorization: LTIK-AUTH-V1 Token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c, Additional=Bearer KxwRJSMeKKF2QT4fwpM

### The Platforms endpoint

The only LTIaaS API endpoint that does not require the `ltik` to be passed is **/api/platforms**, since this endpoint should be accessible outside of a LTI® Launch.

In order to access the **Platforms** endoint you must use a **Bearer Authorization** header passing the API Key generated for your LTIaaS instance.

> Authorization: Bearer \<API_KEY\>


Example:

> Authorization: Bearer KxwRJSMeKKF2QT4fwpM



## Launch flow

The Core Launch Service in **LTIaaS** works similarly to a standard LTI® Launch:

- The **LTIaaS** server will receive the launch on behalf of the Tool;
- Validate the IdToken and store it in the database;
- Redirect the user to the Tool's Launch URL, configured during the LTIaaS onboarding process, passing the `ltik` representing the current launch context.

## Deep Linking flow

The Deep Linking flow starts in the same way as a regular launch, but the user is redirected to a resource selection endpoint instead:

- The **LTIaaS** server will receive the deep linking launch on behalf of the Tool;
- Validate the IdToken and store it in the database;
- Redirect the user to the Tool's Deep Linking URL, configured during the LTIaaS onboarding process, passing the `ltik` representing the current launch context.
- Tool displays the resource selection screen.
- Tool makes a request to the [deep linking endpoint](#deep-linking-endpoint) with the selected resources.
- **LTIaaS** responds with the generated self-submitting form.
- Tool appends form to HTML body.
- Form self submits and finalizes deep linking process.  


## IdToken Endpoint

The **/api/idtoken** endpoint is used to retrieve an IdToken object containing the current launch context information.

### Retrieve IdToken

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/idtoken` | `GET` | ✔️ |

#### Parameters

The **/api/idtoken** endpoint receives no parameters.

#### Response

Returns an `IdToken` object containing the entire launch context information. 

The contents of the object may vary, since Platforms might return different information, but the information is divided into easy to understand categories:

- **user** - User Information.
    - id - User ID.
    - email - User email.
    - given_name - User given name.
    - family_name - User family name.
    - name - User name.
    - roles - User roles in the current context.
- **platform** - Platform information.
    - id - Platform ID.
    - url - Platform URL.
    - clientId - Platform generated Client ID.
    - deploymentId - Platform generated Deployment ID.
    - product_family_code - Platform family code.
    - version - Platform version.
    - name - Platform name.
    - description - Platform description.
- **launch** - Launch context information.
    - type - Launch type.
    - target - Launch target URL.
    - context - Course or section information
        - id
        - label
        - title
        - type
    - resource - Activity or placement information.
        - id
        - title
    - presentation - Information regarding how the Tool is being displayed in the Platform.
        - locale
        - document_target
    - custom - Custom parameters object.
    - lineItemId - Line item assigned to this resource. This field is only present if there is exactly one line item assigned to this resource. Can be used to quickly access the [LineItems endpoint](#lineitems-endpoint) to [submit](#submit-scores) or [retrieve scores](#retrieve-scores).
  
  




Example response:

```javascript
{
  user: {
    id: '2',
    given_name: 'Admin',
    family_name: 'User',
    name: 'Admin User',
    email: 'admin@lms.example.com',
    roles: [
      'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator',
      'http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor',
      'http://purl.imsglobal.org/vocab/lis/v2/system/person#Administrator'
    ]
  },
  platform: {
    id: '0c41aa0849215449d4298e58f7626c68',
    url: 'https://lms.example.com',
    clientId: 'CLIENTID',
    deploymentId: '1',
    product_family_code: 'canvas',
    version: '2020073000',
    guid: 'lms.example.com',
    name: 'LMS',
    description: 'LMS',
    lis: {
      person_sourcedid: '',
      course_section_sourcedid: ''
    },
    deepLinkingSettings: null
  },
  launch: {
    type: 'LtiResourceLinkRequest',
    target: 'http://tool.example.com?resource=value1',
    context: {
      id: '2',
      label: 'course',
      title: 'Course',
      type: [
          'CourseSection'
        ]
    },
    resource: {
      title: 'Activity',
      id: '1'
    },
    presentation: {
      locale: 'en',
      document_target: 'iframe',
      return_url: 'https://lms.example.com/mod/lti/return.php?course=2&launch_container=3&instanceid=1&sesskey=huxFIyJ0BP'
    },
    custom: {
      group: '2',
      system_setting_url: 'https://lms.example.com/mod/lti/services.php/tool/1/custom',
      context_setting_url: 'https://lms.example.com/mod/lti/services.php/CourseSection/2/bindings/tool/1/custom',
      link_setting_url: 'https://lms.example.com/mod/lti/services.php/links/{link_id}/custom'
    },
    lineItemId: 'https://lms.example.com/mod/lti/services.php/2/lineitems/2/lineitem?type_id=1'
  }
}
```

Since the idtoken receives varied information depending on the LMS, fields don't follow a strict naming convention. Fields not using camel case are usually the ones coming directly from the LMS without any modification.

#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const idtoken = await request.get('https://your.ltiaas.com/api/idtoken', { headers: { Authorization: authorizationHeader } }).json()
return idtoken
```

## Deep Linking Endpoint

The **/api/deeplinking** endpoints are used to create Tool Links in the Platform.

### Create Deep Linking form

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/deeplinking/form` | `POST` | ✔️ |

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `contentItems` | `Array` | `body` | ✔️ |
| `options` | `Object` | `body`  |   |

**contentItems:** An array of resources formatted as Content Items following the [IMS Content Item specification](https://www.imsglobal.org/spec/lti-dl/v2p0/#content-item-types).
The most widely used type of contentItem is “ltiResourceLink”, the Tool Link.

**options:** An object containing one or more of the following fields:

- **options.message** - Message displayed to the user if the deep linking process is successfully completed.
- **options.errMessage** - Message displayed to the user if the deep linking process finishes with an error.
- **options.log** - Log generated by the Platform if the deep linking process is successfully completed.
- **options.errLog** - Log generated by the Platform if the deep linking process finishes with an error.




#### Response

Returns a self-submitting form with the signed JWT containing the selected resources that must be appended to the HTML body.

Example response:

```javascript
{ 
  form: '<form id="ltijs_submit" style="display: none;" action="https://ltiadvantagevalidator.imsglobal.org/ltitool/deeplinkresponse.html" method="POST"><input type="hidden" name="JWT" value="eyJhbGciOiJIUzIVCJ9.eyJzdWIbmFtZSI6IG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKV_adQssw5c" /></form><script>document.getElementById("ltijs_submit").submit()</script>' 
}
```

#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const link = {
  contentItems: [{
    type: 'ltiResourceLink',
    url: 'https://tool.com?resourceid=123456',
    title: 'Resource'
  }],
  options: { 
    message: 'Deep Linking successful!',
    log: 'deep_linking_successful',
    errMessage: 'Deep Linking failed!',
    errLog: 'deep_linking_failed'
  }
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const response = await request.post('https://your.ltiaas.com/api/deeplinking/form', { json: link, headers: { Authorization: authorizationHeader } }).json()
return response.form

// Append self-submitting form to HTML body in the client side
$('body').append(form)
```

### Create Deep Linking Message

If you want to build your own self-submitting form, it is also possible to create only the signed JWT message.

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/deeplinking/message` | `POST` | ✔️ |

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `contentItems` | `Array` | `body` | ✔️ |
| `options` | `Object` | `body`  |   |

**contentItems:** An array of resources formatted as Content Items following the [IMS Content Item specification](https://www.imsglobal.org/spec/lti-dl/v2p0/#content-item-types).
The most widely used type of contentItem is [ltiResourceLink](https://www.imsglobal.org/spec/lti-dl/v2p0/#lti-resource-link), the Tool Link.

**options:** An object containing one or more of the following fields:

- **options.message** - Message displayed to the user if the deep linking process is successfully completed.
- **options.errMessage** - Message displayed to the user if the deep linking process finishes with an error.
- **options.log** - Log generated by the Platform if the deep linking process is successfully completed.
- **options.errLog** - Log generated by the Platform if the deep linking process finishes with an error.


#### Response

Returns a signed JWT containing the selected resources that must be inserted in a self-submitting form and appended to the HTML body, and the endpoint to which this message has to be submitted to in order to finish the Deep Linking flow.

Example response:

```javascript
{ 
  message: 'eyJhbGciOiJIUzIVCJ9.eyJzdWIbmFtZSI6IG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKV_adQssw5c',
  endpoint: 'https://ltiadvantagevalidator.imsglobal.org/ltitool/deeplinkresponse.html'
}
```

#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const link = {
  contentItems: [{
    type: 'ltiResourceLink',
    url: 'https://tool.com?resourceid=123456',
    title: 'Resource'
  }],
  options: { 
    message: 'Deep Linking successful!',
    log: 'deep_linking_successful',
    errMessage: 'Deep Linking failed!',
    errLog: 'deep_linking_failed'
  }
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const response = await request.post('https://your.ltiaas.com/api/deeplinking/message', { json: link, headers: { Authorization: authorizationHeader } }).json()

// Building self-submitting form
const form = '<form id="ltijs_submit" style="display: none;" action="' + response.endpoint + '" method="POST"><input type="hidden" name="JWT" value="' + response.message + '" /></form><script>document.getElementById("ltijs_submit").submit()</script>'
return form

// Append self-submitting form to HTML body in the client side
$('body').append(form)
```


## Memberships Endpoint

The **/api/memberships** endpoint represents the [Names and Roles Provisioning Service](#names-and-roles-provisioning) and is used to retrieve a list of User **memberships**.

### Retrieve memberships

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/memberships` | `GET` | ✔️ |

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `role` | `String` | `query` |   |
| `limit` | `Number` | `query`  |   |
| `url` | `String` | `query`  |   |
| `resourceLinkId` | `Boolean` | `query`  |   |


**role:** Filters memberships by role. Possible role values are defined in the [IMS Role Vocabularies](https://www.imsglobal.org/spec/lti/v1p3/#role-vocabularies).

**limit:** Limits the number of memberships returned.

**url:** Retrieves memberships from a specific URL. 

In cases where not all members are retrieved when the membership limit is reached, the returned object will contain a `next` field holding an URL that can be used to retrieve the remaining members. 

*When present, the `url` parameter causes every other parameter to be ignored.* 

The `url` parameter should be URL encoded.

**resourceLinkId:** Retrieves the Resource Link level memberships as specified in the [IMS Resource Link Membership Service specification](https://www.imsglobal.org/spec/lti-nrps/v2p0#resource-link-membership-service).


#### Response

Returns an object with a **members** field containing an array of User memberships. **Only the `user_id` and `roles` fields of each membership are required to be returned by the Platform**.

More information about User memberships can be found in the [IMS Membership specification](https://www.imsglobal.org/spec/lti-nrps/v2p0#membership-container-media-type).

Other fields present in the response can be:

- **id** - URL the memberships were retrieved from.
- **context** - Context the memberships were retrieved from.
- **next** - URL of the next membership page. **This field is only present if the membership limit was reached before the full members list was retrieved.**

Example response:

```javascript
{
  id : 'https://lms.example.com/sections/2923/memberships',
  context: {
    id: '2923-abc',
    label: 'CPS 435',
    title: 'CPS 435 Learning Analytics',
  },
  next: 'https://lms.example.com/sections/2923/memberships/pages/2',
  members : [
    {
      status : 'Active',
      name: 'Jane Q. Public',
      picture : 'https://platform.example.edu/jane.jpg',
      given_name : 'Jane',
      family_name : 'Doe',
      middle_name : 'Marie',
      email: 'jane@platform.example.edu',
      user_id : '0ae836b9-7fc9-4060-006f-27b2066ac545',
      lis_person_sourcedid: '59254-6782-12ab',
      roles: [
        'http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor'
      ]
    }
  ]
}

```


#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const query = {
  role: 'Learner',
  limit: 10
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const response = await request.get('https://your.ltiaas.com/api/memberships', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
return response.members
```

## LineItems Endpoint

The **/api/lineitems** endpoints represent the [Assignment and Grade Service](#assignment-and-grade) and are used to access and manipulate line items and scores.

### Retrieve line items

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems` | `GET` | ✔️ |

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `id` | `String` | `query` |   |
| `resourceId` | `String` | `query`  |   |
| `tag` | `String` | `query`  |   |
| `label` | `String` | `query`  |   |
| `resourceLinkId` | `Boolean` | `query`  |   |
| `limit` | `Number` | `query`  |   |
| `url` | `String` | `query`  |   |


**id:** Retrieves a specific line item by ID. Line item IDs are URLs so the parameter should be URL encoded.

**resourceId:** Filters line items by resourceId.

**tag:** Filters line items by tag.

**label:** Filters line items by label.

**resourceLinkId:** Filters line items by the resource link ID attached to the current launch context.

**limit:** Limits the number of line items returned.

**url:** Retrieves line items from a specific URL.

In cases where not all line items are retrieved when the line item limit is reached, the returned object will contain a `next` field holding an URL that can be used to retrieve the remaining line items. 

*When present, the `url` parameter causes every other parameter to be ignored.* 

The `url` parameter should be URL encoded.

#### Response

Returns an object with a **lineItems** field containing an array of line items.

More information about line items can be found in the [IMS Line Item Service specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#line-item-service).

Other fields present in the response can be:

- **next** - URL of the next line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**
- **prev** - URL of the previous line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**
- **first** - URL of the first line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**
- **last** - URL of the last line item page. **This field is only present if the line item limit was reached before the full list was retrieved.**

Example response:

```javascript
{
  next: 'https://lms.example.com/sections/2923/lineitems/pages/2',
  first: 'https://lms.example.com/sections/2923/lineitems/pages/1',
  last: 'https://lms.example.com/sections/2923/lineitems/pages/3',
  lineItems: [  
    {
      id: 'https://lms.example.com/context/2923/lineitems/1',
      scoreMaximum: 60,
      label: 'Chapter 5 Test',
      resourceId: 'a-9334df-33',
      tag: 'grade',
      resourceLinkId: '1g3k4dlk49fk',
      endDateTime: '2018-04-06T22:05:03Z'
    },
    {
      id: 'https://lms.example.com/context/2923/lineitems/47',
      scoreMaximum: 100,
      label: 'Chapter 5 Progress',
      resourceId: 'a-9334df-33',
      tag: 'originality',
      resourceLinkId: '1g3k4dlk49fk'
    },
    {
      id: 'https://lms.example.com/context/2923/lineitems/69',
      scoreMaximum: 60,
      label: 'Chapter 2 Essay',
      tag: 'grade'
    }
  ]
}
```


#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const query = {
  tag: 'grade_line',
  label: 'Grade line'
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const response = await request.get('https://your.ltiaas.com/api/lineitems', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
return response.lineItems
```

### Create line item

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems` | `POST` | ✔️ |

#### Parameters

Requests **MUST** have a body representing a Line Item Object as specified in the [IMS Line Item Object Specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#creating-a-new-line-item).

The only required parameters are **label** and **scoreMaximum**.

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `label` | `String` | `body`  | ✔️ |
| `scoreMaximum` | `Number` | `body`  | ✔️ |



**label:** Line item label.

**scoreMaximum:** Maximum score allowed for the line item.



#### Response

Returns the newly created line item.


Example response:

```javascript
{
  id: 'https://lms.example.com/context/2923/lineitems/1',
  scoreMaximum: 60,
  label: 'Chapter 5 Test',
  resourceId: 'quiz-231',
  tag: 'grade',
  resourceLinkId: '1234',
  startDateTime: '2018-03-06T20:05:02Z',
  endDateTime: '2018-04-06T22:05:03Z'
}
```


#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const lineitem = {
  scoreMaximum: 60,
  label: 'Grade line',
  resourceId: 'quiz-231',
  tag: 'grade',
  resourceLinkId: idtoken.launch.resource.id, // Retrieving resource link ID from ID Token.
  startDateTime: '2021-03-06T20:05:02Z',
  endDateTime: '2021-04-06T22:05:03Z'
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const newLineitem = await request.post('https://your.ltiaas.com/api/lineitems', { json: lineitem, headers: { Authorization: authorizationHeader } }).json()
return newLineitem
```

*Setting a resourceLinkId will bind the created line item to a specific resource in the LMS.*

### Retrieve line item by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid` | `GET` | ✔️ |

The line item ID is an URL so it **MUST be URL encoded**.

#### Parameters

The **/lineitems/:lineitemid** GET endpoint receives no parameters.


#### Response

Returns the requested line item.


Example response:

```javascript
{
  id: 'https://lms.example.com/context/2923/lineitems/1',
  scoreMaximum: 60,
  label: 'Chapter 5 Test',
  resourceId: 'quiz-231',
  tag: 'grade',
  startDateTime: '2018-03-06T20:05:02Z',
  endDateTime: '2018-04-06T22:05:03Z'
}
```

#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const lineitemId = 'https://lms.example.com/context/2923/lineitems/1'
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const lineitem = await request.get('https://your.ltiaas.com/api/lineitems/' + encodeURIComponent(lineitemId), { headers: { Authorization: authorizationHeader } }).json()
return lineitem
```

### Update line item by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid` | `PUT` | ✔️ |

The line item ID is an URL so it **MUST be URL encoded**.

#### Parameters

Requests **MUST** have a body representing a Line Item Object as specified in the [IMS Line Item Object Specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#creating-a-new-line-item).

The only required parameters are **label** and **scoreMaximum**.

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `label` | `String` | `body`  | ✔️ |
| `scoreMaximum` | `Number` | `body`  | ✔️ |



**label:** Line item label.

**scoreMaximum:** Maximum score allowed for the line item.


#### Response

Returns the updated line item.


Example response:

```javascript
{
  id: 'https://lms.example.com/context/2923/lineitems/1',
  scoreMaximum: 60,
  label: 'Chapter 5 Updated',
  resourceId: 'quiz-231',
  tag: 'grade',
  startDateTime: '2018-03-06T20:05:02Z',
  endDateTime: '2018-04-06T22:05:03Z'
}
```


#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const lineitemId = 'https://lms.example.com/context/2923/lineitems/1'
const lineitem = {
  scoreMaximum: 60,
  label: 'Chapter 5 Updated'
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const updatedLineitem = await request.put('https://your.ltiaas.com/api/lineitems/' + encodeURIComponent(lineitemId), { json: lineitem, headers: { Authorization: authorizationHeader } }).json()
return updatedLineitem
```

### Delete line item by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid` | `DELETE` | ✔️ |

The line item ID is an URL so it **MUST be URL encoded**.

#### Parameters

The **/lineitems/:lineitemid** DELETE endpoint receives no parameters.

#### Response

Returns `204` if the line item is successfully deleted.


#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const lineitemId = 'https://lms.example.com/context/2923/lineitems/1'
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
await request.delete('https://your.ltiaas.com/api/lineitems/' + encodeURIComponent(lineitemId), { headers: { Authorization: authorizationHeader } })
return true
```

### Submit scores

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid/scores` | `POST` | ✔️ |

The line item ID is an URL so it **MUST be URL encoded**.

#### Parameters

Requests MUST have a body representing a Score Object as specified in the [IMS Score Specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#example-application-vnd-ims-lis-v1-score-json-representation).

The required parameters are **userId**, **activityProgress** and **gradingProgress**. The parameters **timestamp** and **scoreMaximum** are also required but are set automatically by **LTIaaS**.

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `userId` | `String` | `body`  | ✔️ |
| `activityProgress` | `String` | `body`  | ✔️ |
| `gradingProgress` | `String` | `body`  | ✔️ |


**userId:** Target user ID.

**activityProgress:** Status of the user towards the activity's completion.

**gradingProgress:** Status of the grading process.


#### Response

Returns the submitted score.


Example response:

```javascript
{
  userId: '5323497',
  activityProgress: 'Completed',
  gradingProgress: 'FullyGraded',
  scoreGiven: 83,
  scoreMaximum: 100,
  comment: 'This is exceptional work.',
  timestamp: '2021-04-16T18:54:36.736+00:00',
}
```

#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const lineitemId = 'https://lms.example.com/context/2923/lineitems/1'
const score = {
  userId: '5323497',
  activityProgress: 'Completed',
  gradingProgress: 'FullyGraded',
  scoreGiven: 83,
  comment: 'This is exceptional work.',
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const submittedScore = await request.post('https://your.ltiaas.com/api/lineitems/' + encodeURIComponent(lineitemId) + '/scores', { json: score, headers: { Authorization: authorizationHeader } }).json()
return submittedScore
```

### Retrieve scores

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/lineitems/:lineitemid/scores` | `GET` | ✔️ |

The line item ID is an URL so it **MUST be URL encoded**.

#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `userId` | `String` | `query` |   |
| `limit` | `Number` | `query`  |   |
| `url` | `String` | `query`  |   |


**userId:** Filters scores based on the user ID.

**limit:** Limits the number of scores returned.

**url:** Retrieves scores from a specific URL.

In cases where not all scores are retrieved when the score limit is reached, the returned object will contain a `next` field holding an URL that can be used to retrieve the remaining scores. 

*When present, the `url` parameter causes every other parameter to be ignored.* 

The `url` parameter should be URL encoded.


#### Response

Returns an object with a **scores** field containing an array of scores.

More information about scores can be found in the [IMS Result Service specification](https://www.imsglobal.org/spec/lti-ags/v2p0/#result-service).

Other fields present in the response can be:

- **next** - URL of the next score page. **This field is only present if the score limit was reached before the full list was retrieved.**
- **prev** - URL of the previous score page. **This field is only present if the score limit was reached before the full list was retrieved.**
- **first** - URL of the first score page. **This field is only present if the score limit was reached before the full list was retrieved.**
- **last** - URL of the last score page. **This field is only present if the score limit was reached before the full list was retrieved.**


Example response:

```javascript
{
  next: 'https://lms.example.com/sections/2923/scores/pages/2',
  first: 'https://lms.example.com/sections/2923/scores/pages/1',
  last: 'https://lms.example.com/sections/2923/scores/pages/3',
  scores: [
    {
      id: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem/results?type_id=1&user_id=2',
      userId: '2',
      resultScore: 100,
      resultMaximum: 100,
      scoreOf: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem?type_id=1',
      timestamp: '2020-06-02T10:51:08-03:00'
    },
    {
      id: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem/results?type_id=1&user_id=2',
      userId: '3',
      resultScore: 90,
      resultMaximum: 100,
      scoreOf: 'https://lms.example.com/lti/services.php/2/lineitems/16/lineitem?type_id=1',
      timestamp: '2020-06-02T10:51:08-03:00'
    }
  ]
}
```


#### Example usage

```javascript
const searchParams = new URLSearchParams(window.location.search)
const ltik = searchParams.get('ltik')
const lineitemId = 'https://lms.example.com/context/2923/lineitems/1'
const query = {
  userId: 2,
  limit: 10
}
const authorizationHeader = `LTIK-AUTH-V1 Token=${ltik}, Additional=Bearer ${API_KEY}`
const response = await request.get('https://your.ltiaas.com/api/lineitems/' + encodeURIComponent(lineitemId) + '/scores', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
return response.scores
```


## Platforms Endpoint

In order for Tool and Platform to interact with each other, they must be registered inside one another. The **/platforms** endpoints are used to access and manipulate Platform registrations inside the Tool.

None of the **/platforms** endpoints requires a `ltik` to be accessed, so it is **highly recommended** to create an [authentication extension](#authentication-extensions).

For more information regarding Tool and Platform registration check [Registrations](#registrations).

### Retrieve Platforms

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms` | `GET` |   |



#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `id` | `String` | `query` |   |
| `url` | `String` | `query` |   |
| `clientId` | `String` | `query`  |   |
| `name` | `String` | `query`  |   |
| `authenticationEndpoint` | `String` | `query`  |   |
| `accesstokenEndpoint` | `String` | `query`  |   |
| `authConfigMethod` | `String` | `query`  |   |
| `authConfigKey` | `String` | `query`  |   |

**id:** Retrieves a specific Platform by ID.

**url:** Filters line items by the URL.

**clientId:** Filters line items by the client ID.

**name:** Filters line items by name.

**authenticationEndpoint:** Filters line items by the authentication endpoint.

**accesstokenEndpoint:** Filters line items by the access token endpoint.

**authConfigMethod:** Filters line items by the token authentication method.

**authConfigKey:** Filters line items by the token authentication key.


#### Response

Returns an object with a **platforms** field containing an array of Platforms registrations.


Example response:

```javascript
{
  platforms: [
    {
      url: 'https://platform.org',
      clientId: '10000000000001',
      name: 'Platform 1',
      authenticationEndpoint: 'https://platform.org/lti/authorize',
      authConfig: {
        key: 'https://platform.org/lti/security/jwks',
        method: 'JWK_SET'
      },
      accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
      id: 'f33d6c450b7632c1ff87586c4f150eee',
      publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
      active: true
    },
    {
      url: 'https://platform2.org',
      clientId: '20naAWuw8m4SGVH',
      name: 'Platform 2',
      authenticationEndpoint: 'https://platform2.org/mod/lti/auth.php',
      authConfig: {
        method: 'JWK_SET',
        key: 'https://platform2.org/mod/lti/certs.php'
      },
      accesstokenEndpoint: 'https://platform2.org/mod/lti/token.php',
      id: '3e7853607a37ed57c980f05c03b116ed',
      publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
      active: true
    }
  ]
}
```


#### Example usage

```javascript
const query = {
  name: 'Platform 1'
}
const authorizationHeader = `Bearer ${API_KEY}`
const response = await request.get('https://your.ltiaas.com/api/platforms', { searchParams: query, headers: { Authorization: authorizationHeader } }).json()
return response.platforms
```

### Register Platform

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms` | `POST` |   |

#### Parameters


| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `url` | `String` | `body`  | ✔️ |
| `clientId` | `String` | `body`  | ✔️ |
| `name` | `String` | `body`  | ✔️ |
| `authenticationEndpoint` | `String` | `body`  | ✔️ |
| `accesstokenEndpoint` | `String` | `body`  | ✔️ |
| `authConfig` | `Object` | `body`  | ✔️ |
| `authConfig.method` | `String` | `body`  | ✔️ |
| `authConfig.key` | `String` | `body`  | ✔️ |

**url:** Platform URL.

**clientId:** Platform client ID.

**name:** Platform name.

**authenticationEndpoint:** Platform authentication endpoint.

**accesstokenEndpoint:** Platform access token endpoint.

**authConfig:** Platform token authentication configuration object.

**authConfig.method:** Platform token authentication method. **MUST be one of JWK_SET, JWK_KEY or RSA_KEY**.

**authConfig.key:** Platform token authentication key.


#### Response

Returns the newly created Platform.


Example response:

```javascript
{
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  },
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  id: 'f33d6c450b7632c1ff87586c4f150eee',
  publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
  active: true
}
```


#### Example usage

```javascript
const platform = {
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  }
}
const authorizationHeader = `Bearer ${API_KEY}`
const newPlatform = await request.post('https://your.ltiaas.com/api/platforms', { json: platform, headers: { Authorization: authorizationHeader } }).json()
return newPlatform
```

### Retrieve Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid` | `GET` |   |

#### Parameters

The **/platforms/:platformid** GET endpoint receives no parameters.


#### Response

Returns the requested Platform.


Example response:

```javascript
{
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  },
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  id: 'f33d6c450b7632c1ff87586c4f150eee',
  publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
  active: true
}
```

#### Example usage

```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
const platform = await request.get('https://your.ltiaas.com/api/platforms/' + platformId, { headers: { Authorization: authorizationHeader } }).json()
return platform
```

### Update Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid` | `PUT` |   |


#### Parameters

| PARAMETER | TYPE | LOCATION | REQUIRED |
| --- | -- | -- | ---- |
| `url` | `String` | `body`  |   |
| `clientId` | `String` | `body`  |   |
| `name` | `String` | `body`  |   |
| `authenticationEndpoint` | `String` | `body`  |   |
| `accesstokenEndpoint` | `String` | `body`  |   |
| `authConfig` | `Object` | `body`  |   |
| `authConfig.method` | `String` | `body`  |   |
| `authConfig.key` | `String` | `body`  |   |

**url:** Platform URL.

**clientId:** Platform client ID.

**name:** Platform name.

**authenticationEndpoint:** Platform authentication endpoint.

**accesstokenEndpoint:** Platform access token endpoint.

**authConfig:** Platform token authentication configuration object.

**authConfig.method:** Platform token authentication method. **MUST be one of JWK_SET, JWK_KEY or RSA_KEY**.

**authConfig.key:** Platform token authentication key.


#### Response

Returns the updated Platform.


Example response:

```javascript
{
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 3',
  authenticationEndpoint: 'https://platform.org/lti/authorize2',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks2',
    method: 'JWK_SET'
  },
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  id: 'f33d6c450b7632c1ff87586c4f150eee',
  publicKey: '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAymGcob5nh7kKHWCNQ3J6\n4dX5fknKrwCifFtt32ov09IUHWj2WwTSnZOvi43a3IpJ+zCj9dUnW9NF+Axx+s/X\nNUIcJWLAZtf3QkDAChJZeEEsxptFm9bFfKZjXlq1e5XFFetZBgUN0d+KDJYZP8BV\nQ1bAIwRcrwDDqVXdmYJlwfejstSm8oPkW7NJv6HBsqcloJVlwIfl5ltZfAUiKgIP\nuecHkD16ma712VxSDhZALhhtgGNRbur64nfkEWenYjCyECWMEFJBBw+ef2FAR5g/\nWtvzvkHZmoG79p/9eBw1wM09edT/GnXMXrv9CLWaTz7aTrn0GCBSK/K1unBaF8Q4\nbzRDDrbETKSLc9TPbD7xw+RIMOjLnJXUSz/2FfqmJggzzBOnISwQsuFPIK8IaCdS\nM+CHpHuUHMY8cPNgn63HiWEuy9evrny6gAK40pIgCpTGdsf0EYvOYn/kP7wElCDf\nmogRFJKBRH7BN0Syk+aMmt1t4mPP1QTk3HdfUmdZTU3ueeaIJ18m6Kif88wtij4F\n6PZDrs6h5BCEZUtpl78+H030TSm7OrY9SlOPcD/K+4fZlrkiuFc2hhjb1awTHd2I\nGzWZ8ur3Dr7V/Zgdm6Tio33ot8dEhJcKXbC8V1jhiVtewyDGqFSOBTRuhxNIt0/0\n24z6Wb9bMUH/ztaywZjkIesCAwEAAQ==\n-----END PUBLIC KEY-----\n',
  active: true
}
```


#### Example usage

```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const platform = {
  name: 'Platform 3',
  authenticationEndpoint: 'https://platform.org/lti/authorize2',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks2'
  }
}
const authorizationHeader = `Bearer ${API_KEY}`
const updatedPlatform = await request.post(('https://your.ltiaas.com/api/platforms/' + platformId, { json: platform, headers: { Authorization: authorizationHeader } }).json()
return updatedPlatform
```

### Delete Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid` | `DELETE` |   |


#### Parameters

The **/platforms/:platformid** DELETE endpoint receives no parameters.

#### Response

Returns `204` if the Platform is successfully deleted.


#### Example usage


```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
await request.delete('https://your.ltiaas.com/api/platforms/' + platformId, { headers: { Authorization: authorizationHeader } })
return true
```

### Activate Platform by ID

Dynamically registered Platforms have to be activated if the the `Auto Activate Dynamically Registered Platforms` option was turned off during the LTIaaS onboarding process.

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid/activate` | `POST` |   |


#### Parameters

The **/platforms/:platformid/activate** POST endpoint receives no parameters.

#### Response

Returns `200` if the Platform is successfully activated.


#### Example usage


```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
await request.post('https://your.ltiaas.com/api/platforms/' + platformId + '/activate', { headers: { Authorization: authorizationHeader } })
return true
```

More information on the **Dynamic Registration Service** can be found on the [Ltijs Dynamic Registration Documentation](https://cvmcosta.me/ltijs/#/dynamicregistration).

### Deactivate Platform by ID

| ROUTE | METHOD  | LTIK REQUIRED |
| ---- | --- | -- |
| `/api/platforms/:platformid/deactivate` | `POST` |   |


#### Parameters

The **/platforms/:platformid/deactivate** POST endpoint receives no parameters.

#### Response

Returns `200` if the Platform is successfully deactivated.


#### Example usage


```javascript
const platformId = 'f33d6c450b7632c1ff87586c4f150eee'
const authorizationHeader = `Bearer ${API_KEY}`
await request.post('https://your.ltiaas.com/api/platforms/' + platformId + '/deactivate', { headers: { Authorization: authorizationHeader } })
return true
```

More information on the **Dynamic Registration Service** can be found on the [Ltijs Dynamic Registration Documentation](https://cvmcosta.me/ltijs/#/dynamicregistration).



## Errors

To ensure consistency, error responses generated by any of the **LTIaaS** endpoints obey the following pattern:

``` javascript
{
  status: ERROR_STATUS,
  error: ERROR_MESSAGE,
  details: ERROR_DETAILS
}
```

Example:

``` javascript
{
  status: 401,
  error: "Unauthorized",
  details: {
    message: "Missing parameter \"token\"."
    bodyReceived: { 
      param: "value"
    }
  }
}
```
