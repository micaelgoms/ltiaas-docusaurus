
The Learning Tools Interoperability (LTI®) protocol is a standard for integration of rich learning applications within educational environments. <sup>[ref](https://www.imsglobal.org/spec/lti/v1p3/)</sup>

Implementing the LTI® 1.3 protocol and all of it's services can be a very difficult, time consuming task, but using **LTIaaS** any project can be easily turned into a fully LTI® 1.3 compliant learning tool, with minimal alterations to the project's codebase.

**LTIaaS** works as a service that seats between an application and a LMS (like Moodle or Canvas), handling LTI® communications and giving the application access to all of the LTI® services and functionalities through a very simple to use API. 


### LTIaaS Basic Usability Flow

After a successful LTI® Launch is completed, **LTIaaS** will redirect the user to the URL registered during your onboarding configuration. **LTIaaS** will also append an `ltik` query parameter, a signed JWT identifying the current launch context.

*LTIaaS launch flow:*

![Launch Flow](assets/launch.png)


This token can then be used with your **API Key** to access any of the [LTIaaS API Service endpoints](api/endpoints.md#ltiaas-api-service-endpoints).


*LTIaaS Service API access flow:*

![API Service Access Flow](assets/access.png)


