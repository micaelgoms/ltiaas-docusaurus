import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
  
  body {
    background: linear-gradient(180deg, #E6F5FF 0%, #FFFFFF 100%);
    -webkit-font-smoothing: antialiased;
    overflow-x: hidden;
  }

  body, input {
    font: 16px Ubuntu, sans-serif;
    color: #64585C;
    font-style: normal;
    font-weight: 500;
    line-height: 32px;
    font-size: 90%; 

    @media (min-width: 769px) {
      font-size: 100%;
    }
  }

  h1, h2, h3, h4, h5, h6 {
    color: #023E73;
    font-family: Russo One;
    font-style: normal;
    font-weight: normal;
    line-height: 48px;
  }

  h1 {
    font-size: 3.052em;
  }

  h2 {
    font-size: 2.441em;
  }

  h3 {
    font-size: 1.953em;
  }

  h4 {
    font-size: 1.563em;
  }

  h5 {
    font-size: 1.25em;
  }

  p {
    font-size: 1em;
  }

  small, .text_small {font-size: 0.8rem;}

  hr {
    border-color: rgba(229, 231, 235, 0.25);
    margin-top: 2rem;
    margin-bottom: 2rem;
  }
`;
