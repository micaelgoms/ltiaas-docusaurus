import React from "react";
import { CodeHightlightProvider } from "./codeHightlight";

function AppProvider({ children }) {
  return <CodeHightlightProvider>{children}</CodeHightlightProvider>;
}

export default AppProvider;
