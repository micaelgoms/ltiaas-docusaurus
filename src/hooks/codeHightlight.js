import React, { createContext, useContext, useState } from "react";

const CodeHightlightContext = createContext({});

const CodeHightlightProvider = ({ children }) => {
  const [currentCode, setCurrentCode] = useState(0);

  return (
    <CodeHightlightContext.Provider value={{ currentCode, setCurrentCode }}>
      {children}
    </CodeHightlightContext.Provider>
  );
};

const useCodeHightlight = () => {
  const context = useContext(CodeHightlightContext);

  if (!context) throw new Error("hook must be used in context.");

  return context;
};

export { CodeHightlightProvider, useCodeHightlight };
