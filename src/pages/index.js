import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Description from "../components/Description";
import Samples from "../components/Samples";

import AppProvider from "../hooks/index";
import Partners from "../components/Partners";
import Steps from "../components/Steps";
import Plans from "../components/Plans";
import Testimonials from "../components/Testimonials";
import Cta from "../components/CTA";
import Footer from "../components/Footer";

const IndexPage = () => {
  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
        {/* <link rel="canonical" href="http://mysite.com/example" /> */}
      </Helmet>

      <GlobalStyle />
      <Navbar />
      <Header />
      <Description />
      <Samples />
      <Partners />
      <Steps />
      <Plans />
      <Testimonials />
      <Cta />
      <Footer />
    </AppProvider>
  );
};

export default IndexPage;
