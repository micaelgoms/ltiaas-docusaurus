import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import NavbarTemplate from "../components/NavbarTemplate";
import comp_graph from "../images/comp_graph.png";
import AppProvider from "../hooks/index";
import Template from "../components/Template";
import Footer from "../components/Footer";
import PricingPlot from "../components/PricingPlot";
import * as S from "../components/PricingPlot/styled";

const PricingPage = () => {

  const [mau, setMau] = React.useState(25);
  const [dl, setDl] = React.useState(false);
  const [names, setNames] = React.useState(false);
  const [grades, setGrades] = React.useState(false);
  const [moodlePlugin, setMoodlePlugin] = React.useState(false);
  const [dynreg, setDynreg] = React.useState(false);

  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
      </Helmet>

      <GlobalStyle />
      <NavbarTemplate />

      <Template>
        <h1>LTIAAS Pricing</h1>
        <small>Latest update: March 9, 2022</small>

        <p>
          We operate on a simple pricing structure with no hidden fees, and you
          only pay for what you need. Actually, you can start using our API
          completely for free and only start paying once you start seeing
          significant API traffic. Once you set up a monthly subscription with
          us, we track the number of users (your customers) that are making LTI
          requests and only bill you for the number of active users each month.
          For example, if you have one user that uses your LTI tool every day,
          several times a day this month, we only bill you for one Monthly
          Active User (MAU). If you have 100 users, each using your tool only
          once this month, we will bill you for 100 MAU.
        </p>

        <h4>Our Pricing Structure</h4>

        <p>
          LTIAAS is completely free if you have 25 or less MAU. This makes it
          easy to test with our API as you get your LTI tool up and running
          without any costs. Once you have more than 25 MAU, you will be charged
          for the first 25 users as well as any additional users. LTIAAS uses
          graduated pricing, so while the base price for the core service is
          $0.02/MAU, once you go above 1,000 MAU, the next 19,000 users are
          billed at a lower price, and so on.
        </p>
        <div className="table-responsive">
          <table className="pricing_table">
            <thead className="pricing_table_header">
              <tr>
                <th>Monthly Active Users✝</th>
                <th>Core ($/MAU*)</th>
                <th>Deep Linking ($/MAU*)</th>
                <th>Grades ($/MAU*)</th>
                <th>Names &amp; Roles ($/MAU*)</th>
                <th>Moodle Plugin</th>
                <th>Dynamic Registration</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Users 1-25</td>
                <td>free**</td>
                <td>free**</td>
                <td>free**</td>
                <td>free**</td>
                <td>free**</td>
                <td>free</td>
              </tr>
              <tr>
                <td>Users 26-1000</td>
                <td>$0.02</td>
                <td>$0.003</td>
                <td>$0.006</td>
                <td>$0.004</td>
                <td>$0.01</td>
                <td>$10.00</td>
              </tr>
              <tr>
                <td>Users 1,001-20,000</td>
                <td>$0.014</td>
                <td>$0.0022</td>
                <td>$0.0042</td>
                <td>$0.0028</td>
                <td>$0.007</td>
                <td>$0.00</td>
              </tr>
              <tr>
                <td>Users 20,001 - 100,000</td>
                <td>$0.008</td>
                <td>$0.0012</td>
                <td>$0.0024</td>
                <td>$0.0016</td>
                <td>$0.004</td>
                <td>$0.00</td>
              </tr>
              <tr>
                <td>Users 100,001+</td>
                <td>$0.0052</td>
                <td>$0.0008</td>
                <td>$0.0016</td>
                <td>$0.001</td>
                <td>$0.0026</td>
                <td>$0.00</td>
              </tr>
            </tbody>
          </table>
        </div>

        <p className="footnote">pricing is preliminary and subject to change</p>
        <p className="footnote">✝Pricing tiers are graduated</p>
        <p className="footnote">*$/MAU = USD per Monthly Active Users</p>
        <p className="footnote">
          **After 26+ MAU, the first 25 users are billed at the 26-1000 user
          rate
        </p>

        <p></p>
        <p></p>

        <h4>Example Pricing</h4>

        <p>
          Here's an example monthly bill for a customer that has 2,000 Monthly
          Active users with a service plan that includes the mandatory{" "}
          <i>Core</i> service along with the <i>Deep Linking</i> and{" "}
          <i>Dynamic Registration</i> addons.
        </p>

        <div className="table-responsive">
          <table>
            <thead className="pricing_table_header">
              <tr>
                <th>MAU</th>
                <th>Core</th>
                <th>Deep Linking</th>
                <th>Dynamic Registration</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>User 1-25</td>
                <td>$0.02 * 25</td>
                <td>$0.003 * 25</td>
                <td>$0.00</td>
                <td>$0.57</td>
              </tr>
              <tr>
                <td>Users 26-1000</td>
                <td>$0.02 * 975</td>
                <td>$0.003 * 975</td>
                <td>$10.00 (flat)</td>
                <td>$32.42</td>
              </tr>
              <tr>
                <td>Users 1,001-2000</td>
                <td>$0.014 * 1000</td>
                <td>$0.0022 * 1000</td>
                <td></td>
                <td>$16.20</td>
              </tr>
              <tr>
                <td>Total Monthly Bill</td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td>
                  <u>$49.19</u>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <p></p>
        <p></p>

        <h4>Pricing Calculator</h4>
        <p>User the simple calculator below to enter in your API options and expected monthly users
          to get an idea of your expected monthly bill.
        </p>
        <S.Wrapper>
          <S.PlanSection>
            <S.CorePlan>
              <S.Form
                name="contactForm"
              >
                <label>
                  Monthly Users
                  <input
                    type="number"
                    name="mau"
                    step="100"
                    min="25"
                    defaultValue="25"
                    onChange={e => {setMau(e.target.value)}}
                  />
                </label>

                <label>
                  <input
                    className="checkbox"
                    type="checkbox"
                    name="dl"
                    onChange={e => {setDl(e.target.checked)}}
                  ></input>
                  Deep Linking
                </label>
                <label>
                  <input
                    className="checkbox"
                    type="checkbox"
                    name="dl"
                    onChange={e => {setNames(e.target.checked)}}
                  ></input>
                  Name &amp; Roles
                </label>
                <label>
                  <input
                    className="checkbox"
                    type="checkbox"
                    name="dl"
                    onChange={e => {setGrades(e.target.checked)}}
                  ></input>
                  Assignments &amp; Grades
                </label>
                <label>
                  <input
                    className="checkbox"
                    type="checkbox"
                    name="dl"
                    onChange={e => {setMoodlePlugin(e.target.checked)}}
                  ></input>
                  Moodle Plugin
                </label>
                <label>
                  <input
                    className="checkbox"
                    type="checkbox"
                    name="dl"
                    onChange={e => {setDynreg(e.target.checked)}}
                  ></input>
                  Dynamic Registration
                </label>

              </S.Form>
            </S.CorePlan>
            <S.PlotHolder>
              <S.CorePlanInsidePlanList>
                <div>
                <PricingPlot mau={mau} dl={dl} names={names} grades={grades} moodlePlugin={moodlePlugin} dynreg={dynreg}></PricingPlot>
                </div>
              </S.CorePlanInsidePlanList>
            </S.PlotHolder>
          </S.PlanSection>
        </S.Wrapper>

        <h4>Comparing The Competition</h4>

        <p>
          The LTIAAS pricing model scales as your company grows its LTI user
          base. We are proud of our incredibly competitive prices that enable
          access to learning tools in places our customers can't afford if they
          use our competitor's services. Below is a chart comparing our pricing
          to one of our largest competitors. We because we charge per ACTIVE
          user, we enable you to keep costs manageable as users come and go, and
          your overall LTI user-base scales.
        </p>

        <img src={comp_graph}></img>
      </Template>
      <Footer />
    </AppProvider>
  );
};

export default PricingPage;
