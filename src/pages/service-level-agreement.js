import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import NavbarTemplate from "../components/NavbarTemplate";
import { LocalizedLink } from "gatsby-theme-i18n";

import AppProvider from "../hooks/index";
import Template from "../components/Template";
import Footer from "../components/Footer";

const ServiceLeavelAgreementPage = () => {
  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
      </Helmet>

      <GlobalStyle />
      <NavbarTemplate />

      <Template>
        <h1>Service Level Agreement</h1>
        <small>Latest update: January 31, 2021</small>

        <p>
          This Service Level Agreement (“SLA”) is a policy governing the use of
          LTIAAS's API Services and applies separately to each account using
          these services. In the event of a conflict between the terms of this
          SLA and the{" "}
          <LocalizedLink to="/terms" language="en">
            Terms of Service
          </LocalizedLink>{" "}
          or other agreement with us governing your use of our API Services (the
          “Agreement”), the terms and conditions of this SLA apply, but only to
          the extent of such conflict. Capitalized terms used herein but not
          defined herein shall have the meanings set forth in the Agreement.
        </p>

        <h4>Service Commitment</h4>
        <p>
          GATHERACT LLC, the parent company of LTIAAS, will use commercially
          reasonable efforts to make services available with a Monthly Uptime
          Percentage of at least 99.95% for each region the API Services are
          hosted, during any monthly billing cycle (the “Service Commitment”).
          In the event that the API Services do not meet the Service Commitment,
          you will be eligible to receive a Service Credit as described below.
        </p>

        <h4>Service Credits</h4>
        <p>
          Service Credits are calculated as a percentage of the total charges
          paid by you for the API Services in the affected region for the
          monthly billing cycle in which the Monthly Uptime Percentage fell
          within the ranges set forth in the table below:
        </p>

        <table>
          <thead>
            <td>Monthly Uptime Percentage</td>
            <td>Service Credit Percentage</td>
          </thead>
          <tr>
            <td>Less than 99.95% but greater than or equal to 99.0%</td>
            <td>10%</td>
          </tr>
          <tr>
            <td>Less than 99.0% but greater than or equal to 95.0%</td>
            <td>25%</td>
          </tr>
          <tr>
            <td>Less than 95.0%</td>
            <td>100%</td>
          </tr>
        </table>

        <p>
          We will apply any Service Credits only against future API Services
          payments otherwise due from you. At our discretion, we may issue the
          Service Credit to the credit card you used to pay for the billing
          cycle in which the Service Commitment was not met. Service Credits
          will not entitle you to any refund or other payment from GATHERACT
          LLC. A Service Credit will be applicable and issued only if the credit
          amount for the applicable monthly billing cycle is greater than one
          dollar ($1 USD). Service Credits may not be transferred or applied to
          any other account. Unless otherwise provided in the Agreement, your
          sole and exclusive remedy for any unavailability, non-performance, or
          other failure by us to provide the API Services is the receipt of a
          Service Credit (if eligible) in accordance with the terms of this SLA.
        </p>

        <h4>Credit Request and Payment Procedures</h4>

        <p>
          To receive a Service Credit, you must submit a claim by emailing
          support@gatheract.com. To be eligible, the credit request must be
          received by us by the end of the second billing cycle after which the
          incident occurred and must include:
        </p>

        <ol>
          <li>the words “SLA Credit Request” in the subject line;</li>
          <li>
            the dates, times, and regions of each lack of Availability incident
            that you are claiming;
          </li>
          <li>the affected AIP Services API Keys;</li>
          <li>
            the billing cycle with respect to which you are claiming Service
            Credits;
          </li>
          <li>
            your request logs that document the errors and corroborate your
            claimed outage (any confidential or sensitive information in these
            logs should be removed or replaced with asterisks)
          </li>
        </ol>

        <p>
          If the Monthly Uptime Percentage of such request is confirmed by us
          and is less than the Service Commitment, then we will issue the
          Service Credit to you within one billing cycle following the month in
          which your request is confirmed by us. Your failure to provide the
          request and other information as required above will disqualify you
          from receiving a Service Credit.
        </p>

        <h4>API Services SLA Exclusions</h4>
        <p>
          The Service Commitment does not apply to any unavailability,
          suspension or termination of the API Services, or any other API
          Services performance issues: (i) caused by factors outside of our
          reasonable control including any force majeure event or Internet
          access or related problems beyond the demarcation point of the API
          Services; (ii) that result from any voluntary actions or inactions by
          you or any third party (e.g., scaling of provisioned capacity,
          misconfiguration of authentication tokens, timeouts related to slow
          3rd party connections, etc.); (iii) that result from your equipment,
          software or other technology and/or third party equipment, software or
          other technology (other than third party equipment within our direct
          control); (iv) that result from you not following the best practices
          described in the Developer Guides on the LTIAAS WebSite; or (v)
          arising from our suspension or termination of your right to use any
          API Services in accordance with the Agreement (collectively, the “API
          Services SLA Exclusions”). If availability is impacted by factors
          other than those used in our Monthly Uptime Percentage calculation,
          then we may issue a Service Credit considering such factors at our
          discretion.
        </p>

        <h4>Definitions</h4>
        <ul>
          <li>
            “Availability” is calculated for each 5-minute interval as the
            percentage of Requests processed by API Services that do not fail
            with Errors and relate solely to the provisioned API Services. If
            you did not make any Requests in a given 5-minute interval, that
            interval is assumed to be 100% available
          </li>
          <li>
            An “Error” is any Request that fails due to an API Gateway internal
            service error.
          </li>
          <li>
            “Monthly Uptime Percentage” for a given region is calculated as the
            average of the Availability for all 5-minute intervals in a monthly
            billing cycle. Monthly Uptime Percentage measurements exclude any
            lack of Availability resulting directly or indirectly from any API
            Services SLA Exclusion.
          </li>
          <li>
            A “Request” is an invocation of an endpoint of any API hosted by
            LTIAAS.
          </li>
          <li>
            A “Service Credit” is a dollar credit, calculated as set forth
            above, that we may credit back to an eligible account.
          </li>
        </ul>
      </Template>
      <Footer />
    </AppProvider>
  );
};

export default ServiceLeavelAgreementPage;
