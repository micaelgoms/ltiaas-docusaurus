import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import NavbarTemplate from "../components/NavbarTemplate";

import AppProvider from "../hooks/index";
import Template from "../components/Template";
import Footer from "../components/Footer";

const PolicyPage = () => {
  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
      </Helmet>

      <GlobalStyle />
      <NavbarTemplate />

      <Template>
        <h1>Privacy Policy</h1>
        <small>Latest update: July 1, 2021</small>
        <p>
          This privacy policy has been compiled to better serve those who are
          concerned with how their 'Personally Identifiable Information' (PII)
          is being used online. PII, as described in US privacy law and
          information security, is information that can be used on its own or
          with other information to identify, contact, or locate a single
          person, or to identify an individual in context. Please read our
          privacy policy carefully to get a clear understanding of how we
          collect, use, protect or otherwise handle your Personally Identifiable
          Information in accordance with our website.
        </p>
        <h4>
          What personal information do we collect from the people that visit our
          website?
        </h4>
        <p>
          When ordering or registering on our site, as appropriate, you may be
          asked to enter your name, email address, mailing address, phone number
          or other details to help you with your experience.
        </p>
        <h4>When do we collect information?</h4>
        <p>
          We collect information from you when you register on our site, place
          an order, subscribe to a newsletter, fill out a form or enter
          information on our site.
        </p>
        <h4>How do we use your information?</h4>
        <p>
          We may use the information we collect from you when you register, make
          a purchase, sign up for our newsletter, respond to a survey or
          marketing communication, surf the website, or use certain other site
          features in the following ways:
        </p>
        <ul>
          <li>
            To allow us to better service you in responding to your customer
            service requests.
          </li>
          <li>To quickly process your transactions.</li>
          <li>
            To send periodic emails regarding your order or other products and
            services.
          </li>
          <li>
            To follow up with them after correspondence (live chat, email or
            phone inquiries)
          </li>
        </ul>
        <h4>How do we protect your information?</h4>
        <p>We use vulnerability scanning and/or scanning to PCI standards.</p>

        <p>
          We only provide articles and information. We never ask for credit card
          numbers. however, this information may be requested through a
          third-party verified payment processor.
        </p>

        <p>We use regular Malware Scanning.</p>

        <p>
          Your personal information is contained behind secured networks and is
          only accessible by a limited number of persons who have special access
          rights to such systems, and are required to keep the information
          confidential. In addition, all sensitive/credit information you supply
          is encrypted via Secure Socket Layer (SSL) technology.
        </p>

        <p>
          We implement a variety of security measures when a user places an
          order enters, submits, or accesses their information to maintain the
          safety of your personal information.
        </p>

        <p>
          All transactions are processed through a gateway provider and are not
          stored or processed on our servers.
        </p>

        <h4>Do we use 'cookies'?</h4>
        <p>
          We do not use cookies for tracking purposes, but we do use them to
          validate that you have logged in to your account.
        </p>
        <p>
          You can choose to have your computer warn you each time a cookie is
          being sent, or you can choose to turn off all cookies. You do this
          through your browser settings. Since browser is a little different,
          look at your browser's Help Menu to learn the correct way to modify
          your cookies.
        </p>
        <p>
          If you turn cookies off, some features will be disabled, that make
          your site experience more inefficient and may not function properly.
        </p>
        <p>
          Furthermore, you will not be able to place orders without being logged
          in which requires a cookie.
        </p>

        <h4>Third-party disclosure</h4>
        <p>
          We do not sell, trade, or otherwise transfer to outside parties your
          Personally Identifiable Information.
        </p>
        <h4>Third-party links</h4>
        <p>
          We include or offer third-party products or services on our website.
          These third-party sites have separate and independent privacy
          policies. We therefore have no responsibility or liability for the
          content and activities of these linked sites. Nonetheless, we seek to
          protect the integrity of our site and welcome any feedback about these
          sites.
        </p>

        <p>
          We, along with third-party vendors such as Google use first-party
          cookies (such as the Google Analytics cookies) and third-party cookies
          (such as the DoubleClick cookie) or other third-party identifiers
          together
        </p>
        <p>
          We use them to compile data regarding user interactions to improve
          user experiences
        </p>
        <p>
          <strong>
            Opting out:
            <br />
            Users can set preferences for how Google advertises to you using the
            Google Ad Settings page. Alternatively, you can opt out by visiting
            the Network Advertising Initiative Opt Out page or by using the
            Google Analytics Opt Out Browser add on.
          </strong>
        </p>
        <h5>California Online Privacy Protection Act</h5>
        <p>
          CalOPPA is the first state law in the nation to require commercial
          websites and online services to post a privacy policy. The law's reach
          stretches well beyond California to require any person or company in
          the United States (and conceivably the world) that operates websites
          collecting Personally Identifiable Information from California
          consumers to post a conspicuous privacy policy on its website stating
          exactly the information being collected and those individuals or
          companies with whom it is being shared. - See more at:&nbsp;
          <a href="http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf">
            http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
          </a>
        </p>
        <p>
          <strong>According to CalOPPA, we agree to the following:</strong>
        </p>
        <p>Users can visit our site anonymously.</p>
        <p>
          Once this privacy policy is created, we will add a link to it on our
          home page or as a minimum, on the first significant page after
          entering our website.
        </p>
        <p>
          Our Privacy Policy link includes the word 'Privacy' and can easily be
          found on the page specified above.
        </p>
        <p>You will be notified of any Privacy Policy changes:</p>
        <ul>
          <li>On our Privacy Policy Page</li>
        </ul>
        <p>Can change your personal information:</p>
        <ul>
          <li>By emailing us</li>
          <li>By updating your online profile with us</li>
        </ul>

        <p>
          <strong>How does our site handle Do Not Track signals?</strong>
        </p>
        <p>
          We honor Do Not Track signals and Do Not Track, plant cookies, or use
          advertising when a Do Not Track (DNT) browser mechanism is in place.
        </p>
        <p>
          <strong>Does our site allow third-party behavioral tracking?</strong>
        </p>
        <p>
          It's also important to note that we do not allow third-party
          behavioral tracking
        </p>

        <h5>COPPA (Children Online Privacy Protection Act)</h5>
        <p>
          When it comes to the collection of personal information from children
          under the age of 13 years old, the Children's Online Privacy
          Protection Act (COPPA) puts parents in control. The Federal Trade
          Commission, United States' consumer protection agency, enforces the
          COPPA Rule, which spells out what operators of websites and online
          services must do to protect children's privacy and safety online.
        </p>
        <p>
          We do not specifically market to children under the age of 13 years
          old.
        </p>

        <h5>Fair Information Practices</h5>
        <p>
          The Fair Information Practices Principles form the backbone of privacy
          law in the United States and the concepts they include have played a
          significant role in the development of data protection laws around the
          globe. Understanding the Fair Information Practice Principles and how
          they should be implemented is critical to comply with the various
          privacy laws that protect personal information.
        </p>
        <p>
          <strong>
            In order to be in line with Fair Information Practices we will take
            the following responsive action, should a data breach occur:
          </strong>
        </p>
        <p>We will notify you via email</p>
        <ul>
          <li>Within 7 business days</li>
        </ul>
        <p>
          We also agree to the Individual Redress Principle which requires that
          individuals have the right to legally pursue enforceable rights
          against data collectors and processors who fail to adhere to the law.
          This principle requires not only that individuals have enforceable
          rights against data users, but also that individuals have recourse to
          courts or government agencies to investigate and/or prosecute
          non-compliance by data processors.
        </p>
        <h5>CAN SPAM Act</h5>
        <p>
          The CAN-SPAM Act is a law that sets the rules for commercial email,
          establishes requirements for commercial messages, gives recipients the
          right to have emails stopped from being sent to them, and spells out
          tough penalties for violations.
        </p>
        <p>
          <strong>We collect your email address in order to:</strong>
        </p>
        <ul>
          <li>
            Send information, respond to inquiries, and/or other requests or
            questions
          </li>
          <li>
            Process orders and to send information and updates pertaining to
            orders
          </li>
          <li>
            Send you additional information related to your product and/or
            service
          </li>
        </ul>
        <p>
          <strong>
            To be in accordance with CANSPAM, we agree to the following:
          </strong>
        </p>
        <ul>
          <li>Not use false or misleading subjects or email addresses.</li>
          <li>
            Identify the message as an advertisement in some reasonable way.
          </li>
          <li>
            Include the physical address of our business or site headquarters.
          </li>
          <li>
            Monitor third-party email marketing services for compliance, if one
            is used.
          </li>
          <li>Honor opt-out/unsubscribe requests quickly.</li>
          <li>
            Allow users to unsubscribe by using the link at the bottom of each
            email.
          </li>
        </ul>
        <p>
          <strong>
            If at any time you would like to unsubscribe from receiving future
            emails,
          </strong>{" "}
          you can email us at support@ltiaas.com, or follow the instructions at
          the bottom of each email. and we will promptly remove you from ALL
          correspondence.
        </p>

        <h5>Contacting Us</h5>
        <p>
          If there are any questions regarding this privacy policy, you may
          contact us using the information below.
          <p></p>
        </p>
        <p>ltiaas.com</p>
        <p>support@ltiaas.com</p>
      </Template>
      <Footer />
    </AppProvider>
  );
};

export default PolicyPage;
