import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import NavbarTemplate from "../components/NavbarTemplate";

import AppProvider from "../hooks/index";
import Template from "../components/Template";
import Footer from "../components/Footer";

const IndexPage = () => {
  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
      </Helmet>

      <GlobalStyle />
      <NavbarTemplate />

      <Template>
        <h1>Terms and Conditions</h1>
        <small>Latest update: January 31, 2021</small>

        <p>
          By using our Services, you are agreeing to these terms. Please read
          these Terms and Conditions ("Terms", "Terms and Conditions") carefully
          before using the https://ltiaas.com website and the mobile application
          (the "Service") operated by GATHERACT LLC ("us", "we", or "our"), the
          parent company of LTIAAS.{" "}
        </p>

        <p>
          Our Services are very diverse, so sometimes additional terms or
          product requirements (including age requirements) may apply.
          Additional terms will be available with the relevant Services, and
          those additional terms become part of your agreement with us if you
          use those Services.
        </p>

        <h4>Terminology</h4>

        <p>
          The following terminology applies to these Terms and Conditions,
          Privacy Statement and Disclaimer notice, and any or all Agreements:
          "Client", "You" and "Your" refer to you, the person accessing this
          website and accepting the Company's terms and conditions. "The
          Company", "Ourselves", "We" and "Us" refer to our Company. "Party",
          "Parties" or "Us" refers to both the Customer and ourselves, or either
          the Customer or ourselves. All terms refer to the offer, acceptance
          and consideration of payment necessary to undertake the process of our
          assistance to the Client in the most appropriate manner, whether
          through formal meetings of a fixed duration, or by any other means,
          with the express purpose of meeting the Client's needs in terms of
          providing the Company's declared services / products, in accordance
          with and subject to applicable US laws. Any use of the above
          terminology or other words in the singular, plural, capital letters
          and/or plural, and/or these terms, is considered interchangeable and
          therefore a reference to them.{" "}
        </p>

        <h4>Using our Services</h4>

        <p>
          You must follow any policies made available to you within the
          Services. Don’t misuse our Services. For example, don’t interfere with
          our Services or try to access them using a method other than the
          interface and the instructions that we provide. You may use our
          Services only as permitted by law, including applicable export and
          re-export control laws and regulations. We may suspend or stop
          providing our Services to you if you do not comply with our terms or
          policies or if we are investigating suspected misconduct.
        </p>

        <p>
          Using our Services does not give you ownership of any intellectual
          property rights in our Services or the content you access. You may not
          use content from our Services unless you obtain permission from its
          owner or are otherwise permitted by law. These terms do not grant you
          the right to use any branding or logos used in our Services. Don’t
          remove, obscure, or alter any legal notices displayed in or along with
          our Services.
        </p>

        <p>
          In connection with your use of the Services, we may send you service
          announcements, administrative messages, and other information. You may
          opt out of some of those communications.
        </p>
        <p>
          Some of our Services are available on mobile devices. Do not use such
          Services in a way that distracts you and prevents you from obeying
          traffic or safety laws.
        </p>

        <h4>Privacy Statement</h4>

        <p>
          We are committed to protecting your privacy. GATHERACT LLC’s privacy
          policies explain how we treat your personal data and protect your
          privacy when you use our Services. By using our Services, you agree
          that GATHERACT LLC can use such data in accordance with our privacy
          policies.
        </p>

        <p>
          Only authorized employees within the company who, in the course of
          their duties, can access and use information collected from individual
          customers.{" "}
        </p>

        <p>
          We are constantly reviewing our systems and data to ensure the best
          possible service to our customers. Government authorities have created
          specific offences for unauthorized actions against computer systems
          and data. We will investigate such actions with a view to bringing
          legal action and/or civil action for damages against those
          responsible.
        </p>

        <h4>Purchases</h4>

        <p>
          If you wish to purchase any product or service made available through
          the Service ("Purchase"), you may be asked to supply certain
          information relevant to your Purchase including, without limitation,
          your name, email address, street address, phone number, company name,
          credit card number, domain name, and various company URLs.
        </p>

        <h4>Subscriptions</h4>

        <p>
          Some parts of the Service are billed on a subscription basis
          ("Subscription(s)"). You will be billed on a recurring monthly basis.
        </p>

        <h4>Software in our Services</h4>

        <p>
          When a Service requires or includes downloadable software, this
          software may update automatically on your device once a new version or
          feature is available. Some Services may let you adjust your automatic
          update settings.
        </p>

        <p>
          GATHERACT LLC gives you a personal, worldwide, royalty-free,
          non-assignable and non-exclusive license to use the software provided
          to you by GATHERACT LLC as part of the Services. This license is for
          the sole purpose of enabling you to use and enjoy the benefit of the
          Services as provided by GATHERACT LLC, in the manner permitted by
          these terms. You may not copy, modify, distribute, sell, or lease any
          part of our Services or included software, nor may you reverse
          engineer or attempt to extract the source code of that software,
          unless laws prohibit those restrictions or you have our written
          permission.
        </p>

        <h4>Disclaimer</h4>

        <h4>Exclusions and Limitations </h4>

        <p>
          The information contained on this website is provided on an " as is "
          basis. To the fullest extent permitted by law, this company:{" "}
        </p>

        <p>
          excludes all representations and warranties with respect to this
          website and its content or that are or may be provided by affiliates
          or any other third party, including with respect to any inaccuracy or
          omission in this website and/or the Company's documentation; and{" "}
        </p>

        <p>
          excludes any liability for damages arising out of or in connection
          with your use of this website. GATHERACT, and GATHERACT LLC’s
          suppliers and distributors, will not be responsible for lost profits,
          revenues, or data, financial losses or indirect, special,
          consequential, exemplary, punitive damages or damage caused to your
          computer, computer software, systems and programs and data relating
          thereto or any other direct or indirect, consequential or incidental
          damages.{" "}
        </p>

        <h4>Liability for our Services</h4>

        <p>
          To the extent permitted by law, the total liability of GATHERACT LLC,
          and its suppliers and distributors, for any claims under these terms,
          including for any implied warranties, is limited to the amount you
          paid us to use the Services.
        </p>

        <p>
          In all cases, GATHERACT LLC, and its suppliers and distributors, will
          not be liable for any loss or damage that is not reasonably
          foreseeable.
        </p>

        <p>
          However, this company does not exclude liability for death or personal
          injury caused by its negligence. The above exclusions and limitations
          apply only to the extent permitted by law. We recognize that in some
          countries, you might have legal rights as a consumer. None of your
          legal rights as a consumer are affected waived by contract.
        </p>

        <h4>Business uses of our Services</h4>

        <p>
          If you are using our Services on behalf of a business, that business
          accepts these terms. It will hold harmless and indemnify GATHERACT LLC
          and its affiliates, officers, agents, and employees from any claim,
          suit or action arising from or related to the use of the Services or
          violation of these terms, including any liability or expense arising
          from claims, losses, damages, suits, judgments, litigation costs and
          attorneys’ fees.
        </p>

        <h4>Cancellation Policy</h4>

        <p>
          A minimum of 24 hours' notice of cancellation is required. Such notice
          may be given, in person, by email, mobile phone, text message, or by
          any other means, and will be accepted subject to written confirmation.
          We reserve the right to charge a cancellation fee of $10 USD to cover
          any administrative costs.
        </p>

        <h4>Modifying and Terminating our Services </h4>

        <p>
          We are constantly changing and improving our Services. We may add or
          remove functionalities or features, and we may suspend or stop a
          Service altogether.
        </p>

        <p>
          The Customer and GATHERACT LLC have the right to terminate any Service
          Agreement for any reason whatsoever, including the termination of
          services already in progress. GATHERACT LLC may also stop providing
          Services to you or add or create new limits to our Services at any
          time.
        </p>

        <h4>Refunds Policy</h4>

        <p>
          No refund will be offered when a service is deemed to have commenced
          and is, for all intents and purposes, in progress. Any amount paid to
          us that constitutes payment for the provision of unused Services, will
          be refunded.
        </p>

        <h4>Log Files</h4>

        <p>
          We use IP addresses to analyse trends, administer the site, track
          user’s movement, and gather broad demographic information for
          aggregate use. IP addresses are not linked to personally identifiable
          information. Additionally, for systems administration, detecting usage
          patterns and troubleshooting purposes, our web servers automatically
          log standard access information including browser type, access
          times/open mail, URL requested, and referral URL. This information is
          not shared with third parties and is used only within this Company on
          a need-to-know basis. Any individually identifiable information
          related to this data will never be used in any way different to that
          stated above without your explicit permission.{" "}
        </p>

        <h4>Cookies</h4>

        <p>
          Like most interactive websites, this Company’s website uses cookies to
          enable us to retrieve user details for each visit. Cookies are used in
          some areas of our site to enable the functionality of this area and
          ease of use for those people visiting. Some of our affiliate partners
          may also use cookies.{" "}
        </p>

        <h4>Links to others' website </h4>

        <p>
          Our Service may contain links to third-party web sites or services
          that are not owned or controlled by GATHERACT LLC.{" "}
        </p>

        <p>
          GATHERACT LLC has no control over, and assumes no responsibility for,
          the content, privacy policies, or practices of any third-party web
          sites or services. The opinions expressed or material appearing on
          these websites are not necessarily shared or endorsed by us and should
          not be considered as the publisher of such opinions or material.
          Please note that we are not responsible for the privacy practices or
          content of these sites. We encourage our users to be aware when they
          leave our site and to read the privacy statements of these sites.{" "}
        </p>

        <p>
          You should evaluate the security and reliability of any other site
          linked to or accessed through this site before disclosing any personal
          information to them. This company will not accept any liability for
          any loss or damage, in any manner whatsoever, regardless of the cause,
          resulting from your disclosure of personal information to third
          parties.{" "}
        </p>

        <p>
          You further acknowledge and agree that GATHERACT LLC shall not be
          responsible or liable, directly or indirectly, for any damage or loss
          caused or alleged to be caused by or in connection with use of or
          reliance on any such content, goods or services available on or
          through any such web sites or services.{" "}
        </p>

        <h4>Links to this website </h4>

        <p>
          You may not create a link to a page on this website without our prior
          written consent. If you link to any page on this website, you do so at
          your own risk and the exclusions and limitations set out above apply
          to your use of this website.{" "}
        </p>

        <h4>Copyright Notice</h4>

        <p>
          Copyright and other relevant intellectual property rights exist on all
          text relating to the Company’s services and the full content of this
          website. This Company’s logo is a registered trademark of this Company
          in the United Kingdom and other countries. The brand names and
          specific services of this Company featured on this web site are trade
          marked{" "}
        </p>

        <h4>Force Majeure</h4>

        <p>
          Neither party shall be liable to the other for any failure to perform
          any obligation under any Agreement which is due to an event beyond the
          control of such party including but not limited to any Act of God,
          terrorism, war, Political insurgence, insurrection, riot, civil
          unrest, act of civil or military authority, uprising, earthquake,
          flood or any other natural or man made eventuality outside of our
          control, which causes the termination of an agreement or contract
          entered into, nor which could have been reasonably foreseen. Any Party
          affected by such event shall forthwith inform the other Party of the
          same and shall use all reasonable endeavours to comply with the terms
          and conditions of any Agreement contained herein.{" "}
        </p>

        <h4>Waiver</h4>

        <p>
          Failure of either Party to insist upon strict performance of any
          provision of this or any Agreement or the failure of either Party to
          exercise any right or remedy to which it, he or they are entitled
          hereunder shall not constitute a waiver thereof and shall not cause a
          diminution of the obligations under this or any Agreement. No waiver
          of any of the provisions of this or any Agreement shall be effective
          unless it is expressly stated to be such and signed by both Parties.
        </p>

        <h4>About these Terms</h4>

        <p>
          We reserve the right to modify these terms or any additional terms
          that apply to a Service to, for example, reflect changes to the law or
          changes to our Services. You should look at the terms regularly. If a
          revision is material, we will try to provide at least 30 days' notice
          prior to any new terms taking effect. What constitutes a material
          change will be determined at our sole discretion.{" "}
        </p>

        <p>
          We’ll post notice of modifications to these terms on this page. We’ll
          post notice of modified additional terms in the applicable Service.
          Changes will not apply retroactively and will become effective no
          sooner than 7 days after they are posted. However, changes addressing
          new functions for a Service or changes made for legal reasons will be
          effective immediately. If you do not agree to the modified terms for a
          Service, you should discontinue your use of that Service.
        </p>

        <p>
          If there is a conflict between these terms and the additional terms,
          the additional terms will control for that conflict.
        </p>

        <p>
          These terms control the relationship between GATHERACT LLC and you.
          They do not create any third-party beneficiary rights.
        </p>

        <p>
          If you do not comply with these terms, and we don’t act right away,
          this doesn’t mean that we are giving up any rights that we may have
          (such as taking action in the future).
        </p>

        <p>
          If it turns out that a particular term is not enforceable, this will
          not affect any other terms.
        </p>
        <p>
          The courts in some countries will not apply United States of America
          law to some types of disputes. If you reside in one of those
          countries, then where United States of America law is excluded from
          applying, your country’s laws will apply to such disputes related to
          these terms.{" "}
        </p>

        <h4>Contact Us</h4>

        <p>
          If you have any questions about these Terms, please contact us at
          support@ltiaas.com.
        </p>
      </Template>
      <Footer />
    </AppProvider>
  );
};

export default IndexPage;
