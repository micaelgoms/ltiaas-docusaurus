import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import NavbarTemplate from "../components/NavbarTemplate";

import AppProvider from "../hooks/index";
import Faq from "../components/FAQ";
import Footer from "../components/Footer";

const PricingPage = () => {
  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
      </Helmet>

      <GlobalStyle />
      <NavbarTemplate />

      <Faq />

      <Footer />
    </AppProvider>
  );
};

export default PricingPage;
