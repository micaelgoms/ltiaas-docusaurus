import * as React from "react";
import GlobalStyle from "../styles/global";
import { Helmet } from "react-helmet";
import NavbarTemplate from "../components/NavbarTemplate";
import { LocalizedLink } from "gatsby-theme-i18n";
import AppProvider from "../hooks/index";
import Template from "../components/Template";
import Footer from "../components/Footer";

const CompliancePage = () => {
  return (
    <AppProvider>
      <Helmet>
        <meta charSet="utf-8" />
        <title>LTI as a Service</title>
      </Helmet>

      <GlobalStyle />
      <NavbarTemplate />

      <Template>
        <h1>Compliance</h1>
        <small>Latest update: October 8, 2021</small>
        <p>
          At LTIAAS, we take privacy and security very seriously.
          For information about our customer's privacy, please read our&nbsp;
          <LocalizedLink to="/privacy-policy" language="en">
            Privacy Policy
          </LocalizedLink>.

          This page, on the other hand, explains the great lengths that LTIAAS has taken
          to ensure the privacy and security of the users that use the API hosted by LTIAAS
          on behalf of our customers. i.e. the teachers and students doing LTI launches through
          an instance of the LTIAAS API hosted on LTIAAS servers.
        </p>

        <h4>Security Highlights</h4>
        <p>At LTIAAS we take pride in the following:</p>
        <ol>
          <li>
            <strong>We don't store personal information if we don't have to</strong>:
            As part of the LTI launch process, an LMS must share user credentials with us.
            In some cases this is non-identifiable information, but for some LMSs this is
            a users name and email address. After we receive this information, <i>we only store
            it for 24 hours</i>, long enough to ensure the user stays connected between the LMS and LTI tool.
            When LMS administrators are registering an LTI Tool, they can specify the security level of
            integration and what information gets sent through LTIAAS. <i>The job of choosing what user
            information gets temporarily stored by LTIAAS is delegated to the institution when registering
            an LTI Tool.</i> This allows for fine control as to what data gets exposed through LTIAAS.
          </li>
          <li>
            <strong>We don't track</strong>: LTIAAS does not use tracking cookies or and other
            logging mechanisms to track specific users on our website or services. We only store a single metric,
            which is the total number of monthly users (anonymized), for billing purposes.
          </li>
          <li>
            <strong>All data is encrypted</strong>: in transit via SSL/HTTPS and when it is stored
            via AES-256 encrypted drives. We require both LMSs and LTI tools that use LTIAAS to use
            SSL/HTTPS as well.
          </li>
          <li>
            <strong>We have policies in place to ensure the privacy of user data</strong>:
            Only our customers have access to their personal API Keys. LTIAAS employees don't 
            have direct access to live user data on our servers. We don't store passwords to our servers and
            require 2-factor authenticators to access any system in LTIAAS.</li>
          <li>
            <strong>Our servers and PCs are constantly monitored</strong>: by industry leading intrusion detection
            and virus scanning software. All LTIAAS production software is scanned for potential vulnerabilities
            before being deployed. We don't deploy any software with known security issues. 
          </li>
          <li>
            <strong>We don't audit our customers</strong>: While LTIAAS takes privacy and security seriously,
            we don't audit our customers to understand their privacy and security practices.
            It is up to our customers and their users to audit our customer's compliance.
          </li>
        </ol>

        <h4>Third-Party Vendor List</h4>
        <p>
          At LTIAAS, we rely on few third-party vendors to help provide our services. 
          The table below outlines these vendors and their relationship to our customer's data.
          When necessary, LTIAAS enters into Data Processing Agreements that restrict what these vendors may use data for.
        </p>
        <table>
          <thead>
            <td>Vendor</td>
            <td>Hosted LTIAAS Service</td>
            <td>Access to Learning Data</td>
            <td>Notes</td>
          </thead>
          <tr>
            <td>Digital Ocean</td>
            <td>x</td>
            <td>x</td>
            <td>
              Digital Ocean is a SOC-2 certified cloud hosting provider that hosts all of
              LTIAAS's cloud infrastructure. While their infrastructure has full access to
              data transferred through and stored at LTIAAS.com, there are strict contractual
              limits on what Digital Ocean can do with that data.
            </td>
          </tr>
          <tr>
            <td>Google Cloud</td>
            <td>x</td>
            <td></td>
            <td>
              Google Cloud is a SOC-2 certified cloud hosting provider that hosts https://portal.ltiaas.com.
              They are given access to authenticate our customers and allow them to access their API
              management portals. No learning information is stored on Google's servers, just customer authentication
              information such as email address and LTIAAS account number.
            </td>
          </tr>
          <tr>
            <td>New Relic</td>
            <td>x</td>
            <td></td>
            <td>
              New Relic is a log aggregator and security event management platform that LTIAAS subscribes to.
              We transfer all system logs to New Relic. These logs contain information like requests made
              to our system, IP address, and name of resource requested. No learning data is accessible to New Relic.
              This service enables an extra layer of security to LTIAAS by automatically detecting suspicious events
              like failed logins and unexpected state changes.
            </td>
          </tr>
          <tr>
            <td>GitLab</td>
            <td>x</td>
            <td></td>
            <td>
              GitLab is our software code repository of choice. Unlike GitHub (what some of our competitors use),
              GitLab's contractual agreement with LTIAAS prohibits them from accessing and using LTIAAS repository data.
              GitLab does not process any customer or learning data.
            </td>
          </tr>
          <tr>
            <td>Stripe</td>
            <td>x</td>
            <td></td>
            <td>
              Stripe is our payment processing provider. Stripe is given access a non-identifiable customer ID number,
              customer coarse location (Country/State), and payment method.
              No other personal information is shared with Stripe. Stripe does not process any learning data.
            </td>
          </tr>
          <tr>
            <td>Google Workspace</td>
            <td>x</td>
            <td></td>
            <td>
              LTIAAS uses Google Workspace for email (Gmail), chat (Google Meet), and office products (Google Docs, etc.).
              Google does not process LTIAAS customer learning data, by they do process email about or customers as needed
              by LTIAAS to do regular business.
            </td>
          </tr>
        </table>
        
        <h4>Diagrams</h4>
        <p>
          LTIAAS uses Digital Ocean to host our customer API instances.
          Below is a simplified architectural diagram of our infrastructure resources and data flow.
        </p>
        <img src="/compliance/infrastructure_diagram.png"></img>

        <h4>Compliance Documents</h4>

        <table>
          <thead>
            <td>Document</td>
            <td>Link</td>
          </thead>
          <tr>
            <td>Information Security Management Program</td>
            <td><LocalizedLink to="/compliance/GatherAct_Information_Security_Management_Program.pdf" language="en">download</LocalizedLink></td>
          </tr>
          <tr>
            <td>Comprehensive IT Security Policy</td>
            <td><LocalizedLink to="/compliance/Comprehensive_IT_Security_Policy.pdf" language="en">download</LocalizedLink></td>
          </tr>
          <tr>
            <td>Comprehensive Security Policy</td>
            <td><LocalizedLink to="/compliance/GatherAct_Security_Policy.pdf" language="en">download</LocalizedLink></td>
          </tr>
          <tr>
            <td>Vendor Management Policy</td>
            <td><LocalizedLink to="/compliance/vendor_management_policy.pdf" language="en">download</LocalizedLink></td>
          </tr>
          <tr>
            <td>Disaster Recovery Plan</td>
            <td>available under NDA</td>
          </tr>
          <tr>
            <td>Software Development Lifecycle</td>
            <td><LocalizedLink to="/compliance/Software_Development_Lifecycle.pdf" language="en">download</LocalizedLink></td>
          </tr>
          <tr>
            <td>CAIQ-Lite Questionnaire</td>
            <td><LocalizedLink to="/compliance/CAIQ-Lite.pdf" language="en">download</LocalizedLink></td>
          </tr>
          <tr>
            <td>CAIQ v3.1 Full Questionnaire</td>
            <td><LocalizedLink to="/compliance/CAIQ_v3.1_Final.xlsx" language="en">download</LocalizedLink></td>
          </tr>
          
          <tr>
            <td>HECVAT Full</td>
            <td><LocalizedLink to="/compliance/HECVATFull211.xlsx" language="en">download</LocalizedLink></td>
          </tr>
        </table>

        <h4>Contacting Us</h4>

        <p>
          If there are any questions regarding LTIAAS privacy and security compliance, you may
          contact us using the information below.
          <p></p>
        </p>
        <ul>
          <li>ltiaas.com</li>
          <li>support@ltiaas.com</li>
        </ul>
      </Template>
      <Footer />
    </AppProvider>
  );
};

export default CompliancePage;
