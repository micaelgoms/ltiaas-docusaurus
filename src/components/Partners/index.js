import React from "react";

import * as S from "./styled";

const Partners = () => {
  return (
    <S.Container>
      <S.Wrapper>
        <div>
          <h1>+200.000</h1>
          <p>
            LTIAAS is trusted by some of the largest names in ed-tech.
            Our API supports over 200,000 users every month.
          </p>
        </div>

        <S.PartnerList>
          <li>
            <a target="_blank" href="https://cpm.org">
              <img src="/partners/cpm.png" alt="cpm" />
            </a>
          </li>
          <li id="exputo">
            <a target="_blank" href="https://www.exputo.com">
              <img src="/partners/exputo.png" alt="exputo" />
            </a>
          </li>
          <li>
            <a target="_blank" href="https://www.umontpellier.fr">
              <img src="/partners/montpellier.png" alt="university montpellier" />
            </a>
          </li>
          <li>
            <a target="_blank" href="http://www.polytech-reseau.org/en/home/">
              <img src="/partners/polytech.png" alt="polytech french universities" />
            </a>
          </li>
          <li>
            <a target="_blank" href="https://www.dawnsign.com">
              <img src="/partners/dawnsign.png" alt="dawn sign press" />
            </a>
          </li>
          <li id="matt">
            <img src="/partners/mymatt.png" alt="my matt" />
          </li>
          <li>
            <a target="_blank" href="https://www.engagefastlearning.org">
              <img src="/partners/engage.png" alt="engage fast learning" />
            </a>
          </li>
          <li id="blending">
            <a target="_blank" href="https://blending.education/blending-space/">
              <img src="/partners/blending.png" alt="blending space" />
            </a>
          </li>
        </S.PartnerList>
      </S.Wrapper>
    </S.Container>
  );
};

export default Partners;
