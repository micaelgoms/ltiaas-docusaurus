import styled from "styled-components";
import flow from "../../images/Flow/3.svg";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 8em auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
  height: 100%;
  min-height: 230px;

  div {
    display: flex;
    flex-direction: column;
    justify-content: center;

    flex: 1;
    padding: 1rem;

    h1 {
      margin-bottom: 1rem;
      position: relative;

      &::before {
        content: "";
        position: absolute;
        background: url(${flow});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        width: 100px;
        height: 160px;
        bottom: 80px;
        left: 0;
        z-index: -5;

        @media (min-width: 992px) {
          left: 80px;
        }
      }
    }

    p {
      font-weight: 400;
    }
  }
`;

export const PartnerList = styled.ul`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  max-width: 100%;
  overflow-x: auto;
  height: 100%;
  padding-left: 1rem;

  @media (min-width: 992px) {
    flex-wrap: wrap;
    max-width: 60%;
  }

  li {
    list-style: none;
    min-width: 150px;
    min-height: 90px;
    width: 100%;
    height: 100%;
    background: #fff;
    border-radius: 5px;
    flex: 1;
    margin: 0.2rem;
    display: flex;
    padding: 0.5rem;
    align-items: center;
    justify-content: center;

    &#blending {
      img {
        background: #5aa1e3;
        border-radius: 5px;
        width: 120px;
      }
    }

    &#exputo {
      img {
        width: 100px;
      }
    }

    &#matt {
      img {
        height: 70px;
      }
    }

    img {
      margin: auto;
      width: 100%;
      height: 50px;
      object-fit: contain;
      display:block;
    }
  }
`;
