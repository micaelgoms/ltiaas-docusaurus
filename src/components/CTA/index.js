import React from "react";
import UilBookmark from "@iconscout/react-unicons/icons/uil-bookmark";
import elements from "../../images/elems.svg";

import * as S from "./styled";

const Cta = () => {
  return (
    <S.Container id="documentation">
      <S.Wrapper>
        <div>
          <h2>See how easy it is to build LTI into your learning tool.</h2>
          <a
            href="https://ltiaas.com/docs"
            target="_blank"
            rel="noopener noreferrer"
          >
            <UilBookmark />
            Documentation
          </a>
        </div>

        <S.ImageSection>
          <img src={elements} alt="elms" />
        </S.ImageSection>
      </S.Wrapper>
    </S.Container>
  );
};

export default Cta;
