import styled from "styled-components";
import buildPath from "../../images/building.svg";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  overflow-x: clip;
  margin-bottom: 0;

  @media (min-width: 756px) {
    margin: 4em auto;
  }

  @media (min-width: 992px) {
    margin: 15em auto 4em;
  }
`;

export const Wrapper = styled.div`
  max-width: 1200px;
  height: auto;
  margin: 0 auto;

  background: #e6f5ff;
  border-radius: 0;
  position: relative;

  display: flex;
  padding: 24px 16px;

  @media (min-width: 768px) {
    padding: 48px;
    border-radius: 16px;
  }

  div {
    max-width: 620px;
    display: flex;
    flex-direction: column;

    h2 {
      margin-bottom: 32px;
      line-height: 48px;
    }

    a {
      display: flex;
      justify-content: center;
      align-items: center;
      background: #f2275d;
      border-radius: 50px;
      color: #ffffff;
      text-decoration: none;

      width: 200px;
      height: 40px;
      font-size: 16px;

      cursor: pointer;
      font-family: Russo One;
      border: none;

      transition: transform 0.2s ease-in-out;

      &:hover {
        transform: translateY(-3px);
      }

      svg {
        margin-right: 8px;
      }
    }
  }
`;

export const ImageSection = styled.section`
  position: relative;
  margin-left: auto;
  display: none;

  @media (min-width: 992px) {
    display: block;
  }

  img {
    position: relative;
    z-index: 5;
  }

  &::before {
    content: "";
    position: absolute;
    background: url(${buildPath});
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    width: 800px;
    height: 800px;
    right: -400px;
    top: -280px;
    z-index: 0;
  }
`;
