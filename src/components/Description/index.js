import React from "react";
import marcaPath from "../../images/marcas.png";

import * as S from "./styled";

function Description() {
  return (
    <S.Container id="description">
      <S.Wrapper>
        <S.Content>
          <h3>The process we follow</h3>
          <p
            data-sal="slide-up"
            data-sal-delay="300"
            data-sal-easing="ease-in-out"
          >
            Tell us your what services you would like and get your own free
            *.ltiaas.com subdomain with a private API just for you.
            Write some very simple code to connect your website or service to
            your private LTI API service hosted by LTIAAS.
          </p>
          <p
            data-sal="slide-up"
            data-sal-delay="400"
            data-sal-easing="ease-in-out"
          >
            LTIAAS then automates the connection between your website or service to all
            LTI compatible LMSs around the globe. Want to make it even easier for LMSs
            to connect with you? Enable our industry-first dynamic registration service 
            to make initial customer connections with LTI push-button.
          </p>
        </S.Content>

        <S.ContainerBanner>
          <S.ImageBanner src={marcaPath} alt="marca path" />
        </S.ContainerBanner>
      </S.Wrapper>

      <S.Flow>
        <li>
          <h5>Launch</h5>
          <p data-sal="slide-right" data-sal-delay="100" data-sal-easing="ease">
            After installing your tool in an LMS, users can <b>launch</b> your tool
            via the *.ltiaas.com subdomain we provide.
          </p>
        </li>
        <li>
          <h5>Redirect</h5>
          <p data-sal="slide-right" data-sal-delay="100" data-sal-easing="ease">
            LTIAAS handles all the authentication. After a secure handshake with
            the LMS, we <b>redirect</b> the user to your tool with a secure key.
          </p>
        </li>
        <li>
          <h5>Endpoints</h5>
          <p data-sal="slide-right" data-sal-delay="100" data-sal-easing="ease">
            Use the secure key we send with each user to access additional <b>endpoints</b> in
            the LTIAAS API such as assignments and grades.
          </p>
        </li>
        <li>
          <h5>Deploy</h5>
          <p data-sal="slide-right" data-sal-delay="100" data-sal-easing="ease">
            When you're ready to go live, deploy your LTI integration to customers
            using the programming language and framework you prefer.
          </p>
        </li>
      </S.Flow>
    </S.Container>
  );
}

export default Description;
