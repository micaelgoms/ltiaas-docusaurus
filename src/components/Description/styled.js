import styled from "styled-components";
import lineDash from "../../images/line_dash.svg";
import flow from "../../images/Flow/1.svg";
import flowLaunch from "../../images/Flow/2.svg";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5em;
`;

export const Wrapper = styled.div`
  display: flex;
  margin: 1em auto;

  width: 100%;
  max-width: 1200px;
  padding: 0 1rem;
  justify-content: center;
  align-items: center;

  flex-wrap: wrap;
  flex-direction: column;
  position: relative;

  &::after {
    position: absolute;
    background: no-repeat center url(${flowLaunch});
    background-size: contain;
    height: 100%;
    top: 365px;
    right: 0px;
    width: 90px;
  }

  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

export const Content = styled.div`
  h3 {
    margin-bottom: 1rem;
    position: relative;

    &::before {
      content: "";
      position: absolute;
      background: no-repeat center url(${flow});
      background-size: contain;
      height: 420px;
      width: 700px;
      bottom: -20px;
      left: 0;
      z-index: -5;
      display: none;

      @media (min-width: 1200px) {
        display: block;
      }
    }
  }

  p {
    margin-bottom: 1.5rem;
  }

  margin-bottom: 1rem;
  margin-right: 1rem;
  flex: 1;
`;

export const ContainerBanner = styled.div`
  overflow-x: hidden;
  margin: 1rem;
  flex: 1;
`;

export const ImageBanner = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
`;

export const Flow = styled.ul`
  display: flex;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
  height: 260px;

  padding: 2rem 1rem;
  padding-right: 4rem;
  margin-top: 32px;

  overflow-x: auto;

  @media (min-width: 769px) {
    padding-right: 1rem;
  }

  li {
    list-style: none;
    position: relative;
    margin-right: 1rem;
    min-width: 250px;

    h5 {
      font-family: Ubuntu;
      line-height: 30px;
      color: #64585c;
      margin-bottom: 1rem;
      font-weight: 600;
    }

    p {
      padding-right: 2rem;
    }

    &::before {
      content: "";
      background: #f2275d;
      position: absolute;
      width: 20px;
      height: 20px;
      border-radius: 50%;
      margin-top: -26px;
    }

    &::after {
      content: "";
      position: absolute;
      background: url(${lineDash});
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
      width: 100%;
      height: 3px;
      top: -17px;
    }

    &:last-child {
      h5 {
        position: relative;

        &::before {
          content: "";
          position: absolute;
          background: no-repeat center url(${flowLaunch});
          background-size: contain;
          height: 230px;
          top: -18px;
          right: -24px;
          width: 80px;
        }
      }
    }
  }
`;
