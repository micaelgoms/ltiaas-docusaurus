import styled from "styled-components";
import AnchorLink from "react-anchor-link-smooth-scroll";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 0 auto;
  background: #023e73;
  z-index: 10;
  position: relative;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;

  padding: 0 1rem;
  position: relative;
`;

export const ButtonToTop = styled(AnchorLink)`
  background: transparent;
  color: #fff;
  border: none;
  cursor: pointer;
  position: absolute;
  z-index: 0;
  transition: transform 0.2s ease-in-out;
  bottom: -25px;
  right: 16px;

  @media (min-width: 992px) {
    right: 0px;
  }

  &:hover {
    transform: translateY(-3px);
  }

  &::before {
    content: "";
    background: #f2275d;
    position: absolute;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    z-index: -5;
    top: 2px;
    right: 1px;
  }
`;

export const Branding = styled.div`
  padding: 48px 0;
  max-width: 255px;

  p {
    color: #fff;
    font-weight: 300;
    font-size: 14px;
    line-height: 24px;
    margin: 1rem 0 2rem;
  }

  ul {
    margin-left: 6px;
    display: flex;

    li {
      position: relative;
      z-index: 5;
      list-style: none;
      margin-right: 24px;

      svg {
        color: #fff;
      }

      &::before {
        content: "";
        position: absolute;
        width: 30px;
        height: 30px;
        background: #1b5181;
        z-index: -1;
        border-radius: 50%;
        top: -3px;
        left: -6px;
      }
    }
  }
`;

export const Links = styled.div`
  display: flex;
  flex-basis: 300px;
  justify-content: space-between;
  margin-bottom: 2rem;

  @media (min-width: 769px) {
    flex-basis: 30%;
  }

  ul {
    flex: 1;

    &:first-child {
      margin-right: 32px;
    }

    @media (min-width: 769px) {
      min-width: auto;
    }

    li {
      list-style: none;
      font-weight: normal;
      font-size: 14px;
      white-space: nowrap;

      a {
        color: #d9dbe1;
        text-decoration: none;
      }

      p {
        margin-bottom: 6px;
        font-size: 18px;
        color: #ffffff;
      }
    }
  }
`;

export const Copyright = styled.div`
  display: block;
  text-align: center;
  background: #013661;
  color: #d9dbe1;
  padding: 1rem 0;

  img {
    width: 15px;
    margin-left: 8px;
  }
`;
