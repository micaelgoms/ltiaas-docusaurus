import React from "react";
import UilInstagram from "@iconscout/react-unicons/icons/uil-instagram";
import UilFacebook from "@iconscout/react-unicons/icons/uil-facebook-f";
import UilTwitter from "@iconscout/react-unicons/icons/uil-twitter";
import UilYoutube from "@iconscout/react-unicons/icons/uil-youtube";
import { UilAngleUp } from "@iconscout/react-unicons";

import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";
import { LocalizedLink } from "gatsby-theme-i18n";

import * as S from "./styled";
import imgBr from "../../images/br.png";

const Footer = () => {
  const imgLogo = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "brand/white.png" }) {
        childImageSharp {
          fixed(width: 100) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
  `);

  return (
    <S.Container>
      <S.Wrapper>
        <S.Branding>
          <Img
            fixed={imgLogo.file.childImageSharp.fixed}
            alt="Lti as a service"
          />

          <p>
            Connect your platform with a new way of teaching. 
          </p>

          <ul>
            <li>
              <UilInstagram size={18} />
            </li>
            <li>
              <UilFacebook size={18} />
            </li>
            <li>
              <UilTwitter size={18} />
            </li>
            <li>
              <UilYoutube size={18} />
            </li>
          </ul>
        </S.Branding>

        <S.Links>
          <ul>
            <li>
              <p>Company</p>
            </li>

            <li>
              <LocalizedLink to="/pricing" language="en">
                Pricing
              </LocalizedLink>
            </li>

            <li>
              <LocalizedLink to="/terms" language="en">
                Terms &amp; Conditions
              </LocalizedLink>
            </li>

            <li>
              <LocalizedLink to="/privacy-policy" language="en">
                Privacy Policy
              </LocalizedLink>
            </li>

            <li>
              <LocalizedLink to="/service-level-agreement" language="en">
                Service Level Agreement
              </LocalizedLink>
            </li>

            <li>
              <LocalizedLink to="/compliance" language="en">
                Compliance
              </LocalizedLink>
            </li>
          </ul>

          <ul>
            <li>
              <p>Help &amp; Support</p>
            </li>

            <li>
              <LocalizedLink to="/faq" language="en">
                FAQ
              </LocalizedLink>
            </li>

            <li>
              <a
                href="https://ltiaas.com/docs"
                target="_blank"
                rel="noopener noreferrer"
              >
                Documentation
              </a>
            </li>

            <li>
              <LocalizedLink to="/contact-us" language="en">
                Contact Us
              </LocalizedLink>
            </li>
            <li>
              <a href="https://portal.ltiaas.com" >
                Customer Portal
              </a>
            </li>
          </ul>
        </S.Links>

        <S.ButtonToTop offset="300" href="#top">
          <UilAngleUp size={42} />
        </S.ButtonToTop>
      </S.Wrapper>

      <S.Copyright>
        <small>© 2020 LTI as a Service - LTIAAS. All rights reserved</small>
        <LocalizedLink to="/" language="br">
          <img src={imgBr} alt="Brasil Flag" />
        </LocalizedLink>
      </S.Copyright>
    </S.Container>
  );
};

export default Footer;
