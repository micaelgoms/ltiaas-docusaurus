import styled from "styled-components";

export const Container = styled.div`
  margin: 0 0.5rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  flex: 1;
  width: calc(100% - 10px);
  max-width: 100%;

  @media (min-width: 992px) {
    max-width: 400px;
  }

  h4 {
    padding: 0 1rem;
    line-height: 28px;
    margin-bottom: 0.5rem;

    @media (min-width: 769px) {
      line-height: 32px;
    }
  }
`;

export const ContainerCards = styled.div`
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  overflow-x: auto;
  overflow-y: hidden;
  padding: 0 4px;
  padding-bottom: 4px;

  @media (min-width: 992px) {
    flex-direction: column;
    justify-content: space-between;
  }
`;
