import styled, { css } from "styled-components";

export const Card = styled.div`
  background: #e6f5ff;
  padding: 1rem;
  border-radius: 5px;
  display: flex;
  flex-direction: column;

  cursor: pointer;
  transition: all 0.2s ease-in-out;

  height: 100%;
  min-width: 275px;
  box-shadow: 0 2px 4px 0px rgb(0 0 0 / 10%);

  ${(props) =>
    props.open &&
    css`
      background: #023e73;

      h5 {
        color: #e6f5ff;
      }
    `}

  @media (min-width: 992px) {
    ${(props) =>
      props.open &&
      css`
        background: #e6f5ff;

        h5 {
          color: #023e73;
        }
      `}
  }

  & + div {
    margin-top: 0;
    margin-left: 0.5rem;
  }

  @media (min-width: 992px) {
    height: auto;

    & + div {
      margin-top: 0.3rem;
      margin-left: 0;
    }
  }
`;

export const Title = styled.h5`
  font-size: 16px;
  line-height: 19px;
  font-family: Ubuntu;
  font-weight: bold;
`;

export const Content = styled.p`
  color: #64585c;
  font-size: 14px;
  line-height: 18px;
  font-weight: 500;

  height: 0;
  opacity: 0;
  margin-top: 0;

  transition: all 0.1s ease-in-out;

  @media (min-width: 992px) {
    ${(props) =>
      props.open &&
      css`
        height: auto;
        opacity: 1;
        margin-top: 1rem;
      `}
  }
`;
