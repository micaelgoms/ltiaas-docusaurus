import React, { useEffect, useState } from "react";
import { useCodeHightlight } from "../../../hooks/codeHightlight";

import * as S from "./styled";

function Card({ id, title, answer, current, setCurrent }) {
  const [open, setOpen] = useState(current === id);
  const { setCurrentCode } = useCodeHightlight();

  const handleOpened = () => {
    setCurrent(id);
    setCurrentCode(id);
  };

  useEffect(() => {
    if (current === id) {
      setOpen(true);
    } else {
      setOpen(false);
    }
  }, [id, current]);

  return (
    <S.Card open={open} onClick={handleOpened}>
      <S.Title>{title}</S.Title>
      <S.Content open={open}>{answer}</S.Content>
    </S.Card>
  );
}

export default Card;
