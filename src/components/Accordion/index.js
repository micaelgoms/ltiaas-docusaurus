import React, { useState } from "react";
import Card from "./Card";
import { questions } from "./content";

import * as S from "./styled";

function Accordion() {
  const [current, setCurrent] = useState(0);

  return (
    <S.Container>
      <h4>A very powerful and easy to use RESTful API</h4>

      <S.ContainerCards>
        {questions.map((question, i) => (
          <Card
            key={i}
            id={i}
            title={question.title}
            answer={question.answer}
            current={current}
            setCurrent={setCurrent}
          />
        ))}
      </S.ContainerCards>
    </S.Container>
  );
}

export default Accordion;
