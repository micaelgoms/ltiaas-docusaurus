export const questions = [
  {
    title: "LTI Launch - Retrieve an ID Token",
    answer:
      "Receive launch requests and securely validate the user with the ltiaas.com API",
  },
  {
    title: "Memberships - Get user information",
    answer:
      "Get the current user's id, email, name, roles, and more inside the current Platform context.",
  },
  {
    title: "Submit a Grade",
    answer:
      "Submit a new grade for the assignment your tool is providing.",
  },
  {
    title: "Deep Linking",
    answer:
      "Provide the user with a selection screen embedded in their LMS.",
  },
  {
    title: "Platform Registration",
    answer:
      "Register your tool with an LMS to enable the required secure authentication.",
  },
];
