import React from "react";

import * as S from "./styled";

import { steps } from "./content";

const Steps = () => {
  return (
    <S.Container>
      <S.Wrapper>
        <div>
          <h3>choose the best solution</h3>
          <p>
            The LTIAAS API is trusted by many in ed-tech because of how simple
            it is to integrate and how reliable it is to use. LTIAAS can typically
            be integrated into an existing product in under a week.
          </p>
        </div>

        <S.StepList>
          {steps.map((step, i) => (
            <li
              key={i}
              data-sal="slide-up"
              data-sal-delay={100 * (i + 2)}
              data-sal-easing="ease-in-out"
            >
              <h4>
                <span>{step.step}</span>
                day
              </h4>
              <p>{step.text}</p>
            </li>
          ))}
        </S.StepList>
      </S.Wrapper>
    </S.Container>
  );
};

export default Steps;
