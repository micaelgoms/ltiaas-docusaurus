export const steps = [
  {
    step: "1",
    text: "Sign-up for your free *.ltiaas.com subdomain and start reading our docs. Want more help? We offer consulting services too.",
  },
  {
    step: "2",
    text: "Set up your launch and deep-linking URLs to receive HTTP-GET requests after we authenticate users and test it out on an LTIAAS test LMS.",
  },
  {
    step: "3",
    text: "Design your landing pages for LTI launches and write some simple code to get the authenticated user information from an LTIAAS API query.",
  },
  {
    step: "4",
    text: "Add features to your app with more advanced API calls like assignments and grades, and names and roles.",
  },
  {
    step: "5",
    text: "Do more testing of your app in a real LTI-capable LMS. Use our demo LMSs if you'd like. Finally, deploy and start connecting with customers!",
  },
];
