import styled from "styled-components";
import flow from "../../images/Flow/4.svg";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 10em auto;
  margin-bottom: 0;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
  height: 100%;

  position: relative;

  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    width: 100%;
    margin-bottom: 2rem;

    h3 {
      position: relative;
      text-align: center;

      &::before {
        content: "";
        position: absolute;
        background: url(${flow});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        width: 250px;
        height: 160px;
        top: -160px;
        left: -50px;
        z-index: -5;

        @media (min-width: 992px) {
          top: -120px;
          left: -260px;
        }
      }
    }

    p {
      font-weight: 400;
      font-size: 18px;
      max-width: 500px;
      text-align: center;
    }
  }
`;

export const StepList = styled.ul`
  display: flex;
  justify-content: space-between;
  align-items: center;

  overflow-x: auto;
  overflow-y: hidden;

  @media (min-width: 769px) {
    padding: 0 1rem;
  }

  li {
    list-style: none;
    min-width: 200px;
    width: 100%;
    height: 100%;
    flex: 1;
    margin: 0.5rem;
    display: flex;
    flex-direction: column;
    padding: 0.5rem;

    h4 {
      text-align: center;

      span {
        font-size: 48px;
        margin-right: 1rem;
      }
    }

    p {
      text-align: center;
    }
  }
`;
