import React from "react";
import Loadable from 'react-loadable';


const LoadablePlot = Loadable({
  loader: () => import('react-plotly.js'),
  loading() {
    return <div>Loading...</div>;
  },
});

function PricingPlot(options) {

  const x = makeArr(1,options.mau,25);
  const total_items = x[x.length - 1];
  let y = [];
  let total_1_1000 = 0;
  let total_1001_20000 = 0;
  let total_20001_100000 = 0;
  let total_100001 = 0;
  let total_dyn_reg = 0;
  if(options.mau < 25) {
    y = x.map(x => x*0);
  } else {
    total_1_1000 = 0.02;
    total_1001_20000 = 0.014;
    total_20001_100000 = 0.008;
    total_100001 = 0.0052;
    if(options.dl) {
      total_1_1000 += 0.003;
      total_1001_20000 += 0.0022;
      total_20001_100000 += 0.0012;
      total_100001 += 0.0008;
    }
    if(options.grades) {
      total_1_1000 += 0.006;
      total_1001_20000 += 0.0042;
      total_20001_100000 += 0.0024;
      total_100001 += 0.0016;
    }
    if(options.names) {
      total_1_1000 += 0.004;
      total_1001_20000 += 0.0028;
      total_20001_100000 += 0.0016;
      total_100001 += 0.001;
    }
    if(options.moodlePlugin) {
      total_1_1000 += 0.01;
      total_1001_20000 += 0.007;
      total_20001_100000 += 0.004;
      total_100001 += 0.0026;
    }
    if(options.dynreg) {
      total_dyn_reg = 10;
    }

    y = x.map(x => {
      if(total_items <= 25) {
        return 0;
      } else if(x < 1001) {
        return Math.round(100*(x*total_1_1000 + total_dyn_reg))/100;
      } else if(x < 20001) {
        return Math.round(100*((x-1000)*total_1001_20000 + (total_1_1000*1000) + total_dyn_reg))/100;
      } else if(x < 100001) {
        return Math.round(100*((x-20000)*total_20001_100000 + (total_1001_20000*18999) + (total_1_1000*1000) + total_dyn_reg))/100;
      } else {
        return Math.round(100*((x-100000)*total_100001 + (total_20001_100000*79999) + (total_1001_20000*18999) + (total_1_1000*1000) + total_dyn_reg))/100;
      }
    });
  }


  return (
    <LoadablePlot
        data={[
          {
            x: x,
            y: y,
            type: 'scatter',
            fill: 'tozeroy',
            marker: {color: '#023F73'},
          },
        ]}
        layout={{
          modebar: {
            remove: ["autoScale2d", "autoscale", "editInChartStudio", "editinchartstudio", "hoverCompareCartesian", "hovercompare", "lasso", "lasso2d", "orbitRotation", "orbitrotation", "pan", "pan2d", "pan3d", "reset", "resetCameraDefault3d", "resetCameraLastSave3d", "resetGeo", "resetSankeyGroup", "resetScale2d", "resetViewMapbox", "resetViews", "resetcameradefault", "resetcameralastsave", "resetsankeygroup", "resetscale", "resetview", "resetviews", "select", "select2d", "sendDataToCloud", "senddatatocloud", "tableRotation", "tablerotation", "toImage", "toggleHover", "toggleSpikelines", "togglehover", "togglespikelines", "toimage", "zoom", "zoom2d", "zoom3d", "zoomIn2d", "zoomInGeo", "zoomInMapbox", "zoomOut2d", "zoomOutGeo", "zoomOutMapbox", "zoomin", "zoomout"]
          },
          showlegend: false,
          title: `Pricing for ${options.mau} MAU`,
          xaxis: {
            showline: true,
            showgrid: false,
            showticklabels: true,
            linecolor: 'rgb(204,204,204)',
            linewidth: 2,
            ticks: 'outside',
            tickcolor: 'rgb(204,204,204)',
            tickwidth: 2,
            ticklen: 5,
            tickfont: {
              family: 'Arial',
              size: 12,
              color: 'rgb(82, 82, 82)'
            },
            title: 'Monthly Active Users (MAU)'
          },
          yaxis: {
            showgrid: false,
            zeroline: false,
            showline: false,
            showticklabels: false,
            title: 'Monthly Bill (USD)'
          },
          autosize: true,
          margin: {
            autoexpand: false,
            l: 50,
            r: 120,
            t: 30,
            b: 50
          },
          annotations: [
            {
              xref: 'paper',
              x: 1.0,
              y: y[y.length-1],
              xanchor: 'left',
              yanchor: 'middle',
              text: `Total: $${y[y.length-1]}`,
              showarrow: false,
              font: {
                family: 'Arial',
                size: 16,
                color: 'black'
              }
            }
          ]
        }}
        useResizeHandler
      />
  );
}

function makeArr(startValue, stopValue, cardinality) {
  var arr = [];
  var step = (stopValue - startValue) / (cardinality - 1);
  for (var i = 0; i < cardinality; i++) {
    arr.push(startValue + Math.round(step * i));
  }
  return arr;
}


export default PricingPlot;
