import styled, { keyframes } from "styled-components";
import chechPath from "../../images/check.svg";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
  height: 100%;
`;

export const PlanSection = styled.section`
  max-width: 1000px;
  width: 100%;
  margin: 0 auto;
`;

export const CorePlan = styled.div`
  list-style: none;
  width: 100%;
  display: none;
  flex-direction: column;
  align-items: center;

  background: #ffffff;
  border: 2px solid #e6f5ff;
  box-sizing: border-box;
  border-radius: 10px;

  min-width: 200px;
  padding: 1rem;
  margin-bottom:1rem !important;
  transition: border 0.2s ease-in-out;

  @media (min-width: 992px) {
    display: flex;
  }

  &:hover {
    border-color: #f2275d;
  }

  img {
    width: 120px;
    height: 150px;
  }

  h5 {
    margin: 1rem 0;
    color: #64585c;
  }

  div {
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    min-height: 200px;

    position: relative;

    width: auto;

    span {
      font-size: 14px;

      &::before {
        content: "";
        position: absolute;
        background: url(${chechPath});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        width: 24px;
        height: 24px;
        left: -32px;
        margin-top: 3px;
      }
    }
  }
`;

export const CorePlanInsidePlanList = styled.div`
  list-style: none;
  width: 100%;
  height: 480px;
  display: flex;
  flex-direction: column;
  align-items: center;

  background: #ffffff;
  border: 2px solid #e6f5ff;
  box-sizing: border-box;
  border-radius: 10px;

  min-width: 200px;
  padding: 1rem;

  margin: 0 !important;

  @media (min-width: 600px) {
    max-width: 100%;
  }

  @media (min-width: 992px) {
    display: none;
  }

  img {
    width: 120px;
    height: 150px;
  }

  h5 {
    margin: 1rem 0;
    color: #64585c;
  }

  div {
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    margin: 0 !important;

    position: relative;

    height:100%;
    width: 100%;

  }
`;

export const PlotHolder = styled.ul`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  margin: 0 !important;

  li {
    list-style: none;
    width: 100%;
    height: auto;
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;

    background: #ffffff;
    border: 2px solid #e6f5ff;
    box-sizing: border-box;
    border-radius: 10px;

    max-width: 330px;
    min-width: 300px;
    padding: 1rem;

    transition: border 0.2s ease-in-out;

    &:hover {
      border-color: #f2275d;
    }

    img {
      width: 120px;
      height: 150px;
    }

    h5 {
      margin: 1rem 0;
      color: #64585c;
    }

    div {
      display: flex;
      align-items: flex-start;
      justify-content: flex-start;
      min-height: 80px;
      flex-direction: column;

      position: relative;

      left: 50%;
      margin-left: -95%;
      width: auto;

      span {
        font-size: 14px;

        &::before {
          content: "";
          position: absolute;
          background: url(${chechPath});
          background-repeat: no-repeat;
          background-position: center;
          background-size: contain;
          width: 24px;
          height: 24px;
          left: -32px;
          margin-top: 3px;
        }
      }
    }
  }
`;

const arrowAnimation = keyframes`
  0% {
    transform: scale(1.05);
  }

  50% {
    transform: scale(1);
  }

  100% {
    transform: scale(1.05);
  }
`;

export const Form = styled.form`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;

  label:first-of-type {
    width:100%;
  }
  label {
    color: #023e73;
    font-family: Russo One;
    margin: 0.5rem 0;
    min-width: 240px;

    input,
    select {
      width: 100%;
      height: 40px;
      border: 2px solid #023e73;
      background: transparent;
      border-radius: 4px;
      padding: 0 0.5rem;
    }

    select {
      font-family: Russo One;
      font-size: 18px;
      color: #023e73;
      height: 50px;
    }

    textarea {
      width: 100%;
      height: 200px;
      border: 2px solid #023e73;
      background: transparent;
      border-radius: 4px;
      padding: 0.5rem;
      font: 16px Ubuntu, sans-serif;
      resize: none;
    }
  }

  .checkbox {
    width: unset;
    height: unset;
    margin: 1rem;
  }

  button {
    display: flex;
    justify-content: center;
    align-items: center;
    background: #f2275d;
    border-radius: 50px;
    color: #ffffff;
    text-decoration: none;

    width: 200px;
    height: 40px;
    font-size: 16px;

    cursor: pointer;
    font-family: Russo One;
    border: none;

    transition: transform 0.2s ease-in-out;

    &:hover {
      transform: translateY(-3px);
    }

    svg {
      animation: ${arrowAnimation} 1s ease-in-out infinite;
      margin-left: 8px;
    }
  }
`;