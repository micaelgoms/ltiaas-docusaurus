import React from "react";
import Accordion from "../Accordion";
import Highlight from "../Highlight";

import * as S from "./styled";

function Samples() {
  return (
    <S.Container
      id="cases"
      data-sal="fade"
      data-sal-delay="50"
      data-sal-easing="ease-in-out"
    >
      <S.Wrapper>
        <S.Content>
          <Highlight />
          <Accordion />
        </S.Content>
      </S.Wrapper>
    </S.Container>
  );
}

export default Samples;
