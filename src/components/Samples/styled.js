import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  margin: 1em auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
`;

export const Content = styled.div`
  display: flex;
  flex-wrap: wrap;

  position: relative;
  flex-direction: column-reverse;

  @media (min-width: 992px) {
    flex-direction: row;
  }
`;
