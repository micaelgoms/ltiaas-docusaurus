import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  background: linear-gradient(180deg, #e6f5ff 0%, #ffffff 100%);
  -webkit-font-smoothing: antialiased;

  width: 100%;
  height: 100vh;
`;

export const Wrapper = styled.div`
  display: flex;
  margin: auto;

  width: 100%;
  max-width: 1200px;
  padding: 0 1rem;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  img {
    width: 100%;
    height: 100%;

    max-width: 500px;
  }
`;

export const contentMsg = styled.div`
  text-align: center;

  a {
    font-weight: 800;

    &:link {
      color: #f2275d;
    }

    &:visited {
      color: #f2275d;
    }

    &:hover {
      color: #f2275d;
    }

    &:active {
      color: #f2275d;
    }
  }
`;
