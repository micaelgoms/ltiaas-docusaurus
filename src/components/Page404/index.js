import React from "react";
import * as S from "./styled";
import GlobalStyle from "../../styles/global";
import { LocalizedLink } from "gatsby-theme-i18n";

import src404 from "../../images/404.svg";

function Page404() {
  return (
    <>
      <GlobalStyle />
      <S.Container>
        <S.Wrapper>
          <img src={src404} alt="" />

          <S.contentMsg>
            <h2>Page not found!</h2>
            <p>Sorry, we couldn’t find what you were looking for.</p>

            <LocalizedLink to="/" language="en">
              Go Home
            </LocalizedLink>
          </S.contentMsg>
        </S.Wrapper>
      </S.Container>
    </>
  );
}

export default Page404;
