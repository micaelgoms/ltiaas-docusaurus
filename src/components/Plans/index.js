import React from "react";

import * as S from "./styled";

import { core, addons } from "./content";
import { LocalizedLink } from "gatsby-theme-i18n";
import UilPriceTag from "@iconscout/react-unicons/icons/uil-signin";
import UilCreateDashboard from "@iconscout/react-unicons/icons/uil-create-dashboard";

const Plans = () => {
  return (
    <S.Container id="pricing">
      <S.Wrapper>
        <S.TitleWrapper>
          <h3>Choose Your Plan</h3>
          <p>
            Let's choose the package that is best for you. Unlike some of our
            competitors, at LTIAAS we believe in letting you choose only what
            you need and not making you buy something you don't. Use our
            <S.BlueButton to="/pricing" language="en">Pricing Calculator</S.BlueButton>
            to understand your costs before you subscribe to LTIAAS.
          </p>
        </S.TitleWrapper>

        <S.PlanSection>
          <S.CorePlan>
            <img src={core.image} alt={core.title} />
            <h5>{core.title}</h5>

            <div>
              {core.features.map((feat, i) => (
                <span key={i}>{feat}</span>
              ))}
            </div>

            <h4>{core.price}</h4>
            <small>MAU = Monthly Active Users</small>
          </S.CorePlan>

          <S.PlansList>
            <S.CorePlanInsidePlanList>
              <img src={core.image} alt={core.title} />
              <h5>{core.title}</h5>

              <div>
                {core.features.map((feat, i) => (
                  <span key={i}>{feat}</span>
                ))}
              </div>

              <h4>{core.price}</h4>
              <small>MAU = Monthly Active Users</small>
            </S.CorePlanInsidePlanList>

            {addons.map((addon, i) => (
              <li key={i}>
                <p><UilCreateDashboard size={22} /> Addon</p>
                <h5>{addon.title}</h5>

                <div>
                  {addon.features.map((feat, i) => (
                    <span key={i}>{feat}</span>
                  ))}
                </div>

                <h4>{addon.price}</h4>
              </li>
            ))}
          </S.PlansList>
        </S.PlanSection>
        <S.ContainerThinFootnote>
          <p>Learn more about how our pricing scales as you grow on our <a href="/pricing">pricing</a> page.</p>
        </S.ContainerThinFootnote>
        <S.ContainerThin>
          <S.CenterWrapper>
            <h5>Get started for free with our self-service portal:</h5>
            <LocalizedLink to="https://portal.ltiaas.com" language="en">
              <UilPriceTag /> Customer Portal
            </LocalizedLink>
          </S.CenterWrapper>
        </S.ContainerThin>
      </S.Wrapper>
    </S.Container>
  );
};

export default Plans;
