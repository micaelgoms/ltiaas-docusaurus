export const core = {
  image: require("../../images/plans/free.svg").default,
  title: "Core: Base Package",
  features: [
    "Auth",
    "Launch",
    "ID Token",
    "Unlimited Platforms",
    "Custom Subdomain",
  ],
  price: "$ 0.02 / MAU",
};

export const addons = [
  {
    title: "Deep Linking",
    features: ["Multiple Apps", "Custom Activities"],
    price: "+ $ 0.003 / MAU",
  },
  {
    title: "Names and Roles",
    features: ["List all students in a class", "Identify teachers and admins"],
    price: "+ $ 0.004 / MAU",
  },
  {
    title: "Assignments and Grades",
    features: ["Get a list of assignments", "Post grades back to class"],
    price: "+ $ 0.006 / MAU",
  },
  {
    title: "Dynamic Registration",
    features: ["Simplify LMS registration", "Lower barrier to entry"],
    price: "+ $ 10 / Month",
  },
];
