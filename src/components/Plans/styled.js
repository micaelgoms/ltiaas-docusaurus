import styled from "styled-components";
import chechPath from "../../images/check.svg";
import flow from "../../images/Flow/5.svg";
import { LocalizedLink } from "gatsby-theme-i18n";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 0 auto;
  margin-bottom: 8em;
`;

export const ContainerThin = styled.div`
  display: flex;
  flex-direction: column;

  margin: 0 auto;
  margin-top: 1rem;
`;

export const ContainerThinFootnote = styled.div`
  display: flex;
  flex-direction: column;

  margin: 0 auto;
  margin-bottom: 1rem;
  @media (min-width: 992px) {
    width: 970px;
  }
  font-size: 10pt;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
  height: 100%;
`;

export const CenterWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  border-radius: 16px;
  background: #e6f5ff;

  @media (min-width: 992px) {
    width: 860px;
  }
  width: 100%;
  margin: 0 auto;
  padding: 0 1rem;

  overflow-x: hidden;
  padding-top: 1rem;
  padding-bottom: 1rem;

  a {
    display: flex;
    justify-content: center;
    align-items: center;
    background: #f2275d;
    border-radius: 50px;
    color: #ffffff;
    text-decoration: none;

    width: 200px;
    height: 40px;
    font-size: 16px;

    cursor: pointer;
    font-family: Russo One;
    border: none;

    transition: transform 0.2s ease-in-out;

    &:hover {
      transform: translateY(-3px);
    }
    svg {
      margin-right: 8px;
    }
  }
`

export const BlueButton = styled(LocalizedLink)`
  margin: 0 0.2rem;
  background: #023e73;
  border-radius: 50px;
  padding: 4px 10px;
  color: #fff;
  text-decoration: none;
  font-weight: 800;
  font-size: 16px;

  transition: transform 0.2s ease-in-out;

  &:hover {
    background: #01213D;
  }

`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  margin-bottom: 2rem;
  padding: 0 1rem;

  overflow-x: hidden;
  padding-top: 10em;

  @media (min-width: 992px) {
    overflow-x: unset;
  }

  h3 {
    position: relative;

    &::before {
      content: "";
      position: absolute;
      background: url(${flow});
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
      width: 250px;
      height: 150px;
      top: -160px;
      right: -120px;
      z-index: -5;

      @media (min-width: 992px) {
        top: -110px;
        right: -260px;
      }
    }
  }

  p {
    font-weight: 400;
    font-size: 18px;
    max-width: 500px;
    text-align: center;
  }
`;

export const PlanSection = styled.section`
  display: flex;
  max-width: 1000px;
  width: 100%;
  margin: 0 auto;
`;

export const CorePlan = styled.div`
  list-style: none;
  width: 100%;
  height: 615px;
  display: none;
  flex-direction: column;
  align-items: center;

  background: #ffffff;
  border: 2px solid #e6f5ff;
  box-sizing: border-box;
  border-radius: 10px;

  max-width: 330px;
  min-width: 300px;
  padding: 2rem 0;
  margin: 8px;
  transition: border 0.2s ease-in-out;

  @media (min-width: 992px) {
    display: flex;
  }

  &:hover {
    border-color: #f2275d;
  }

  img {
    width: 120px;
    height: 150px;
  }

  h5 {
    margin: 1rem 0;
    color: #64585c;
  }

  div {
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    min-height: 200px;

    position: relative;

    left: 50%;
    margin-left: -95%;
    width: auto;

    span {
      font-size: 14px;

      &::before {
        content: "";
        position: absolute;
        background: url(${chechPath});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        width: 24px;
        height: 24px;
        left: -32px;
        margin-top: 3px;
      }
    }
  }
`;

export const CorePlanInsidePlanList = styled.div`
  list-style: none;
  width: 100%;
  height: 568px;
  display: flex;
  flex-direction: column;
  align-items: center;

  background: #ffffff;
  border: 2px solid #e6f5ff;
  box-sizing: border-box;
  border-radius: 10px;

  max-width: 330px;
  min-width: 300px;
  padding: 2rem 0;

  margin: 8px auto;
  transition: border 0.2s ease-in-out;

  @media (min-width: 600px) {
    max-width: 100%;
    margin: 30px;
  }

  @media (min-width: 992px) {
    display: none;
  }

  &:hover {
    border-color: #f2275d;
  }

  img {
    width: 120px;
    height: 150px;
  }

  h5 {
    margin: 1rem 0;
    color: #64585c;
  }

  div {
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    min-height: 200px;

    position: relative;

    left: 50%;
    margin-left: -95%;
    width: auto;

    span {
      font-size: 14px;

      &::before {
        content: "";
        position: absolute;
        background: url(${chechPath});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        width: 24px;
        height: 24px;
        left: -32px;
        margin-top: 3px;
      }
    }
  }
`;

export const PlansList = styled.ul`
  display: flex;
  align-items: center;
  flex-wrap: wrap;

  li {
    list-style: none;
    width: 100%;
    height: auto;
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;

    background: #ffffff;
    border: 2px solid #e6f5ff;
    box-sizing: border-box;
    border-radius: 10px;

    max-width: 330px;
    min-width: 300px;
    padding: 2rem 0;

    transition: border 0.2s ease-in-out;
    margin: 8px auto;

    @media (min-width: 992px) {
      margin: 8px;
    }

    &:hover {
      border-color: #f2275d;
    }

    img {
      width: 120px;
      height: 150px;
    }

    h5 {
      margin: 0.5rem 0;
      color: #64585c;
    }

    p {
      padding: 4px 20px;
      border-radius: 32px;
      background-color: #ff0059;
      color: white;

      svg {
        margin-bottom: -4px;
      }
    }

    div {
      display: flex;
      align-items: flex-start;
      justify-content: flex-start;
      min-height: 80px;
      flex-direction: column;

      position: relative;

      left: 50%;
      margin-left: -95%;
      width: auto;

      span {
        font-size: 14px;

        &::before {
          content: "";
          position: absolute;
          background: url(${chechPath});
          background-repeat: no-repeat;
          background-position: center;
          background-size: contain;
          width: 24px;
          height: 24px;
          left: -32px;
          margin-top: 3px;
        }
      }
    }
  }
`;
