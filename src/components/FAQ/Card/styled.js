import styled, { css } from "styled-components";

export const Card = styled.div`
  background: #e6f5ff;
  padding: 1rem;
  border-radius: 5px;
  display: flex;
  flex-direction: column;

  cursor: pointer;
  transition: all 0.2s ease-in-out;

  box-shadow: 0 2px 4px 0px rgb(0 0 0 / 10%);
  margin-bottom: 0.5rem;
  align-items: start;

  ${(props) =>
    props.open &&
    css`
      background: #e6f5ff;

      h5 {
        color: #023e73;
      }
    `}
`;

export const Title = styled.h5`
  font-size: 16px;
  line-height: 19px;
  font-family: Ubuntu;
  font-weight: bold;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;

export const Content = styled.p`
  color: #64585c;
  font-size: 16px;
  line-height: 28px;
  font-weight: 500;
  display: block;
  overflow: hidden;

  height: 0;
  margin-top: 0;

  transition: all 0.2s ease-in-out;

  ${(props) =>
    props.open &&
    css`
      height: auto;
      margin-top: 1rem;
    `}
`;
