import React, { useState } from "react";
import { UilAngleDown } from "@iconscout/react-unicons";
import { UilAngleUp } from "@iconscout/react-unicons";

import * as S from "./styled";

function Card({ id, title, answer }) {
  const [open, setOpen] = useState(0 === id);

  const handleOpened = () => {
    setOpen(!open);
  };

  return (
    <S.Card open={open} onClick={handleOpened}>
      <S.Title>
        {title}
        {!open ? <UilAngleDown size={24} /> : <UilAngleUp size={24} />}
      </S.Title>
      <S.Content open={open}>{answer}</S.Content>
    </S.Card>
  );
}

export default Card;
