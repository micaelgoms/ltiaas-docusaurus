import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  max-width: 768px;
  margin: 0 auto 2.5em;
  padding: 1rem;
`;

export const ContainerCards = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  overflow-x: auto;
  overflow-y: hidden;
  padding: 0 4px;
  padding-bottom: 4px;
  margin-top: 2rem;
`;
