export const questions = [
  {
    title: "How does pricing work at LTIAAS?",
    answer:
      "We operate on a simple pricing structure with no hidden fees, and you only pay for what you need. Actually, you can start using our API completely for free and only start paying once you start seeing significant API traffic. Once you set up a monthly subscription with us, we track the number of users (your customers) that are making LTI requests and only bill you for the number of active users each month. For example, if you have one user that uses your LTI tool every day, several times a day this month, we only bill you for one Monthly Active User (MAU). If you have 10 users, each using your tool only once this month, we will bill you for 10 MAU.  ",
  },
  {
    title: "How much will I expect to pay?",
    answer: "LTIAAS is completely free if you have 25 or less Monthly Active Users (MAU). This makes it easy to test with our API as you get your LTI tool up and running without any costs. Once you have more than 25 MAU, you will be charged for the first 25 users as well as any additional users. LTIAAS uses graduated pricing, so while the base price for the core service is $0.02/MAU, once you go above 1,000 MAU, the next 19,000 users are billed at a lower price, and so on. You can find more details on our pricing page. "
  },
  {
    title: "Do you offer consulting services?",
    answer: "Yes, We offer consulting services for Integrating LTIAAS into your product and helping you interface with your customers.  Our consulting fees are commensurate with the industry.  Consulting fees are not charged for basic email support and introductory meetings and sales calls. "
  }
];
