import React from "react";
import Card from "./Card";
import { questions } from "./content";

import * as S from "./styled";

function Accordion() {
  return (
    <S.Container id="top">
      <h1>Frequently asked questions</h1>
      <small>Latest update: January 31, 2021</small>

      <S.ContainerCards>
        {questions.map((question, i) => (
          <Card
            key={i}
            id={i}
            title={question.title}
            answer={question.answer}
          />
        ))}
      </S.ContainerCards>
    </S.Container>
  );
}

export default Accordion;
