import styled, { keyframes } from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 2rem auto 5rem;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 768px;
  height: 100%;
  padding: 1rem;

  position: relative;

  h1 {
    margin-bottom: 1rem;
  }

  p {
    margin-bottom: 1rem;
  }
`;

const arrowAnimation = keyframes`
  0% {
    transform: scale(1.05);
  }

  50% {
    transform: scale(1);
  }

  100% {
    transform: scale(1.05);
  }
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;

  label {
    color: #023e73;
    font-family: Russo One;
    margin: 0.5rem 0;

    input,
    select {
      width: 100%;
      height: 40px;
      border: 2px solid #023e73;
      background: transparent;
      border-radius: 4px;
      padding: 0 0.5rem;
    }

    select {
      font-family: Russo One;
      font-size: 18px;
      color: #023e73;
      height: 50px;
    }

    textarea {
      width: 100%;
      height: 200px;
      border: 2px solid #023e73;
      background: transparent;
      border-radius: 4px;
      padding: 0.5rem;
      font: 16px Ubuntu, sans-serif;
      resize: none;
    }
  }

  .checkbox {
    width: unset;
    height: unset;
    margin: 1rem;
  }

  button {
    display: flex;
    justify-content: center;
    align-items: center;
    background: #f2275d;
    border-radius: 50px;
    color: #ffffff;
    text-decoration: none;

    width: 200px;
    height: 40px;
    font-size: 16px;

    cursor: pointer;
    font-family: Russo One;
    border: none;

    transition: transform 0.2s ease-in-out;

    &:hover {
      transform: translateY(-3px);
    }

    svg {
      animation: ${arrowAnimation} 1s ease-in-out infinite;
      margin-left: 8px;
    }
  }
`;
