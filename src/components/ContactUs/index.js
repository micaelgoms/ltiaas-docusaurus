import React, { useState } from "react";
import * as S from "./styled";
import { UilMailbox } from "@iconscout/react-unicons";
import toast, { Toaster } from "react-hot-toast";
import { PopupWidget } from "react-calendly";
const encode = (data) => {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
};

function ContactUs() {
  const [dataForm, setDataForm] = useState({
    name: "",
    email: "",
    role: "technical",
    privacy: false,
    message: "",
  });

  const handleInput = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    setDataForm({
      ...dataForm,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!dataForm.privacy) {
      toast.error("Please accept our privacy policy first.");
      return;
    }
    const options = {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "contactForm", ...dataForm }),
    };

    fetch("/", options)
      .then((response) => {
        toast.success("Thanks for your interest!");
        console.log("res", response);
      })
      .catch((error) => {
        toast.error("oops... something wrong happened");
        console.error(error);
      });
  };

  return (
    <S.Container id="top">
      <Toaster position="top-center" />

      <S.Wrapper>
        <PopupWidget
          branding
          color="#ff0059"
          pageSettings={{
            backgroundColor: "ecf6fe",
            hideEventTypeDetails: false,
            hideGdprBanner: true,
            hideLandingPageDetails: false,
            primaryColor: "023e73",
            textColor: "000000",
          }}
          prefill={{
            guests: ["joecrop@ltiaas.com", "cvmcosta@ltiaas.com"],
          }}
          text="Schedule a Meeting With Us!"
          textColor="#ffffff"
          url="https://calendly.com/ltiaas/30min"
        />
        <h1>Contact Us</h1>
        <p>
          Have any questions about LTIAAS? Need help using or integration our
          API? Please send us a message and we will get back to you ASAP.
        </p>
        <p> 
          Or, schedule a meeting with us using the button at the bottom of the page.
        </p>

        <S.Form
          name="contactForm"
          method="post"
          data-netlify="true"
          data-netlify-honeypot="bot-field"
          onSubmit={handleSubmit}
        >
          <label>
            Your Name:
            <input
              type="text"
              name="name"
              value={dataForm.name}
              onChange={handleInput}
            />
          </label>

          <label>
            Your Email:
            <input
              type="email"
              name="email"
              value={dataForm.email}
              onChange={handleInput}
            />
          </label>

          <label>
            Your Role:
            <select name="role" value={dataForm.role} onChange={handleInput}>
              <option value="technical">Technical</option>
              <option value="business">Business</option>
              <option value="educator">Educator</option>
            </select>
          </label>

          <label>
            Message:
            <textarea
              name="message"
              value={dataForm.message}
              onChange={handleInput}
            ></textarea>
          </label>

          <label>
            <input
              class="checkbox"
              type="checkbox"
              name="privacy"
              checked={dataForm.privacy}
              onChange={() =>
                handleInput({
                  target: { name: "privacy", value: !dataForm.privacy },
                })
              }
            ></input>
            I have read and I accept the{" "}
            <a target="blank" href="/privacy-policy">
              privacy policy
            </a>
            .
          </label>

          <button type="submit">
            Send <UilMailbox size={22} />
          </button>
        </S.Form>
      </S.Wrapper>
    </S.Container>
  );
}

export default ContactUs;
