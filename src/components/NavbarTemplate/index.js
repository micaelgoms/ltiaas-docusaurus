import React, { useState } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

import { LocalizedLink } from "gatsby-theme-i18n";

import * as S from "./styled";

function Navbar() {
  const [openSidebar, setOpenSidebar] = useState(false);

  const imgLogo = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "brand/logo.png" }) {
        childImageSharp {
          fixed(width: 100) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
  `);

  const handleToogleSidebar = () => {
    setOpenSidebar(!openSidebar);
  };

  return (
    <>
      <S.FakeContainer />
      <S.Container>
        <S.Content openSidebar={openSidebar}>
          <div>
            <LocalizedLink to="/" language="en">
              <Img
                fixed={imgLogo.file.childImageSharp.fixed}
                alt="Lti as a service"
              />
            </LocalizedLink>

            <ul>
              <li>
                <S.ButtonWaitlistMobile to="https://portal.ltiaas.com" language="en">
                  Customer Portal
                </S.ButtonWaitlistMobile>
              </li>
              <li>
                <S.ButtonWaitlistMobile to="/contact-us" language="en">
                  Contact Us
                </S.ButtonWaitlistMobile>
              </li>
            </ul>
          </div>

          <S.RightContainer>
            <S.ButtonWaitlist to="https://portal.ltiaas.com" language="en">
              Customer Portal
            </S.ButtonWaitlist>
            <S.ButtonWaitlist to="/contact-us" language="en">
              Contact Us
            </S.ButtonWaitlist>
          </S.RightContainer>
          <S.ButtonSidebar
            openSidebar={openSidebar}
            onClick={handleToogleSidebar}
          >
            <span />
            <span />
          </S.ButtonSidebar>
        </S.Content>
      </S.Container>
    </>
  );
}

export default Navbar;
