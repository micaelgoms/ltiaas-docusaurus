import styled from "styled-components";
import Slider from "infinite-react-carousel";
import flow from "../../images/Flow/4.svg";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 8em auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 1200px;
  height: 100%;
`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  margin-bottom: 2rem;
  padding: 0 1rem;

  p {
    font-weight: 400;
    font-size: 18px;
    max-width: 500px;
    text-align: center;
  }

  h3 {
    position: relative;

    &::before {
      content: "";
      position: absolute;
      background: url(${flow});
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
      width: 250px;
      height: 160px;
      top: -160px;
      left: -110px;
      z-index: -5;

      @media (min-width: 992px) {
        top: -120px;
        left: -260px;
      }
    }
  }
`;

export const CommentsList = styled(Slider)`
  .carousel-dots li button {
    &::before {
      background: #f2275d;
      color: transparent;
      width: 10px;
      height: 10px;
      content: "";
      border-radius: 50%;
    }

    &:hover {
      transform: none;
    }
  }

  .carousel-dots .carousel-dots-active button {
    &::before {
      background: #fff;
      border: 4px solid #f2275d;
      top: -4px;
      left: -3px;
    }

    &:hover {
      transform: none;
    }
  }
`;

export const CommentWrapper = styled.div`
  padding: 1rem;
`;

export const CommentBaloon = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  div {
    background: #ffffff;
    border-radius: 10px;
    text-align: center;
    position: relative;

    padding: 1rem;
    box-shadow: 0px 30px 40px rgba(212, 217, 232, 0.2);
    margin-bottom: 2rem;

    &::before {
      content: "";
      position: absolute;
      background: #fff;
      width: 24px;
      height: 24px;
      bottom: -12px;
      margin-left: -12px;
      transform: rotateZ(45deg);
      border-radius: 4px;
      box-shadow: 0px 30px 40px rgba(212, 217, 232, 0.2);
    }

    h5 {
      margin: 0.5rem 0;
    }

    p {
      text-align: center;
      margin-bottom: 0.5rem;
      font-size: 14px;
      line-height: 1.8;
      height: 100%;

      @media (min-width: 769px) {
        height: 150px;
      }
    }
  }

  img {
    width: 56px;
    height: 56px;
    box-shadow: 0px 5px 5px rgb(212 217 232 / 20%);
    margin-bottom: 8px;
    border-radius: 50%;
  }

  h6 {
    margin: 0.5rem 0;
    font-size: 18px;
    line-height: 0;
  }
`;
