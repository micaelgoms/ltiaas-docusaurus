export const comments = [
  {
    title: "Dynos.IO",
    content:
      "Implementing LTI spec for your product can be very time-consuming and can cause significant delays in your product implementation, if not done right. Ltiaas helped us greatly in achieving our LTI integration milestone. We highly recommend LTIJS or Ltiaas for your LTI integration needs for their detailed documentation and support.",
    photo: require("../../images/clients/pramod.jpeg").default,
    author: "Pramod Vadrevu",
    ocupation: "Founder & President",
  },
  {
    title: "CPM Educational Program",
    content:
      "With ltiaas everything just worked. No surprises and no unexpected delays. We went from proof of concept to production ready in a few weeks. Awesome experience all around.",
    photo: require("../../images/clients/curtis.jpeg").default,
    author: "Curtis Fuhriman",
    ocupation: "CTO",
  },
  {
    title: "Engage Fast Learning",
    content:
      "LTIAAS was very easy to get installed on multiple LMS platforms for a fraction of the cost that IMS or other providers would charge and their support service has been outstanding since then. Would highly recommend Carlos and his team.",
    photo: require("../../images/clients/nitin.jpeg").default,
    author: "Nitin Jain",
    ocupation: "CEO & Founder",
  },
  {
    title: "Exputo",
    content:
      "When your LMS only supports LTI 1.1 (without deep linking), you have two options. Cry yourself to sleep or have the good luck to find LTIaaS. The team at LTIaaS took our requirements and turned our LTI 1.1 implementation into a fully compliant LTI 1.3 setup. Goodbye manually adding 200+ LTI 1.1 links! We could not be happier with the support and customizations that we received from LTIaaS. Highly recommended!",
    photo: require("../../images/clients/david.jpeg").default,
    author: "David Pesce",
    ocupation: "President",
  },
];
