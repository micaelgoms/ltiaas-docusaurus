import React from "react";

import * as S from "./styled";

import { comments } from "./content";

const Testimonials = () => {
  const getQtdItem = () => {
    if (typeof window !== `undefined`) {
      if (window.innerWidth > 768) {
        return 2;
      }

      return 1;
    }
  };

  const settings = {
    arrows: false,
    slidesPerRow: getQtdItem(),
    dots: true,
  };

  return (
    <S.Container>
      <S.Wrapper>
        <S.TitleWrapper>
          <h3>Our Clients Speak</h3>
          <p>We have been working with clients around the world</p>
        </S.TitleWrapper>

        <S.CommentsList {...settings}>
          {comments.map((comment, i) => (
            <S.CommentWrapper key={i}>
              <S.CommentBaloon>
                <div>
                  <h5>{comment.title}</h5>
                  <p>{comment.content}</p>
                </div>

                {comment.photo && (
                  <img src={comment.photo} alt={comment.author} />
                )}
                <h6>{comment.author}</h6>
                <small>{comment.ocupation}</small>
              </S.CommentBaloon>
            </S.CommentWrapper>
          ))}
        </S.CommentsList>
      </S.Wrapper>
    </S.Container>
  );
};

export default Testimonials;
