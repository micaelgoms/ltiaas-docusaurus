import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 2.5em auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  width: 100%;
  max-width: 768px;
  height: 100%;
  padding: 1rem;

  position: relative;

  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    width: 100%;
    margin-bottom: 2rem;
  }

  small {
    margin-bottom: 1rem;
  }

  p {
    margin-bottom: 1rem;
  }

  .footnote {
    margin-top: 0rem;
    margin-bottom: -1rem;
    font-size: 12px;
  }

  img {
    width: 100%;
    object-fit: cover;
  }

  table {
    width: 734px;
    border-collapse: collapse;
    margin-bottom: 32px;
  }

  .pricing_table {
    margin-bottom: 0;
  }

  .table-responsive {
    overflow-x: auto;
    -webkit-box-pack: unset;
    -webkit-justify-content: unset;
    -ms-flex-pack: unset;
    justify-content: unset;
    -webkit-align-items: unset;
    -webkit-box-align: unset;
    -ms-flex-align: unset;
    align-items: unset;
  }

  table {
    tr {
      td {
        border: 2px solid rgba(100, 88, 92, 0.15);
        padding: 0.5rem;

        &:nth-child(2) {
          background: rgba(100, 88, 92, 0.05);
          font-family: "Lucida Console", "Courier New", monospace;
          text-align: center;
          font-weight: 800;
          font-size: 18px;
        }
        &:nth-child(3) {
          background: rgba(100, 88, 92, 0.05);
          font-family: "Lucida Console", "Courier New", monospace;
          text-align: center;
          font-weight: 800;
          font-size: 18px;
        }
        &:nth-child(4) {
          background: rgba(100, 88, 92, 0.05);
          font-family: "Lucida Console", "Courier New", monospace;
          text-align: center;
          font-weight: 800;
          font-size: 18px;
        }
        &:nth-child(5) {
          background: rgba(100, 88, 92, 0.05);
          font-family: "Lucida Console", "Courier New", monospace;
          text-align: center;
          font-weight: 800;
          font-size: 18px;
        }
        &:nth-child(6) {
          background: rgba(100, 88, 92, 0.05);
          font-family: "Lucida Console", "Courier New", monospace;
          text-align: center;
          font-weight: 800;
          font-size: 18px;
        }
        &:nth-child(7) {
          background: rgba(100, 88, 92, 0.05);
          font-family: "Lucida Console", "Courier New", monospace;
          text-align: center;
          font-weight: 800;
          font-size: 18px;
        }
      }
    }

    thead {
      td {
        padding: 0.5rem;
        border: 2px solid #023e73;
        color: #023e73;
        font-family: Russo One;
        line-height: 20px;
      }
    }
  }

  .pricing_table_header, th{
    background-color:#023D70;
    color:#FFF;
    font-weight:bold;
    text-align:center;
    vertical-align:top;
  }

  ul {
    margin-left: 16px;

    li {
      margin-bottom: 8px;
    }
  }

  ol {
    list-style: none;
    counter-reset: ol-counter;
    margin-bottom: 32px;

    li {
      counter-increment: ol-counter;
      margin-bottom: 8px;

      &::before {
        content: counter(ol-counter) " . ";
        font-weight: bold;
        font-family: Russo One;
        margin-right: 4px;
      }
    }
  }

  a {
    font-weight: 800;

    &:link {
      color: #023e73;
    }

    &:visited {
      color: #023e73;
    }

    &:hover {
      color: #f2275d;
    }

    &:active {
      color: #023e73;
    }
  }
`;
