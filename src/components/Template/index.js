import React from "react";
import * as S from "./styled";

function Template({ children }) {
  return (
    <S.Container id="top">
      <S.Wrapper>{children}</S.Wrapper>
    </S.Container>
  );
}

export default Template;
