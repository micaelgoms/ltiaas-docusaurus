import styled, { keyframes } from "styled-components";
import Lottie from "react-lottie";

export const Container = styled.div`
  width: 100%;
  min-height: 60vh;
  display: flex;
  flex-direction: column;
`;

export const Title = styled.div`
  max-width: 750px;
  text-align: center;

  margin: 32px auto;
  margin-bottom: 0;

  padding: 0 1rem;
  position: relative;

  @media (min-width: 769px) {
    margin-bottom: -50px;
  }

  h1 {
    margin-bottom: 16px;
    font-size: 2.5em;
    line-height: 42px;

    @media (min-width: 769px) {
      font-size: 3.052em;
      line-height: 58px;
    }
  }

  p {
    font-size: 1.125em;
  }
`;

export const AnimationContainer = styled(Lottie)``;

const arrowAnimation = keyframes`
  0% {
    transform: translateY(-1px);
  }

  50% {
    transform: translateY(1px);
  }

  100% {
    transform: translateY(-1px);
  }
`;

export const ButtonGuied = styled.button`
  background: #f2275d;
  color: #fff;
  border-radius: 50px;
  padding: 7px 17px;
  margin: 5% auto 0;

  display: flex;
  align-items: center;

  font-size: 18px;

  cursor: pointer;
  font-family: Russo One;
  border: none;

  transition: transform 0.2s ease-in-out;

  &:hover {
    transform: translateY(-3px);
  }

  svg {
    animation: ${arrowAnimation} 1s ease-in-out infinite;
    margin-right: 8px;
  }

  @media (min-width: 992px) {
    margin: 20% auto 0;
  }
`;
