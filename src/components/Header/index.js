import React from "react";
import * as S from "./styled";
import { useTranslation } from "react-i18next";

import animationData from "../../animations/data.json";

function Header() {
  const { t } = useTranslation();

  const defaultOptions = {
    renderer: "svg",
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <S.Container id="top">
      <S.Title>
        <h1
          data-sal="slide-up"
          data-sal-delay="300"
          data-sal-easing="ease-in-out"
        >
          {t("titleHeader")}
        </h1>
        <p
          data-sal="slide-up"
          data-sal-delay="400"
          data-sal-easing="ease-in-out"
        >
          {t("subTitleHeader")}
        </p>
      </S.Title>

      <S.AnimationContainer
        data-sal="slide-up"
        data-sal-delay="100"
        data-sal-easing="ease-in-out"
        options={defaultOptions}
        style={{
          maxWidth: 1200,
        }}
      />
    </S.Container>
  );
}

export default Header;
