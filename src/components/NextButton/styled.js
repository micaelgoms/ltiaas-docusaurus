import styled from "styled-components";

export const Container = styled.button`
  background: #f2275d;
  color: #fff;
  border-radius: 50px;
  padding: 7px 17px;
  font-size: 18px;

  position: absolute;

  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0;

  transition: 2s opacity linear;

  svg {
    margin-right: 8px;
  }
`;
