import React from "react";
import UilArrowDown from "@iconscout/react-unicons/icons/uil-arrow-down";

import * as S from "./styled";

function NextButton() {
  return (
    <S.Container id="next-button">
      <UilArrowDown size={24} />
      Next
    </S.Container>
  );
}

export default NextButton;
