import styled, { css } from "styled-components";
import { LocalizedLink } from "gatsby-theme-i18n";

export const FakeContainer = styled.div`
  width: 100%;
  height: 75px;
  display: flex;
  z-index: 9999;
  background: #e6f5ff;

  @media (min-width: 820px) {
    display: none;
  }
`;

export const Container = styled.div`
  position: fixed;
  width: 100%;
  top: 0;
  height: 75px;
  display: flex;
  z-index: 9999;
  background: #e6f5ff;
  box-shadow: 0 2px 4px 0px rgb(0 0 0 / 5%);

  @media (min-width: 820px) {
    position: inherit;
    box-shadow: none;
  }
`;

export const Content = styled.nav`
  padding-right: 1rem;
  padding-left: 1rem;
  margin: 0 auto;

  display: flex;
  justify-content: space-between;
  align-items: center;

  width: 100%;
  max-width: 1200px;

  div {
    display: flex;
    align-items: center;
  }

  .gatsby-image-wrapper {
    margin-bottom: 4px;
  }

  ul {
    display: block;
    margin: 0;
    transition: all 0.3s ease-in-out;
    background: #023e73;
    text-align: center;

    position: absolute;
    right: 0;
    left: 0;
    top: 75px;

    height: 0;
    z-index: 0;
    overflow: hidden;
    text-overflow: clip;

    ${(props) =>
      props.openSidebar &&
      css`
        height: 250px;
        padding: 1rem;
      `}

    @media (min-width: 820px) {
      margin-left: 2rem;
      position: unset;
      background: transparent;
      display: flex;
      height: auto;
    }

    li {
      list-style: none;

      @media (min-width: 820px) {
        & + li {
          margin-left: 1rem;
        }
      }

      &:nth-last-child(-n+2)  {
        display: block;

        @media (min-width: 820px) {
          display: none;
        }
      }

      a {
        text-decoration: none;
        color: #fff;
        opacity: 0;
        transition: all 0.4s linear;
        display: block;

        ${(props) =>
          props.openSidebar &&
          css`
            opacity: 1;
          `}

        @media (min-width: 820px) {
          color: inherit;
          opacity: 1;
        }
      }
    }
  }
`;

export const ButtonWaitlist = styled(LocalizedLink)`
  margin-left: 1rem;
  background: #023e73;
  border-radius: 50px;
  padding: 0 16px;
  color: #fff;
  display: none;
  text-decoration: none;
  font-weight: 800;

  transition: transform 0.2s ease-in-out;

  &:hover {
    transform: translateY(-3px);
  }

  @media (min-width: 820px) {
    display: block;
  }
`;

export const RightContainer = styled.div`
  margin-left: auto;

  @media (min-width: 820px) {
    display: block;
  }
`;

export const ButtonWaitlistMobile = styled(LocalizedLink)`
  margin-left: auto;
  background: #fff;
  border-radius: 50px;
  padding: 0 16px;
  color: #023e73 !important;
  display: none;
  text-decoration: none;
  font-weight: 800;
  margin: 14px;

  transition: transform 0.2s ease-in-out;

  &:hover {
    transform: translateY(-3px);
  }

  @media (min-width: 820px) {
    display: block;
  }
`;

export const ButtonSidebar = styled.button`
  position: relative;
  margin: auto 0;
  width: 30px;
  height: 24px;
  display: block;
  background: transparent;
  border: none;

  @media (min-width: 820px) {
    display: none;
  }

  span {
    background: #023e73;
    width: 30px;
    height: 4px;
    display: block;
    border-radius: 3px;
    transition: transform 0.2s ease-in-out;

    ${(props) =>
      props.openSidebar &&
      css`
        transform: rotateZ(45deg);
        margin-top: -8px;
      `}

    & + span {
      ${(props) =>
        props.openSidebar &&
        css`
          transform: rotateZ(-45deg);
        `}
    }

    &:first-child {
      margin-bottom: 5px;
    }
  }
`;
