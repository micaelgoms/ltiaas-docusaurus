import React, { useState } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

import AnchorLink from "react-anchor-link-smooth-scroll";
import { LocalizedLink } from "gatsby-theme-i18n";

import * as S from "./styled";

function Navbar() {
  const [openSidebar, setOpenSidebar] = useState(false);

  const imgLogo = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "brand/logo.png" }) {
        childImageSharp {
          fixed(width: 100) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
  `);

  const handleToogleSidebar = () => {
    setOpenSidebar(!openSidebar);
  };

  return (
    <>
      <S.FakeContainer />
      <S.Container>
        <S.Content openSidebar={openSidebar}>
          <div>
            <LocalizedLink to="/" language="en">
              <Img
                fixed={imgLogo.file.childImageSharp.fixed}
                alt="Lti as a service"
              />
            </LocalizedLink>

            <ul>
              <li>
                <AnchorLink
                  href="#description"
                  offset="100"
                  onClick={handleToogleSidebar}
                >
                  Why LTIAAS?
                </AnchorLink>
              </li>
              <li>
                <AnchorLink
                  href="#cases"
                  offset="100"
                  onClick={handleToogleSidebar}
                >
                  Use cases
                </AnchorLink>
              </li>
              <li>
                <AnchorLink
                  href="#pricing"
                  offset="100"
                  onClick={handleToogleSidebar}
                >
                  Pricing
                </AnchorLink>
              </li>
              <li>
                <AnchorLink
                  href="#documentation"
                  offset="100"
                  onClick={handleToogleSidebar}
                >
                  Documentation
                </AnchorLink>
              </li>
              <li>
                <S.ButtonWaitlistMobile to="https://portal.ltiaas.com" language="en">
                  Customer Portal
                </S.ButtonWaitlistMobile>
              </li>
              <li>
                <S.ButtonWaitlistMobile to="/contact-us" language="en">
                  Contact Us
                </S.ButtonWaitlistMobile>
              </li>
            </ul>
          </div>

          <S.RightContainer>
            <S.ButtonWaitlist to="https://portal.ltiaas.com" language="en">
              Customer Portal
            </S.ButtonWaitlist>
            <S.ButtonWaitlist to="/contact-us" language="en">
              Contact Us
            </S.ButtonWaitlist>
          </S.RightContainer>
          <S.ButtonSidebar
            openSidebar={openSidebar}
            onClick={handleToogleSidebar}
          >
            <span />
            <span />
          </S.ButtonSidebar>
        </S.Content>
      </S.Container>
    </>
  );
}

export default Navbar;
