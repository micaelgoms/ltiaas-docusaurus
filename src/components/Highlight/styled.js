import styled from "styled-components";
import SyntaxHighlighter from "react-syntax-highlighter";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;

  width: 100%;
  height: auto;

  flex: 1;
  padding: 0.3rem;
  min-width: 100%;

  @media (min-width: 769px) {
    min-width: auto;
    padding: 0;
  }
`;

export const Highlight = styled(SyntaxHighlighter)`
  border-radius: 5px;
  height: 450px;
  line-height: 22px;

  scrollbar-width: thin;
  scrollbar-color: rgba(225, 225, 225, 0.5) transparent;

  &::-webkit-scrollbar {
    width: 8px;
  }

  &::-webkit-scrollbar-track {
    background: transparent;
  }

  &::-webkit-scrollbar-thumb {
    background-color: rgba(225, 225, 225, 0.5);
    border-radius: 20px;
    border: 3px solid transparent;
  }
`;

export const Terminal = styled(SyntaxHighlighter)`
  border-radius: 0 0 5px 5px;
  height: 100px;
  overflow: hidden;
  line-height: 20px;
`;
