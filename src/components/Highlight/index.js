import React from "react";
import {
  tomorrowNightBlue,
  vs2015,
} from "react-syntax-highlighter/dist/esm/styles/hljs";

import { useCodeHightlight } from "../../hooks/codeHightlight";
import { code } from "./content";

import * as S from "./styled";

const Highlight = () => {
  const { currentCode } = useCodeHightlight();

  const outString = `  your.ltiaas.com/api/login           | Method: \`POST\` | Status: 302 | Response Time: 350ms
  clientlms.com/auth.php?id_token=... | Method: \`GET\`  | Status: 200 | Response Time: 800ms
  yourtool.com/launch?ltik=...        | Methos: \`GET\`  | Status: 200 | Response Time: 66ms`;

  return (
    <S.Wrapper>
      <S.Highlight
        language="javascript"
        style={tomorrowNightBlue}
        showLineNumbers={true}
      >
        {code[currentCode]}
      </S.Highlight>
    </S.Wrapper>
  );
};

export default Highlight;
