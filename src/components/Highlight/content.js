export const code = [
`// Retrieve ID Token after launch
app.get('/app', async (req, res, next) => {
  // Get the ltik token generated by the LTI Launch
  const ltik = req.query.ltik
  // Build authorization header
  const authorizationHeader = \`LTIK-AUTH-V1 Token=\${ltik}, Additional=Bearer \${API_KEY}\`
  // Get the idtoken using the LTIaaS API
  const url = 'https://your.ltiaas.com/api/idtoken'
  const idtoken = await request.get(url, { 
    headers: { Authorization: authorizationHeader }
  }).json()
  return res.send(idtoken)
})`,

`// Retrieve course memberships
app.get('/get-members', async (req, res, next) => {
  // Get the ltik token generated by the LTI Launch
  const ltik = req.query.ltik
  // Build authorization header
  const authorizationHeader = \`LTIK-AUTH-V1 Token=\${ltik}, Additional=Bearer \${API_KEY}\`
  // Get memberships using the LTIaaS API
  const url = 'https://your.ltiaas.com/api/memberships'
  const response = await request.get(url, { 
    headers: { Authorization: authorizationHeader }
  }).json()
  return res.send(response.members)
})`,

`// Submit user scores
app.get('/submit-score', async (req, res, next) => {
  // Get the ltik token generated by the LTI Launch
  const ltik = req.query.ltik
  // Get Grade Line ID
  const lineitemId = encodeURIComponent('https://lms.example.com/context/2923/lineitems/1')
  // Build score object
  const score = {
    userId: '5323497',
    activityProgress: 'Completed',
    gradingProgress: 'FullyGraded',
    scoreGiven: 83,
    comment: 'This is exceptional work.',
  }
  // Build authorization header
  const authorizationHeader = \`LTIK-AUTH-V1 Token=\${ltik}, Additional=Bearer \${API_KEY}\`
  // Submit user score using the LTIaaS API
  const url = \`https://your.ltiaas.com/api/lineitems/\${lineitemId}/scores\`
  const response = await request.post(url, {
    json: score, headers: { Authorization: authorizationHeader }
  }).json()
  return res.send(response)
})`,

`// Using Deep Linking to submit resources to LMS
// Calling the LTIaaS API in the backend
app.post('/submit-link', async (req, res, next) => {
  // Get the ltik token generated by the LTI Launch
  const ltik = req.query.ltik
  // Build Tool Link object
  const link = {
    contentItems: [{
      type: 'ltiResourceLink',
      url: 'https://tool.com?resourceid=123456',
      title: 'Resource'
    }],
    options: { 
      message: 'Deep Linking successful!',
      log: 'deep_linking_successful'
    }
  }
  // Build authorization header
  const authorizationHeader = \`LTIK-AUTH-V1 Token=\${ltik}, Additional=Bearer \${API_KEY}\`
  // Call the LTIaaS API to generate self-submitting form
  const url = 'https://your.ltiaas.com/api/deeplinking/form'
  const response = await request.post(url, {
    json: link, headers: { Authorization: authorizationHeader }
  }).json()
  return res.send(response)
})

// Append self-submitting form to HTML body in the frontend
$('body').append(response.form)`,

`// Create a new Platform registration
const platform = {
  url: 'https://platform.org',
  clientId: '10000000000001',
  name: 'Platform 1',
  authenticationEndpoint: 'https://platform.org/lti/authorize',
  accesstokenEndpoint: 'https://platform.org/login/oauth2/token',
  authConfig: {
    key: 'https://platform.org/lti/security/jwks',
    method: 'JWK_SET'
  }
}
// Build authorization header
const authorizationHeader = \`Bearer \${API_KEY}\`
// Register Platform using the LTIaaS API
const url = 'https://your.ltiaas.com/api/platforms'
const newPlatform = await request.post(url, {
  json: platform, headers: { Authorization: authorizationHeader }
}).json()
return newPlatform`
];
